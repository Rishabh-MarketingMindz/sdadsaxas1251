<?php
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';

// check if database backup page is active
@$database_backup = get_option('database_backup_activate');
if($database_backup == "yes"){
?>
<style type="text/css">
	/* Absolute Center Spinner */
	.loading {
		position: fixed;
		z-index: 999;
		height: 2em;
		width: 2em;
		overflow: visible;
		margin: auto;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
	}
	/* Transparent Overlay */
	.loading:before {
		content: '';
		display: block;
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(18, 199, 132, 0.88);
	}
	/* :not(:required) hides these rules from IE9 and below */
	.loading:not(:required) {
		/* hide "loading..." text */
		font: 0/0 a;
		color: #fff;
		text-shadow: none;
		background-color: rgba(18, 199, 132, 0.88);
		border: 0;
	}
	.loading:not(:required):after {
		content: '';
		display: block;
		font-size: 10px;
		width: 1em;
		height: 1em;
		margin-top: -0.5em;
		-webkit-animation: spinner 1500ms infinite linear;
		-moz-animation: spinner 1500ms infinite linear;
		-ms-animation: spinner 1500ms infinite linear;
		-o-animation: spinner 1500ms infinite linear;
		animation: spinner 1500ms infinite linear;
		border-radius: 0.5em;
		-webkit-box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
		box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, #E53373 -1.5em 0 0 0, #E53373 -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
	}

	/* Animation */
	@-webkit-keyframes spinner {
	 0% {
	 -webkit-transform: rotate(0deg);
	 -moz-transform: rotate(0deg);
	 -ms-transform: rotate(0deg);
	 -o-transform: rotate(0deg);
	 transform: rotate(0deg);
	}
	 100% {
	 -webkit-transform: rotate(360deg);
	 -moz-transform: rotate(360deg);
	 -ms-transform: rotate(360deg);
	 -o-transform: rotate(360deg);
	 transform: rotate(360deg);
	}
	}
	@-moz-keyframes spinner {
	 0% {
	 -webkit-transform: rotate(0deg);
	 -moz-transform: rotate(0deg);
	 -ms-transform: rotate(0deg);
	 -o-transform: rotate(0deg);
	 transform: rotate(0deg);
	}
	 100% {
	 -webkit-transform: rotate(360deg);
	 -moz-transform: rotate(360deg);
	 -ms-transform: rotate(360deg);
	 -o-transform: rotate(360deg);
	 transform: rotate(360deg);
	}
	}
	@-o-keyframes spinner {
	 0% {
	 -webkit-transform: rotate(0deg);
	 -moz-transform: rotate(0deg);
	 -ms-transform: rotate(0deg);
	 -o-transform: rotate(0deg);
	 transform: rotate(0deg);
	}
	 100% {
	 -webkit-transform: rotate(360deg);
	 -moz-transform: rotate(360deg);
	 -ms-transform: rotate(360deg);
	 -o-transform: rotate(360deg);
	 transform: rotate(360deg);
	}
	}
	@keyframes spinner {
	 0% {
	 -webkit-transform: rotate(0deg);
	 -moz-transform: rotate(0deg);
	 -ms-transform: rotate(0deg);
	 -o-transform: rotate(0deg);
	 transform: rotate(0deg);
	}
	 100% {
	 -webkit-transform: rotate(360deg);
	 -moz-transform: rotate(360deg);
	 -ms-transform: rotate(360deg);
	 -o-transform: rotate(360deg);
	 transform: rotate(360deg);
	}
	}
	.alert-box {
		display: none;
		position: fixed;
		width: 17%;
		height: 60px;
		z-index: 99999999999;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		padding: 15px;
		margin-bottom: 20px;
		border: 1px solid transparent;
		border-radius: 4px;
	}
	.success {
		color: #3c763d;
		background-color: #beffa4;
		border-color: #73d026;
	}
	.unsuccess {
		color: #4a4a4a;
		background-color: #ffa4a4;
		border-color: #d02626;
	}
</style>
<div class="alert-box"></div>
<div class="WPMC_media_scan">
  <div style="display: none" class="loading">Loading&#8230;</div>
  <!-- CMS usage data from w3techs.com / captured 7/6/16 -->
  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="" type="button">Create Full Backup</button>
      </li>
      <li> <span class="text">Settings:</span>
        <select name="content[]" multiple id="content" class="btn btn-blue">
          <option value="Pages">Pages</option>
          <option value="Post">Post</option>
          <option value="Custom Post">Custom Posts</option>
        </select>
      </li>
      <li>
        <select name="plugins[]" multiple id="plugins" class="btn btn-blue">
          <option value="Yoast SEO">Yoast SEO</option>
          <option value="WP Cache">WP Cache</option>
          <option value="WPML">WPML</option>
        </select>
      </li>
      <li>
        <button class="btn btn-blue" id="" type="button">Multisite</button>
      </li>
      <li>
        <label class="gray">Last Backup: 7-July-2019</label>
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="abcd" class="table table-striped table-sm table_design" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th>Backups</th>
          <th>Backups Content</th>
          <th>Creation Date</th>
          <th>Size</th>
        </tr>
      </thead>
      <tbody id="media_scanned_result">
        <tr>
          <td><input type="checkbox" name="" value=""></td>
          <td><span class="img_pic"></span><span class="img_name">My first backup <span class="red">restore</span></span></td>
          <td><a href="#">Full Backup</a>
            <div class="bottom_btn"><a href="#" class="red">All content</a></div></td>
          <td>7-Jun-2019 | 20:23:45</td>
          <td class="size-optimize"><span class="old-size">1.4 GB</span></td>
        </tr>
        <tr>
          <td><input type="checkbox" name="" value=""></td>
          <td><span class="img_pic"></span><span class="img_name">Only Images <span class="red">restore</span></span></td>
          <td><a href="#">Custom Backup</a>
            <div class="bottom_btn"><a href="#" class="red">Only Media</a></div></td>
          <td>7-Jun-2019 | 20:23:45</td>
          <td class="size-optimize"><span class="old-size">235 MD</span></td>
        </tr>
        <tr>
          <td><input type="checkbox" name="" value=""></td>
          <td><span class="img_pic"></span><span class="img_name">Products <span class="red">restore</span></span></td>
          <td><a href="#">Custom Backup</a>
            <div class="bottom_btn"><a href="#" class="red">Products</a></div></td>
          <td>7-Jun-2019 | 20:23:45</td>
          <td class="size-optimize"><span class="old-size">3 GB</span></td>
        </tr>
      </tbody>
    </table>
  </div>  
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-blue" id="" type="button">Select Filtered Content</button>
      </li>
      <li>
        <button class="btn btn-red" id="" type="button">Backup Filtered Content</button>
      </li>
      <li>
        <button class="btn btn-red" id="" type="button">Import Backup file</button>
      </li>
      <li>
        <button class="btn btn-red" id="" type="button">Restore Backup file</button>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_cpt',
			},
			success: function(response) {
				jQuery('#plugins').empty().append(response);
				jQuery('#plugins').multiselect({
				    nonSelectedText: 'Plugins',
				    allSelectedText: 'Plugins',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});
				/*jQuery('#postTypeOpt').multiselect('refresh');*/
	          	
	        },
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });
	});
	jQuery(document).ready(function(){
		jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_cpt',
			},
			success: function(response) {
				jQuery('#content').empty().append(response);
				jQuery('#content').multiselect({
				    nonSelectedText: 'Content',
				    allSelectedText: 'Content',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});
				/*jQuery('#postTypeOpt').multiselect('refresh');*/
	          	
	        },
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });
	});
</script>
<?php
}else{
  ?>
  <div class="WPMC_media_scan">
    <div><h3><?php _e("Database backup page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_string"); ?></h3></div>
  </div>
  <?php
}
?>