<?php
// date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');

$cron_type = $_POST['cron_type'];
if($cron_type == 'add_cron'){
    add_cron($_POST['time'], $_POST['interval'], $_POST['hook']);
}elseif($cron_type == 'remove_cron'){
    my_deactivation($_POST['hook']);
}

function add_cron($time, $schedule, $hookname){
    $shedule_time = strtotime($time);
    if (! wp_next_scheduled ( $hookname )) {
        // start new schedule
        wp_schedule_event($shedule_time, $schedule, $hookname);
        echo $message = __("Automatic Image Deleter Successfully Scheduled.","wp_media_cleaner");
    }
    else {
        // delete previous running schedule
        wp_clear_scheduled_hook( $hookname );
        // start new schedule
        wp_schedule_event($shedule_time, $schedule, $hookname);
        echo $message = __("Automatic Image Deleter Successfully Updated.","wp_media_cleaner");
    }
}

function my_deactivation($hook) {
    wp_clear_scheduled_hook( $hook );
    echo $message = __("Automatic Image Deleter Successfully Disabled.","wp_media_cleaner");
}