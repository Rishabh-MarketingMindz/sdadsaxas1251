<?php
error_reporting(0);
require_once('../../../../wp-load.php');
require_once('../../../../wp-admin/includes/file.php');
require 'simple_html_dom.php';

function getinboundLinks($domain_name) {
    $res = array();
    $arr = array();
    $url = $domain_name;
    $url_without_www=str_replace('http://','',$url);
    $url_without_www=str_replace('www.','',$url_without_www);
    $url_without_www= str_replace(strstr($url_without_www,'/'),'',$url_without_www);
    $url_without_www=trim($url_without_www);
    $input = @file_get_contents($url) or die('Could not access file: $url');
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
    if(preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) {
        // Site URL
        $wp_site_urls = get_site_url();
        foreach($matches as $match) {
            if(strpos($match[2],site_url()) !== false){
                if (filter_var($match[2], FILTER_VALIDATE_URL) && $match[2] !='mailto:' && strpos($match[2],$wp_site_urls) !== false) {
                    $res[] = $match[2];
                }
            }
        }
    }
    $data = array();
    $i = 0;
    foreach ($res as $value) {
        $html = file_get_html($value);
        // For Images
        foreach($html->find('img') as $element) {
            if(strpos($element->src,site_url()) !== false){
                $data[$i]['src'] = $element->src;

                // Root directory path of WordPress website
                $wp_root_pathh = get_home_path();
                // Site URL
                $wp_site_urll = get_site_url();

                $dir_path =  str_replace($wp_site_urll.'/',$wp_root_pathh,$data[$i]['src']);
                $unixtime = filemtime($dir_path);
                $data[$i]['datetime'] = date("Y-m-d | h:i:s",$unixtime);
                $str = file_get_contents($value);
                if(strlen($str)>0){
                    $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
                    preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
                    $data[$i]['title'] = $title[1];
                    $i++;
                }
            }
        }
    }
    return $data;
}

function walkDir($path = null) {
    $file_path = get_home_path().'';
    if(empty($path)) {
        $d = new DirectoryIterator($file_path);
    } else {
        $d = new DirectoryIterator($path);
    }
    $j = 0;
    global $arr2;
    foreach($d as $f) {
        if(
            $f->isFile() && 
            preg_match("/(\.gif|\.png|\.jpe?g)$/", $f->getFilename())
        ) {
            list($w, $h) = getimagesize($f->getPathname());
            // Complete directory path of images
            $dir_path = $f->getPathname();
            // Root directory path of WordPress website
            $wp_root_path = get_home_path();
            // Site URL
            $wp_site_url = get_site_url();

            $dimensions_w = $w;
            $dimensions_h = $h;
            $file_name = $f->getFilename();
            
            $arr2[$j]['src'] =  str_replace($wp_root_path,$wp_site_url.'/',$dir_path);
            $unixtime = filemtime($dir_path);
            $arr2[$j]['datetime'] = date("Y-m-d | h:i:s",$unixtime);
            $j++;
        } elseif($f->isDir() && $f->getFilename() != '.' && $f->getFilename() != '..') {
            walkDir($f->getPathname());
        }
    }
    // return $image_absolute_url;
}

$Domain='http://localhost/wpthemes/wp_media_cleaner/';
$arr1 = getinboundLinks($Domain);
walkDir();


// echo "<pre>";
// print_r($arr2);
// die();
function multi_array_diff($arr1, $arr2){
    $arrDiff = array();
    foreach($arr1 as $key => $val) {
        if(isset($arr2[$key])){
            if(is_array($val)){
                $arrDiff[$key] = multi_array_diff($val, $arr2[$key]);
            }else{
                if(in_array($val, $arr2)!=1){
                    $arrDiff[$key] = $val;
                }
            }
        }else{
            $arrDiff[$key] = $val;
        }
    }
    return $arrDiff;
}
 
echo '<pre>';
print_r(multi_array_diff($arr1, $arr2));
echo '</pre>';