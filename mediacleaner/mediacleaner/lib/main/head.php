<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( __('Kangaroos cannot jump here') );
}
?>
<?php if(is_admin()){ ?>
	<link rel="stylesheet" type="text/css" href="<?php echo WPMC_CSS . DIRECTORY_SEPARATOR . 'WPMC-style.css' ?>">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="<?php echo WPMC_JS . DIRECTORY_SEPARATOR . 'WPMC-script.js' ?>" type="text/javascript"></script>
<?php } ?>
<style type="text/css" media="all">
	.toplevel_page_media-cleaner-media-scan > div.wp-menu-image {
		background: none !important;
	}

	.toplevel_page_media-cleaner-media-scan > div.wp-menu-image:before {
		content: '';
		background: url('<?php echo wp_make_link_relative( WPMC_IMAGE ); ?>/menu-icon-30x30.png') no-repeat center center;
		width: 25px;
	}
	#toplevel_page_media-cleaner-media-scan .wp-submenu .wp-first-item{
		display: none;
	}
</style>