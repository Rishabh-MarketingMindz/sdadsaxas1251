<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
$media_urls = $_POST['media_src'];
$site_prefix_for_bulk = $_POST['site_prefix'];
$media_src = array();
$i = 0;
foreach ($media_urls as $mediavalue) {
	$media_src[$i]['media_src'] = $mediavalue;
	$i++;
}
$j = 0;
foreach ($site_prefix_for_bulk as $mediavalues) {
	$media_src[$j]['site_prefix'] = $mediavalues;
	$j++;
}
$request_type = $_POST['request_type'];
// $keep_original = get_option('wp_media_keep_original');
// if($keep_original == 'keep_original'){
// 	$backup = 'yes';
// }else{
// 	$backup = 'no';
// }
$backup = $_POST['backup'];
$media_prefix = $_POST['website_prefix'];
// Get directory path where to save backup images start
$directory_path = rtrim(get_home_path(), '/');
$wp_root_pathh = $directory_path.'/wp-content/plugins/wp_media_cleaner/backup/'	;
define('DIRECTORY', $wp_root_pathh);
// Get directory path where to save backup images ends
$delArr = array();
if($request_type == "single_media_delete"){
	$site_prefix = $media_prefix;
	$multisite_posts_table = $site_prefix."posts";
	$multisite_postsmeta_table = $site_prefix."postmeta";
	$delete_backup_table = $wpdb->prefix."optimizer_backup";

	$file = $media_urls;
	// Check if requested images are not in Excluded table start
    $exclude_table_name = $wpdb->prefix."optimizer_backup";
    $exclude_array = $wpdb->get_results("SELECT * from $exclude_table_name where type = 'exclude' AND Image_url = '$file'");
    // Check if requested images are not in Excluded table ends
    if(empty($exclude_array)){
		preg_match('!\d+!', $site_prefix, $matches);
		$get_site_id = $matches[0];

		if(is_multisite()){
			// get main site path
			$current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
			$mainsite_url = $current_blog_details->path;

			// get multi site path
			$current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
			$multisite_url = $current_blog_details2->path;
			if($get_site_id){
				$img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$file);
				$dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
				// getting blog id 
				$the_blog_id = $get_site_id;
			}else{
				$dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
				// getting blog id 
				$the_blog_id = '1';
			}
		}else{
			$dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
			// getting blog id 
			$the_blog_id = '1';
		}
		if($backup == 'yes'){
			$content = file_get_contents($dir_path);
			$is_backup = file_put_contents(DIRECTORY . basename($file), $content);
			chmod(DIRECTORY . basename($file), 0644);

			// Add media to 'wp_optimizer_backup' for backup start
				// Check if media is from media library

				// Trim URL to get specific part of multisite URL
					// "https://mediacleaner/wp-content/uploads/sites/2/2019/11/a_nature.jpg" but it store in database like "https://mediacleaner/my-second-site/wp-content/uploads/sites/2/2019/11/a_nature.jpg"
				$file_updated = substr($file, strpos($file, "/wp-content/") + 1);
				$if_from_library = $wpdb->get_results("SELECT * FROM $multisite_posts_table WHERE guid LIKE '%$file_updated'");
				if(!empty($if_from_library)){
					$take_backup = $wpdb->insert( $delete_backup_table, array("Image_url" => $file, "type" => "delete_backup", "blog_id" => $the_blog_id, "media_id" => $media_id));
				}
			// Add media to 'wp_optimizer_backup' for backup ends
			if($is_backup){
				if($get_site_id){
					if(!empty($if_from_library)){
						$attachmentid = @$if_from_library[0]->ID;
						$attachment_named = @$if_from_library[0]->post_name;
						$attachment_name = $attachment_named.'__trashed';
						$delete_attachment = $wpdb->query( $wpdb->prepare("UPDATE $multisite_posts_table SET post_status = 'trash', post_name = '$attachment_name' WHERE ID = '$attachmentid'"));
						if($delete_attachment){
							// Update (insert) post meta for trash attachment
							$get_time = time();
							$trash_status_meta = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_trash_meta_status", "meta_value" => $get_time, "post_id" => $attachmentid));

							$trash_time_meta = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_trash_meta_time", "meta_value" => "inherit", "post_id" => $attachmentid));

							$trash_slug_meta = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_desired_post_slug", "meta_value" => $attachment_named, "post_id" => $attachmentid));
							//Delete main attachment file from disk
							$delete_dir_attachment = unlink($dir_path);
							if($delete_dir_attachment){
								// Now delete all the resized images from directory START
								$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM $multisite_postsmeta_table where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
								$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
								if(@$unserializemetadata['sizes']){
									$mediabaseurl = substr($file, 0, strrpos( $file, '/'));
									$metaMediaArr = array();
									foreach ($unserializemetadata['sizes'] as $metavalue) {
										$murl = $mediabaseurl.'/'.$metavalue['file'];
										$metaMediaArr[] = $murl;
									}
									$meta_media_array = array_unique($metaMediaArr);
									foreach ($meta_media_array as $meta_url) {
										$meta_dir_path = str_replace(get_site_url().'/',get_home_path(),$meta_url);
										// Take backup of meta images
										$take_backup_meta = $wpdb->insert( $delete_backup_table, array("Image_url" => $meta_url, "type" => "delete_backup_meta", "blog_id" => $the_blog_id, "media_id" => $attachmentid));
										$content = file_get_contents($meta_dir_path);
										$is_backup = file_put_contents(DIRECTORY . basename($meta_dir_path), $content);
										chmod(DIRECTORY . basename($meta_dir_path), 0644);
										if($is_backup){
											unlink($meta_dir_path);
										}
									}
								}
								// Now delete all the resized images from directory ENDS
							}
							if($delete_dir_attachment){
								$message = __("Image deleted successfully");
								$response = array(
						    		'message'=> $message,
						    		'url'=> $file,
						    		'status' => 'success'
						    	);
							}else{
								$message = __("Something went wrong, Please try again","wp_media_cleaner");
								$response = array(
						    		'message'=> $message,
						    		'url'=> $file,
						    		'status' => 'error'
						    	);
							}
						}
					}else{
						$delete_src = $file;
						$if_deleted = unlink($dir_path);
						if($if_deleted){
							$message = __("Image deleted successfully");
							$response = array(
					    		'message'=> $message,
					    		'url'=> $file,
					    		'status' => 'success'
					    	);
						}else{
							$message = __("Something went wrong, Please try again","wp_media_cleaner");
							$response = array(
					    		'message'=> $message,
					    		'url'=> $file,
					    		'status' => 'error'
					    	);
						}
					}
				}else{
					// Check if media is from library : IN SINGLE SITE
					$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $multisite_posts_table WHERE guid='%s';", $file ));
					$attachmentid = @$attachment[0];
					if($attachmentid){
						$delete_attachment = wp_trash_post($attachmentid);
						if($delete_attachment){								
							//Delete main attachment file from disk
							$delete_dir_attachment = unlink($dir_path);
							if($delete_dir_attachment){
								// Now delete all the resized images from directory START
								$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM $multisite_postsmeta_table where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
								$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
								if(@$unserializemetadata['sizes']){
									$mediabaseurl = substr($file, 0, strrpos( $file, '/'));
									$metaMediaArr = array();
									foreach ($unserializemetadata['sizes'] as $metavalue) {
										$murl = $mediabaseurl.'/'.$metavalue['file'];
										$metaMediaArr[] = $murl;
									}
									$meta_media_array = array_unique($metaMediaArr);
									foreach ($meta_media_array as $meta_url) {
										$meta_dir_path = str_replace(get_site_url().'/',get_home_path(),$meta_url);
										// Take backup of meta images
										$take_backup_meta = $wpdb->insert( $delete_backup_table, array("Image_url" => $meta_url, "type" => "delete_backup_meta", "blog_id" => $the_blog_id, "media_id" => $attachmentid));
										$content = file_get_contents($meta_dir_path);
										$is_backup = file_put_contents(DIRECTORY . basename($meta_dir_path), $content);
										chmod(DIRECTORY . basename($meta_dir_path), 0644);
										if($is_backup){
											unlink($meta_dir_path);
										}
									}
								}
								// Now delete all the resized images from directory ENDS
							}
							if($delete_dir_attachment){
								$message = __("Image deleted successfully");
								$response = array(
						    		'message'=> $message,
						    		'url'=> $file,
						    		'status' => 'success'
						    	);
							}else{
								$message = __("Something went wrong, Please try again","wp_media_cleaner");
								$response = array(
						    		'message'=> $message,
						    		'url'=> $file,
						    		'status' => 'error'
						    	);
							}
						}
					}else{
						$delete_src = $file;
						$if_deleted = unlink($dir_path);
						if($if_deleted){
							$message = __("Image deleted successfully");
							$response = array(
					    		'message'=> $message,
					    		'url'=> $file,
					    		'status' => 'success'
					    	);
						}else{
							$message = __("Something went wrong, Please try again","wp_media_cleaner");
							$response = array(
					    		'message'=> $message,
					    		'url'=> $file,
					    		'status' => 'error'
					    	);
						}
					}
				}
			}
		}else{
			if($get_site_id){
				// Trim URL to get specific part of multisite URL
				// "https://mediacleaner/wp-content/uploads/sites/2/2019/11/a_nature.jpg" but it store in database like "https://mediacleaner/my-second-site/wp-content/uploads/sites/2/2019/11/a_nature.jpg"

				$file_updated = substr($file, strpos($file, "/wp-content/") + 1);
				// Check if media is from library : IN SINGLE SITE
				$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $multisite_posts_table WHERE guid LIKE '%$file_updated'"));
				$attachmentid = @$attachment[0];
				if($attachmentid){
					$delete_attachment = $wpdb->query("DELETE FROM $multisite_posts_table WHERE ID = '$attachmentid'");
					if($delete_attachment){
						//Delete main attachment file from disk
						$delete_dir_attachment = unlink($dir_path);
						if($delete_dir_attachment){
							// Now delete all the resized images from directory START
							$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM $multisite_postsmeta_table where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
							$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
							if(@$unserializemetadata['sizes']){
								$mediabaseurl = substr($file, 0, strrpos( $file, '/'));
								foreach ($unserializemetadata['sizes'] as $metavalue) {
									$murl = $mediabaseurl.'/'.$metavalue['file'];
									$meta_dir_path = str_replace(get_site_url().'/',get_home_path(),$murl);
									$delArr[] = $murl;
									unlink($meta_dir_path);
								}
							}
							// Now delete all the resized images from directory ENDS
						}
						if($delete_dir_attachment){
							$message = __("Image deleted successfully");
							$response = array(
					    		'message'=> $message,
					    		'url'=> $file,
					    		'status' => 'success'
					    	);
						}else{
							$message = __("Something went wrong, Please try again","wp_media_cleaner");
							$response = array(
					    		'message'=> $message,
					    		'url'=> $file,
					    		'status' => 'error'
					    	);
						}
					}
				}else{
					$delete_src = $file;
					$if_deleted = unlink($dir_path);
					if($if_deleted){
						$message = __("Image deleted successfully");
						$response = array(
				    		'message'=> $message,
				    		'url'=> $file,
				    		'status' => 'success'
				    	);
					}else{
						$message = __("Something went wrong, Please try again","wp_media_cleaner");
						$response = array(
				    		'message'=> $message,
				    		'url'=> $file,
				    		'status' => 'error'
				    	);
					}
				}
			}else{
				// Check if media is from library : IN SINGLE SITE
				$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $multisite_posts_table WHERE guid='%s';", $file ));
				$attachmentid = @$attachment[0];
				if($attachmentid){
					$delete_attachment = wp_delete_attachment($attachmentid, true);
				}else{
					$delArr[] = $file;
					unlink($dir_path);
				}
			}
		}
	}else{
    	$msg = __("The image is excluded","wp_media_cleaner");
    	$message = $msg;
    	// $message = $msg.': '.$file;
    	$response = array(
    		'message'=> $message,
    		'url'=> $file,
    		'status' => 'error'
    	);
    }
    echo json_encode($response);
}else{
	if($backup == 'yes'){
		foreach ($media_src as $media_value) {
			$file = $media_value['media_src'];
			$site_prefix = $media_value['site_prefix'];
			$multisite_posts_table = $site_prefix."posts";
			$multisite_postsmeta_table = $site_prefix."postmeta";
			$delete_backup_table = $wpdb->prefix."optimizer_backup";

			// Check if requested images are not in Excluded table start
		    $exclude_table_name = $wpdb->prefix."optimizer_backup";
		    $exclude_array = $wpdb->get_results("SELECT * from $exclude_table_name where type = 'exclude' AND Image_url = '$file'");
		    // Check if requested images are not in Excluded table ends
		    if(empty($exclude_array)){
				preg_match('!\d+!', $site_prefix, $matches);
				$get_site_id = $matches[0];

				if(is_multisite()){
					// get main site path
					$current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
					$mainsite_url = $current_blog_details->path;

					// get multi site path
					$current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
					$multisite_url = $current_blog_details2->path;
					if($get_site_id){
						$img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$file);
						$dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
						// getting blog id 
						$the_blog_id = $get_site_id;
					}else{
						$dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
						// getting blog id 
						$the_blog_id = '1';
					}
				}else{
					$dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
					// getting blog id 
					$the_blog_id = '1';
				}
				$content = file_get_contents($dir_path);
				$is_backup = file_put_contents(DIRECTORY . basename($file), $content);
				chmod(DIRECTORY . basename($file), 0644);

				// Add media to 'wp_optimizer_backup' for backup start
					// Check if media is from media library
					// Trim URL to get specific part of multisite URL
					// "https://mediacleaner/wp-content/uploads/sites/2/2019/11/a_nature.jpg" but it store in database like "https://mediacleaner/my-second-site/wp-content/uploads/sites/2/2019/11/a_nature.jpg"
					$file_updated = substr($file, strpos($file, "/wp-content/") + 1);
					$if_from_library = $wpdb->get_results("SELECT * FROM $multisite_posts_table WHERE guid LIKE '%$file_updated'");
					if(!empty($if_from_library)){
						$take_backup = $wpdb->insert( $delete_backup_table, array("Image_url" => $file, "type" => "delete_backup", "blog_id" => $the_blog_id, "media_id" => $media_id));
					}
				// Add media to 'wp_optimizer_backup' for backup ends

				if($is_backup){
					if($get_site_id){
						if(!empty($if_from_library)){
							$attachmentid = @$if_from_library[0]->ID;
							$attachment_named = @$if_from_library[0]->post_name;
							$attachment_name = $attachment_named.'__trashed';
							$delete_attachment = $wpdb->query( $wpdb->prepare("UPDATE $multisite_posts_table SET post_status = 'trash', post_name = '$attachment_name' WHERE ID = '$attachmentid'"));
							if($delete_attachment){
								// Update (insert) post meta for trash attachment
								$get_time = time();
								$trash_status_meta = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_trash_meta_status", "meta_value" => $get_time, "post_id" => $attachmentid));

								$trash_time_meta = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_trash_meta_time", "meta_value" => "inherit", "post_id" => $attachmentid));

								$trash_slug_meta = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_desired_post_slug", "meta_value" => $attachment_named, "post_id" => $attachmentid));
								//Delete main attachment file from disk
								$delete_dir_attachment = unlink($dir_path);
								if($delete_dir_attachment){
									// Now delete all the resized images from directory START
									$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM $multisite_postsmeta_table where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
									$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
									if(@$unserializemetadata['sizes']){
										$mediabaseurl = substr($file, 0, strrpos( $file, '/'));
										$metaMediaArr = array();
										foreach ($unserializemetadata['sizes'] as $metavalue) {
											$murl = $mediabaseurl.'/'.$metavalue['file'];
											$metaMediaArr[] = $murl;
										}
										$meta_media_array = array_unique($metaMediaArr);
										foreach ($meta_media_array as $meta_url) {
											$meta_dir_path = str_replace(get_site_url().'/',get_home_path(),$meta_url);
											// Take backup of meta images
											$take_backup_meta = $wpdb->insert( $delete_backup_table, array("Image_url" => $meta_url, "type" => "delete_backup_meta", "blog_id" => $the_blog_id, "media_id" => $attachmentid));
											$content = file_get_contents($meta_dir_path);
											$is_backup = file_put_contents(DIRECTORY . basename($meta_dir_path), $content);
											chmod(DIRECTORY . basename($meta_dir_path), 0644);
											if($is_backup){
												unlink($meta_dir_path);
											}
											$delArr[] = $meta_url;
										}
									}
									// Now delete all the resized images from directory ENDS
								}
							}
						}else{
							$delArr[] = $file;
							unlink($dir_path);
						}
					}else{
						// Check if media is from library : IN SINGLE SITE
						$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $multisite_posts_table WHERE guid='%s';", $file ));
						$attachmentid = @$attachment[0];
						if($attachmentid){
							$delete_attachment = wp_trash_post($attachmentid);
							if($delete_attachment){								
								//Delete main attachment file from disk
								$delete_dir_attachment = unlink($dir_path);
								if($delete_dir_attachment){
									// Now delete all the resized images from directory START
									$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM $multisite_postsmeta_table where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
									$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
									if(@$unserializemetadata['sizes']){
										$mediabaseurl = substr($file, 0, strrpos( $file, '/'));
										$metaMediaArr = array();
										foreach ($unserializemetadata['sizes'] as $metavalue) {
											$murl = $mediabaseurl.'/'.$metavalue['file'];
											$metaMediaArr[] = $murl;
										}
										$meta_media_array = array_unique($metaMediaArr);
										foreach ($meta_media_array as $meta_url) {
											$meta_dir_path = str_replace(get_site_url().'/',get_home_path(),$meta_url);
											// Take backup of meta images
											$take_backup_meta = $wpdb->insert( $delete_backup_table, array("Image_url" => $meta_url, "type" => "delete_backup_meta", "blog_id" => $the_blog_id, "media_id" => $attachmentid));
											$content = file_get_contents($meta_dir_path);
											$is_backup = file_put_contents(DIRECTORY . basename($meta_dir_path), $content);
											chmod(DIRECTORY . basename($meta_dir_path), 0644);
											if($is_backup){
												unlink($meta_dir_path);
											}
											$delArr[] = $meta_url;
										}
									}
									// Now delete all the resized images from directory ENDS
								}
							}
						}else{
							$delArr[] = $file;
							unlink($dir_path);
						}
					}
				}
			}
		}
	}else{
		foreach ($media_src as $media_value) {
			$file = $media_value['media_src'];
			$site_prefix = $media_value['site_prefix'];
			$multisite_posts_table = $site_prefix."posts";
			$multisite_postsmeta_table = $site_prefix."postmeta";

			// Check if requested images are not in Excluded table start
		    $exclude_table_name = $wpdb->prefix."optimizer_backup";
		    $exclude_array = $wpdb->get_results("SELECT * from $exclude_table_name where type = 'exclude' AND Image_url = '$file'");
		    // Check if requested images are not in Excluded table ends
		    if(empty($exclude_array)){
				preg_match('!\d+!', $site_prefix, $matches);
				$get_site_id = $matches[0];

				if(is_multisite()){
					// get main site path
					$current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
					$mainsite_url = $current_blog_details->path;

					// get multi site path
					$current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
					$multisite_url = $current_blog_details2->path;
					if($get_site_id){
						$img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$file);
						$dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
						// getting blog id 
						$the_blog_id = $get_site_id;
					}else{
						$dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
						// getting blog id 
						$the_blog_id = '1';
					}
				}else{
					$dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
					// getting blog id 
					$the_blog_id = '1';
				}

				if($get_site_id){
					// Trim URL to get specific part of multisite URL
					// "https://mediacleaner/wp-content/uploads/sites/2/2019/11/a_nature.jpg" but it store in database like "https://mediacleaner/my-second-site/wp-content/uploads/sites/2/2019/11/a_nature.jpg"

					$file_updated = substr($file, strpos($file, "/wp-content/") + 1);
					// Check if media is from library : IN SINGLE SITE
					$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $multisite_posts_table WHERE guid LIKE '%$file_updated'"));
					$attachmentid = @$attachment[0];
					if($attachmentid){
						$delete_attachment = $wpdb->query("DELETE FROM $multisite_posts_table WHERE ID = '$attachmentid'");
						if($delete_attachment){
							//Delete main attachment file from disk
							$delete_dir_attachment = unlink($dir_path);
							if($delete_dir_attachment){
								// Now delete all the resized images from directory START
								$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM $multisite_postsmeta_table where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
								$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
								if(@$unserializemetadata['sizes']){
									$mediabaseurl = substr($file, 0, strrpos( $file, '/'));
									foreach ($unserializemetadata['sizes'] as $metavalue) {
										$murl = $mediabaseurl.'/'.$metavalue['file'];
										$meta_dir_path = str_replace(get_site_url().'/',get_home_path(),$murl);
										$delArr[] = $murl;
										unlink($meta_dir_path);
									}
								}
								// Now delete all the resized images from directory ENDS
							}
						}
					}else{
						$delArr[] = $file;
						unlink($dir_path);
					}
				}else{
					// Check if media is from library : IN SINGLE SITE
					$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $multisite_posts_table WHERE guid='%s';", $file ));
					$attachmentid = @$attachment[0];
					if($attachmentid){
						$delete_attachment = wp_delete_attachment($attachmentid, true);
					}else{
						$delArr[] = $file;
						unlink($dir_path);
					}
				}
			}
		}
	}
	$result = json_encode($delArr);
	echo $result;
}