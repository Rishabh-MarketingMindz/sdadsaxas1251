<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
global $wpdb;
$media_src = $_POST['media_src'];
$type = $_POST['type'];
$action = $_POST['action'];
$media_prefix = $_POST['media_prefix'];
$get_site_id = $_POST['blog_id'];

// Restore original images which are saved in backup dir. during optimization start
$bulk_restore_images = array();
$i = 0;
foreach ($media_src as $mediavalue) {
  $bulk_restore_images[$i]['media_src'] = $mediavalue;
  $i++;
}
$j = 0;
foreach ($media_prefix as $mediavalues) {
  $bulk_restore_images[$j]['site_prefix'] = $mediavalues;
  $j++;
}
// Restore original images which are saved in backup dir. during optimization ends

if($get_site_id){
  $site_prefix = $wpdb->get_blog_prefix($get_site_id);
}else{
  $site_prefix = $wpdb->get_blog_prefix(1);
}
$destination_media_name = basename($media_src);

// URL of image where it will optimized (the orinigal path of the image)
$destination_dir_path = str_replace(get_site_url().'/',get_home_path(),$media_src);

// Backup directory
$directory_path = rtrim(get_home_path(), '/');
$wp_root_path = $directory_path.'/wp-content/plugins/wp_media_cleaner/backup/optimization/';

// URL of image where it will stored for backup (in the "optimization" folder)
$destination_media_url = $wp_root_path.$destination_media_name;
// Set the path of image by URL
define('DIRECTORY', $destination_dir_path);
if($type == "delete_original"){ // If need to delete original image
  if(file_exists($destination_media_url)){
    $is_unlink = unlink($destination_media_url);
    if($is_unlink){
      $optimization_backup = $wpdb->prefix."optimizer_backup";
      $if_optimized = $wpdb->get_results("SELECT * FROM $optimization_backup WHERE Image_url = '$media_src' AND type !='exclude'");
      if(count($if_optimized)>0){
        $delete_row = $wpdb->query("DELETE FROM $optimization_backup WHERE Image_url = '$media_src' AND type !='exclude'");
      }
      $resp = __("File is deleted successfully","wp_media_cleaner");
    }else{
      $resp = __("Please grant permission to plugin folder","wp_media_cleaner");
    }
  }else{
    $resp = __("File is not found.","wp_media_cleaner");
  }
  echo json_encode($resp);
}elseif($type == "restore_original_image"){ // Restore optimized image
  if($action == "single_restore_original_image"){
    $multisite_posts_table = $media_prefix."posts";
    $multisite_postsmeta_table = $media_prefix."postmeta";
    $delete_backup_table = $wpdb->prefix."optimizer_backup";

    $site_prefix = $media_prefix;
    $file = $media_src;
    preg_match('!\d+!', $site_prefix, $matches);
    $get_site_id = $matches[0];

    if(is_multisite()){
      // get main site path
      $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
      $mainsite_url = $current_blog_details->path;

      // get multi site path
      $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
      $multisite_url = $current_blog_details2->path;
    }
    if($get_site_id){
      $img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$file);
      $dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
      $if_optimized = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$media_src' AND type IS NULL AND optimization_amount IS NOT NULL");
      if(!empty($if_optimized)){
        $content = file_get_contents($destination_media_url);
        $is_backup = file_put_contents($dir_path, $content);
        chmod($dir_path, 0664);
        if($is_backup){
          $update_row = $wpdb->query( $wpdb->prepare("UPDATE $delete_backup_table SET type = 'restored_original' WHERE Image_url = '$media_src'"));
          if($update_row){
            $resp = __("File is restored successfully","wp_media_cleaner");
          }
        }
      }
    }else{
      $dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
      $if_optimized = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$media_src' AND type IS NULL AND optimization_amount IS NOT NULL");
      if(!empty($if_optimized)){
        $content = file_get_contents($destination_media_url);
        $is_backup = file_put_contents($dir_path, $content);
        chmod($dir_path, 0664);
        if($is_backup){
          $update_row = $wpdb->query( $wpdb->prepare("UPDATE $delete_backup_table SET type = 'restored_original' WHERE Image_url = '$media_src'"));
          if($update_row){
            $resp = __("File is restored successfully","wp_media_cleaner");
          }
        }
      }
    }
    echo $resp;
  }elseif($action == "bulk_restore_original_image"){    
    $restoreUrls = array();
    foreach ($bulk_restore_images as $bulk_restore_images_values){
      $file = $bulk_restore_images_values['media_src'];
      $site_prefix = $bulk_restore_images_values['site_prefix'];

      $destination_media_name = basename($file);
      // URL of image where it will stored for backup (in the "optimization" folder)
      $destination_media_url = $wp_root_path.$destination_media_name;

      $multisite_posts_table = $site_prefix."posts";
      $multisite_postsmeta_table = $site_prefix."postmeta";
      $delete_backup_table = $wpdb->prefix."optimizer_backup";      

      preg_match('!\d+!', $site_prefix, $matches);
      $get_site_id = $matches[0];

      if(is_multisite()){
        // get main site path
        $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
        $mainsite_url = $current_blog_details->path;

        // get multi site path
        $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
        $multisite_url = $current_blog_details2->path;
      }

      if($get_site_id){
        $img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$file);
        $dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
        $if_optimized = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$file' AND type IS NULL AND optimization_amount IS NOT NULL");
        if(!empty($if_optimized)){
          $content = file_get_contents($destination_media_url);
          $is_backup = file_put_contents($dir_path, $content);
          chmod($dir_path, 0664);
          if($is_backup){
            $update_row = $wpdb->query( $wpdb->prepare("UPDATE $delete_backup_table SET type = 'restored_original' WHERE Image_url = '$file'"));
            if($update_row){
              $restoreUrls[] = $file;
            }
          }
        }
      }else{
        $dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
        $if_optimized = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$file' AND type IS NULL AND optimization_amount IS NOT NULL");
        if(!empty($if_optimized)){
          $content = file_get_contents($destination_media_url);
          $is_backup = file_put_contents($dir_path, $content);
          chmod($dir_path, 0664);
          if($is_backup){
            $update_row = $wpdb->query( $wpdb->prepare("UPDATE $delete_backup_table SET type = 'restored_original' WHERE Image_url = '$file'"));
            if($update_row){
              $restoreUrls[] = $file;
            }
          }
        }
      }
    }
    if(!empty($restoreUrls)){
      $message = __("Images restored successfully");
      $response = array(
        'message'=> $message,
        'url'=> $file,
        'status' => 'success'
      );
    }
    $result = json_encode($response);
    echo $result;
  }
}elseif($type == "update_quality"){
  $img_quality = $_POST['compress_amount'];
  $image_type = $_POST['image_type'];
  $row_name = "wp_media_quality_".$image_type;
  $is_update_quality = update_option( $row_name, $img_quality );
  $resp = array("image_type"=>$image_type,"img_quality"=>$img_quality);
  echo json_encode($resp);
}elseif($type == "get_extensions_quality"){
  $getTheExtension = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."options WHERE option_name LIKE 'wp_media_quality_%'");
  $response = '';
  foreach ($getTheExtension as $imgInfo) {
    $option_name = $imgInfo->option_name;
    $image_type = str_replace("wp_media_quality_", "", $option_name);
    $quality = $imgInfo->option_value;
    if($image_type == 'png'){
      $response .= '
        <div class="range-slider">
          <label>'.$image_type.'</label>
          <input name="image_quality[]" data-format="'.$image_type.'" class="range-slider_range" type="range" value="'.$quality.'" min="0" max="9">
          <span class="range-slider_value">'.$quality.'</span>
        </div>
      ';
    }else{
      $response .= '
        <div class="range-slider">
          <label>'.$image_type.'</label>
          <input name="image_quality[]" data-format="'.$image_type.'" class="range-slider_range" type="range" value="'.$quality.'" min="0" max="100">
          <span class="range-slider_value">'.$quality.'</span>
        </div>
      ';
    }
  }
  echo $response;
}elseif($type == "update_original_image"){
  $keep_original = $_POST['keep_original'];
  if(!empty($keep_original)){
    $is_update_quality = update_option( "wp_media_keep_original", $keep_original[0] );
  }else{
    $is_update_quality = update_option( "wp_media_keep_original", NULL );
  }
  echo $is_update_quality;
}elseif($type == "excluded_media"){
  if($action == "bulk_media_exclude"){
    $table_name = $wpdb->prefix."optimizer_backup";
    $media_excluded = array();
    $media_src = array_unique($media_src);
    foreach ($media_src as $media_url) {
      $url_exist = $wpdb->get_results("SELECT * from $table_name WHERE type = 'exclude' AND Image_url = '$media_url'");
      $if_have = "";
      $if_have = count($url_exist);
      if($if_have>0){
        $delete_image = $wpdb->query("DELETE FROM $table_name WHERE type = 'exclude' AND Image_url = '$media_url'");        
        if(!empty($delete_image)){
          $media_excluded[] = array(
            'url' => $media_url,
            'actions' => 'not_excluded'
          );
        }
      }else{
        $update_image = $wpdb->insert( $table_name, array("Image_url" => $media_url,"type" => "exclude"));
        if(!empty($update_image)){
          $media_excluded[] = array(
            'url' => $media_url,
            'actions' => 'excluded'
          );
        }
      }
    }
    echo json_encode($media_excluded);
  }else{
    $exclude_action = $_POST['exclude_action'];
    $table_name = $wpdb->prefix."optimizer_backup";

    $url_exist = $wpdb->get_results("SELECT * from $table_name WHERE type = 'exclude' AND Image_url = '$media_src'");
    $if_have = count($url_exist);
    if($if_have>0){
      $update_image = $wpdb->query( $wpdb->prepare("DELETE FROM $table_name WHERE type = 'exclude' AND Image_url = '$media_src'"));
    }else{
      $update_image = $wpdb->insert( $table_name, array("Image_url" => $media_src,"type" => "exclude"));
    }
    if($update_image){
      echo "success";
    }else{
      echo "error";
    }
  }
}elseif($type == "restore_deleted_image"){ // Restore deleted images
  // if($action == "bulk_restore_images"){
  //   foreach ($bulk_restore_images as $bulk_restore_images_values) {
  //     $file = $bulk_restore_images_values['media_src'];
  //     $site_prefix = $bulk_restore_images_values['media_prefix'];

  //     $multisite_posts_table = $site_prefix."posts";
  //     $multisite_postsmeta_table = $site_prefix."postmeta";
  //     $delete_backup_table = $wpdb->prefix."optimizer_backup";

  //     // get main site path
  //     $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
  //     $mainsite_url = $current_blog_details->path;

  //     // get multi site path
  //     $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
  //     $multisite_url = $current_blog_details2->path;

  //     if($get_site_id != 1){
  //       // Restore image path
  //       $img_restore_path = str_replace(get_site_url().'/',get_home_path(),$file);

  //       // Get backup directory path with URL
  //       $deleted_backup_dir = plugin_dir_url('_FILE_').'wp_media_cleaner/backup/';
  //       $backup_url = $deleted_backup_dir.$destination_media_name;
  //       $backup_url = str_replace(get_site_url().'/',get_home_path(),$backup_url);

  //       $if_backup = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$file' AND (type = 'delete_backup' OR type = 'delete_backup_meta')");

  //       if(!empty($if_backup)){
  //         @$post_id = $if_backup[0]->media_id;
  //         if($post_id){
  //           $attachment = $wpdb->get_results("SELECT * FROM $multisite_posts_table WHERE ID = '$post_id'");        
  //           $attachment_named = @$attachment[0]->post_name;
  //           $attachment_name = str_replace('__trashed','',$attachment_named);        
  //           $untrash_media = $wpdb->query( $wpdb->prepare("UPDATE $multisite_posts_table SET post_status = 'inherit', post_name = '$attachment_name' WHERE ID = '$post_id'"));
  //           if($untrash_media){
  //             // Update post meta for untrash
  //             $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_desired_post_slug' AND post_id = '$post_id'");
  //             $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_trash_meta_time' AND post_id = '$post_id'");
  //             $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_trash_meta_status' AND post_id = '$post_id'");
  //             $wp_old_slug = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_old_slug", "meta_value" => $attachment_named, "post_id" => $post_id));

  //             $content = file_get_contents($backup_url);
  //             $is_backup = file_put_contents($img_restore_path, $content);
  //             if($is_backup){
  //               chmod($img_restore_path, 0664);
  //               $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$file' AND type = 'delete_backup'");
  //               $delArr[] = $file;
  //             }
  //             // Check if the post have meta URL, if have then restore them too
  //             $if_meta_urls = $wpdb->get_results("SELECT * FROM $delete_backup_table where media_id = '$post_id' AND type = 'delete_backup_meta'");
  //             if(!empty($if_meta_urls)){
  //               foreach ($if_meta_urls as $meta_value) {
  //                 $meta_image_url = $meta_value->Image_url;
  //                 $meta_backup_url = $deleted_backup_dir.basename($meta_image_url);
  //                 $meta_img_home_path = str_replace(get_site_url().'/',get_home_path(),$meta_image_url);
  //                 $meta_content = file_get_contents($meta_backup_url);
  //                 $meta_backup = file_put_contents($meta_img_home_path, $meta_content);
  //                 if($meta_backup){
  //                   chmod($meta_img_home_path, 0664);
  //                   $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$meta_image_url' AND type = 'delete_backup_meta'");
  //                   $delArr[] = $meta_image_url;
  //                 }
  //               }
  //             }        
  //           }
  //         }else{
  //           $content = file_get_contents($backup_url);
  //           $is_backup = file_put_contents($img_restore_path, $content);
  //           chmod($img_restore_path, 0664);
  //           $delArr[] = $file;
  //         }
  //       }else{
  //         if(file_exists($img_restore_path)){
  //           $content = file_get_contents($img_restore_path);
  //           $is_backup = file_put_contents($img_restore_path, $content);
  //           chmod($img_restore_path, 0664);
  //           if($is_backup){
  //             $delArr[] = $file;
  //           }
  //         }else{
  //           $message = __("Image is not found","wp_media_cleaner");
  //           $response = array(
  //             'message'=> $message,
  //             'url'=> $file,
  //             'status' => 'error'
  //           );
  //         }
  //       }
  //     }else{
  //       // Restore image path
  //       $img_restore_path = str_replace(get_site_url().'/',get_home_path(),$file);

  //       // Get backup directory path with URL
  //       $deleted_backup_dir = plugin_dir_url('_FILE_').'wp_media_cleaner/backup/';
  //       $backup_url = $deleted_backup_dir.$destination_media_name;
  //       $backup_url = str_replace(get_site_url().'/',get_home_path(),$backup_url);

  //       $dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
  //       $if_backup = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$file' AND (type = 'delete_backup' OR type = 'delete_backup_meta')");
  //       if(!empty($if_backup)){
  //         @$post_id = $if_backup[0]->media_id;
  //         if($post_id){
  //           $untrash_media = wp_untrash_post($post_id);
  //           if($untrash_media){
  //             $content = file_get_contents($backup_url);
  //             $is_backup = file_put_contents($img_restore_path, $content);
  //             if($is_backup){
  //               chmod($img_restore_path, 0664);
  //               $resp = "restored";
  //               $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$file' AND type = 'delete_backup'");
  //               $delArr[] = $file;
  //             }
  //             // Check if the post have meta URL, if have then restore them too
  //             $if_meta_urls = $wpdb->get_results("SELECT * FROM $delete_backup_table where media_id = '$post_id' AND type = 'delete_backup_meta'");
  //             if(!empty($if_meta_urls)){
  //               foreach ($if_meta_urls as $meta_value) {
  //                 $meta_image_url = $meta_value->Image_url;
  //                 $meta_backup_url = $deleted_backup_dir.basename($meta_image_url);
  //                 $meta_img_home_path = str_replace(get_site_url().'/',get_home_path(),$meta_image_url);
  //                 $meta_content = file_get_contents($meta_backup_url);
  //                 $meta_backup = file_put_contents($meta_img_home_path, $meta_content);
  //                 if($meta_backup){
  //                   chmod($meta_img_home_path, 0664);
  //                   $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$meta_image_url' AND type = 'delete_backup_meta'");
  //                   $delArr[] = $meta_image_url;
  //                 }
  //               }
  //             }        
  //           }
  //         }else{
  //           $content = file_get_contents($backup_url);
  //           $is_backup = file_put_contents($img_restore_path, $content);
  //           chmod($img_restore_path, 0664);
  //           $delArr[] = $file;
  //         }
  //       }else{
  //         if(file_exists($img_restore_path)){
  //           $content = file_get_contents($img_restore_path);
  //           $is_backup = file_put_contents($img_restore_path, $content);
  //           chmod($img_restore_path, 0664);
  //           $delArr[] = $file;
  //         }else{
  //           $message = __("Image is not found","wp_media_cleaner");
  //           $response = array(
  //             'message'=> $message,
  //             'url'=> $file,
  //             'status' => 'error'
  //           );
  //         }
  //       }
  //     }
  //     echo json_encode($response);
  //   }
  // }else{
  //   $multisite_posts_table = $site_prefix."posts";
  //   $multisite_postsmeta_table = $site_prefix."postmeta";
  //   $delete_backup_table = $wpdb->prefix."optimizer_backup";
  //   $file = $media_src;

  //   // get main site path
  //   $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
  //   $mainsite_url = $current_blog_details->path;

  //   // get multi site path
  //   $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
  //   $multisite_url = $current_blog_details2->path;

  //   if($get_site_id != 1){
  //     // Restore image path
  //     $img_restore_path = str_replace(get_site_url().'/',get_home_path(),$file);

  //     // Get backup directory path with URL
  //     $deleted_backup_dir = plugin_dir_url('_FILE_').'wp_media_cleaner/backup/';
  //     $backup_url = $deleted_backup_dir.$destination_media_name;
  //     $backup_url = str_replace(get_site_url().'/',get_home_path(),$backup_url);

  //     $if_backup = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$media_src' AND (type = 'delete_backup' OR type = 'delete_backup_meta')");

  //     if(!empty($if_backup)){
  //       @$post_id = $if_backup[0]->media_id;
  //       if($post_id){
  //         $attachment = $wpdb->get_results("SELECT * FROM $multisite_posts_table WHERE ID = '$post_id'");        
  //         $attachment_named = @$attachment[0]->post_name;
  //         $attachment_name = str_replace('__trashed','',$attachment_named);        
  //         $untrash_media = $wpdb->query( $wpdb->prepare("UPDATE $multisite_posts_table SET post_status = 'inherit', post_name = '$attachment_name' WHERE ID = '$post_id'"));
  //         if($untrash_media){
  //           // Update post meta for untrash
  //           $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_desired_post_slug' AND post_id = '$post_id'");
  //           $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_trash_meta_time' AND post_id = '$post_id'");
  //           $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_trash_meta_status' AND post_id = '$post_id'");
  //           $wp_old_slug = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_old_slug", "meta_value" => $attachment_named, "post_id" => $post_id));

  //           $content = file_get_contents($backup_url);
  //           $is_backup = file_put_contents($img_restore_path, $content);
  //           if($is_backup){
  //             chmod($img_restore_path, 0664);
  //             $resp = "restored";
  //             $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$media_src' AND type = 'delete_backup'");
  //           }
  //           // Check if the post have meta URL, if have then restore them too
  //           $if_meta_urls = $wpdb->get_results("SELECT * FROM $delete_backup_table where media_id = '$post_id' AND type = 'delete_backup_meta'");
  //           if(!empty($if_meta_urls)){
  //             foreach ($if_meta_urls as $meta_value) {
  //               $meta_image_url = $meta_value->Image_url;
  //               $meta_backup_url = $deleted_backup_dir.basename($meta_image_url);
  //               $meta_img_home_path = str_replace(get_site_url().'/',get_home_path(),$meta_image_url);
  //               $meta_content = file_get_contents($meta_backup_url);
  //               $meta_backup = file_put_contents($meta_img_home_path, $meta_content);
  //               if($meta_backup){
  //                 chmod($meta_img_home_path, 0664);
  //                 $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$meta_image_url' AND type = 'delete_backup_meta'");
  //               }
  //             }
  //           }        
  //         }
  //       }else{
  //         $content = file_get_contents($backup_url);
  //         $is_backup = file_put_contents($img_restore_path, $content);
  //         chmod($img_restore_path, 0664);
  //         $resp = "restored";
  //       }      
  //       if($resp){
  //         $message = __("Image restored successfully");
  //         $response = array(
  //             'message'=> $message,
  //             'url'=> $file,
  //             'status' => 'success'
  //           );
  //       }else{
  //         $message = __("Something went wrong, Please try again","wp_media_cleaner");
  //         $response = array(
  //             'message'=> $message,
  //             'url'=> $file,
  //             'status' => 'error'
  //           );
  //       }
  //     }else{
  //       if(file_exists($img_restore_path)){
  //         $content = file_get_contents($img_restore_path);
  //         $is_backup = file_put_contents($img_restore_path, $content);
  //         chmod($img_restore_path, 0664);
  //         $message = __("Image restored successfully");
  //         $response = array(
  //           'message'=> $message,
  //           'url'=> $file,
  //           'status' => 'success'
  //         );
  //       }else{
  //         $message = __("Image is not found","wp_media_cleaner");
  //         $response = array(
  //           'message'=> $message,
  //           'url'=> $file,
  //           'status' => 'error'
  //         );
  //       }
  //     }
  //   }else{
  //     // Restore image path
  //     $img_restore_path = str_replace(get_site_url().'/',get_home_path(),$media_src);

  //     // Get backup directory path with URL
  //     $deleted_backup_dir = plugin_dir_url('_FILE_').'wp_media_cleaner/backup/';
  //     $backup_url = $deleted_backup_dir.$destination_media_name;
  //     $backup_url = str_replace(get_site_url().'/',get_home_path(),$backup_url);

  //     $dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
  //     $if_backup = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$media_src' AND (type = 'delete_backup' OR type = 'delete_backup_meta')");
  //     if(!empty($if_backup)){
  //       @$post_id = $if_backup[0]->media_id;
  //       if($post_id){
  //         $untrash_media = wp_untrash_post($post_id);
  //         if($untrash_media){
  //           $content = file_get_contents($backup_url);
  //           $is_backup = file_put_contents($img_restore_path, $content);
  //           if($is_backup){
  //             chmod($img_restore_path, 0664);
  //             $resp = "restored";
  //             $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$media_src' AND type = 'delete_backup'");
  //           }
  //           // Check if the post have meta URL, if have then restore them too
  //           $if_meta_urls = $wpdb->get_results("SELECT * FROM $delete_backup_table where media_id = '$post_id' AND type = 'delete_backup_meta'");
  //           if(!empty($if_meta_urls)){
  //             foreach ($if_meta_urls as $meta_value) {
  //               $meta_image_url = $meta_value->Image_url;
  //               $meta_backup_url = $deleted_backup_dir.basename($meta_image_url);
  //               $meta_img_home_path = str_replace(get_site_url().'/',get_home_path(),$meta_image_url);
  //               $meta_content = file_get_contents($meta_backup_url);
  //               $meta_backup = file_put_contents($meta_img_home_path, $meta_content);
  //               if($meta_backup){
  //                 chmod($meta_img_home_path, 0664);
  //                 $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$meta_image_url' AND type = 'delete_backup_meta'");
  //               }
  //             }
  //           }        
  //         }
  //       }else{
  //         $content = file_get_contents($backup_url);
  //         $is_backup = file_put_contents($img_restore_path, $content);
  //         chmod($img_restore_path, 0664);
  //         $resp = "restored";
  //       }      
  //       if($resp){
  //         $message = __("Image restored successfully");
  //         $response = array(
  //             'message'=> $message,
  //             'url'=> $file,
  //             'status' => 'success'
  //           );
  //       }else{
  //         $message = __("Something went wrong, Please try again","wp_media_cleaner");
  //         $response = array(
  //             'message'=> $message,
  //             'url'=> $file,
  //             'status' => 'error'
  //           );
  //       }
  //     }else{
  //       if(file_exists($img_restore_path)){
  //         $content = file_get_contents($img_restore_path);
  //         $is_backup = file_put_contents($img_restore_path, $content);
  //         chmod($img_restore_path, 0664);
  //         $message = __("Image restored successfully");
  //         $response = array(
  //           'message'=> $message,
  //           'url'=> $file,
  //           'status' => 'success'
  //         );
  //       }else{
  //         $message = __("Image is not found","wp_media_cleaner");
  //         $response = array(
  //           'message'=> $message,
  //           'url'=> $file,
  //           'status' => 'error'
  //         );
  //       }
  //     }
  //   }
  //   echo json_encode($response);
  // }
  $multisite_posts_table = $site_prefix."posts";
  $multisite_postsmeta_table = $site_prefix."postmeta";
  $delete_backup_table = $wpdb->prefix."optimizer_backup";
  $file = $media_src;

  if(is_multisite()){
    // get main site path
    $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
    $mainsite_url = $current_blog_details->path;

    // get multi site path
    $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
    $multisite_url = $current_blog_details2->path;
  }

  if($get_site_id != 1){
    // Restore image path
    $img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$file);
    $img_restore_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);

    // Get backup directory path with URL
    $deleted_backup_dir = plugin_dir_url('_FILE_').'wp_media_cleaner/backup/';
    $backup_url = $deleted_backup_dir.$destination_media_name;
    $backup_url = str_replace(get_site_url().'/',get_home_path(),$backup_url);

    $if_backup = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$media_src' AND (type = 'delete_backup' OR type = 'delete_backup_meta')");

    if(!empty($if_backup)){
      @$post_id = $if_backup[0]->media_id;
      if($post_id){
        $attachment = $wpdb->get_results("SELECT * FROM $multisite_posts_table WHERE ID = '$post_id'");        
        $attachment_named = @$attachment[0]->post_name;
        $attachment_name = str_replace('__trashed','',$attachment_named);        
        $untrash_media = $wpdb->query( $wpdb->prepare("UPDATE $multisite_posts_table SET post_status = 'inherit', post_name = '$attachment_name' WHERE ID = '$post_id'"));
        if($untrash_media){
          // Update post meta for untrash
          $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_desired_post_slug' AND post_id = '$post_id'");
          $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_trash_meta_time' AND post_id = '$post_id'");
          $wpdb->query("DELETE FROM $multisite_postsmeta_table WHERE meta_key = '_wp_trash_meta_status' AND post_id = '$post_id'");
          $wp_old_slug = $wpdb->insert( $multisite_postsmeta_table, array("meta_key" => "_wp_old_slug", "meta_value" => $attachment_named, "post_id" => $post_id));

          $content = file_get_contents($backup_url);
          $is_backup = file_put_contents($img_restore_path, $content);
          if($is_backup){
            chmod($img_restore_path, 0664);
            $resp = "restored";
            $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$media_src' AND type = 'delete_backup'");
          }
          // Check if the post have meta URL, if have then restore them too
          $if_meta_urls = $wpdb->get_results("SELECT * FROM $delete_backup_table where media_id = '$post_id' AND type = 'delete_backup_meta'");
          if(!empty($if_meta_urls)){
            foreach ($if_meta_urls as $meta_value) {
              $meta_image_url = $meta_value->Image_url;
              $meta_backup_url = $deleted_backup_dir.basename($meta_image_url);
              $meta_img_home_path = str_replace(get_site_url().'/',get_home_path(),$meta_image_url);
              $meta_content = file_get_contents($meta_backup_url);
              $meta_backup = file_put_contents($meta_img_home_path, $meta_content);
              if($meta_backup){
                chmod($meta_img_home_path, 0664);
                $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$meta_image_url' AND type = 'delete_backup_meta'");
              }
            }
          }        
        }
      }else{
        $content = file_get_contents($backup_url);
        $is_backup = file_put_contents($img_restore_path, $content);
        chmod($img_restore_path, 0664);
        $resp = "restored";
      }      
      if($resp){
        $message = __("Image restored successfully");
        $response = array(
            'message'=> $message,
            'url'=> $file,
            'status' => 'success'
          );
      }else{
        $message = __("Something went wrong, Please try again","wp_media_cleaner");
        $response = array(
            'message'=> $message,
            'url'=> $file,
            'status' => 'error'
          );
      }
    }else{
      if(file_exists($img_restore_path)){
        $content = file_get_contents($img_restore_path);
        $is_backup = file_put_contents($img_restore_path, $content);
        chmod($img_restore_path, 0664);
        $message = __("Image restored successfully");
        $response = array(
          'message'=> $message,
          'url'=> $file,
          'status' => 'success'
        );
      }else{
        $message = __("Image is not found","wp_media_cleaner");
        $response = array(
          'message'=> $message,
          'url'=> $file,
          'status' => 'error'
        );
      }
    }
  }else{
    // Restore image path
    $img_restore_path = str_replace(get_site_url().'/',get_home_path(),$media_src);

    // Get backup directory path with URL
    $deleted_backup_dir = plugin_dir_url('_FILE_').'wp_media_cleaner/backup/';
    $backup_url = $deleted_backup_dir.$destination_media_name;
    $backup_url = str_replace(get_site_url().'/',get_home_path(),$backup_url);

    $dir_path = str_replace(get_site_url().'/',get_home_path(),$file);
    $if_backup = $wpdb->get_results("SELECT * FROM $delete_backup_table where Image_url = '$media_src' AND (type = 'delete_backup' OR type = 'delete_backup_meta')");
    if(!empty($if_backup)){
      @$post_id = $if_backup[0]->media_id;
      if($post_id){
        $untrash_media = wp_untrash_post($post_id);
        if($untrash_media){
          $content = file_get_contents($backup_url);
          $is_backup = file_put_contents($img_restore_path, $content);
          if($is_backup){
            chmod($img_restore_path, 0664);
            $resp = "restored";
            $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$media_src' AND type = 'delete_backup'");
          }
          // Check if the post have meta URL, if have then restore them too
          $if_meta_urls = $wpdb->get_results("SELECT * FROM $delete_backup_table where media_id = '$post_id' AND type = 'delete_backup_meta'");
          if(!empty($if_meta_urls)){
            foreach ($if_meta_urls as $meta_value) {
              $meta_image_url = $meta_value->Image_url;
              $meta_backup_url = $deleted_backup_dir.basename($meta_image_url);
              $meta_img_home_path = str_replace(get_site_url().'/',get_home_path(),$meta_image_url);
              $meta_content = file_get_contents($meta_backup_url);
              $meta_backup = file_put_contents($meta_img_home_path, $meta_content);
              if($meta_backup){
                chmod($meta_img_home_path, 0664);
                $wpdb->query("DELETE FROM $delete_backup_table WHERE media_id = '$post_id' AND Image_url = '$meta_image_url' AND type = 'delete_backup_meta'");
              }
            }
          }        
        }
      }else{
        $content = file_get_contents($backup_url);
        $is_backup = file_put_contents($img_restore_path, $content);
        chmod($img_restore_path, 0664);
        $resp = "restored";
      }      
      if($resp){
        $message = __("Image restored successfully");
        $response = array(
            'message'=> $message,
            'url'=> $file,
            'status' => 'success'
          );
      }else{
        $message = __("Something went wrong, Please try again","wp_media_cleaner");
        $response = array(
            'message'=> $message,
            'url'=> $file,
            'status' => 'error'
          );
      }
    }else{
      if(file_exists($img_restore_path)){
        $content = file_get_contents($img_restore_path);
        $is_backup = file_put_contents($img_restore_path, $content);
        chmod($img_restore_path, 0664);
        $message = __("Image restored successfully");
        $response = array(
          'message'=> $message,
          'url'=> $file,
          'status' => 'success'
        );
      }else{
        $message = __("Image is not found","wp_media_cleaner");
        $response = array(
          'message'=> $message,
          'url'=> $file,
          'status' => 'error'
        );
      }
    }
  }
  echo json_encode($response);
}