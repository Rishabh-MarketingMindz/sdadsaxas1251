<?php

class WPMC_Core {
	public $admin = null;
	public $last_analysis = null; //TODO: Is it actually used?
	public $engine = null;
	public $catch_timeout = true; // This will halt the plugin before reaching the PHP timeout.
	private $regex_file = '/[A-Za-z0-9-_,.\(\)\s]+[.]{1}(MIMETYPES)/';
	public $current_method = 'media';
	private $refcache = array();
	public $servername = null;
	public $upload_folder = null;
	public $contentDir = null; // becomes 'wp-content/uploads'
	private $check_content = null;
	private $check_postmeta = null;
	private $check_posts = null;
	private $check_widgets = null;
	private $debug_logs = null;
	public $site_url = null;

	public function __construct( $admin ) {
		$this->admin = $admin;
		$this->site_url = get_site_url();
		$this->current_method = get_option( 'wpmc_method', 'media' );
		$types = "jpg|jpeg|jpe|gif|png|tiff|bmp|csv|pdf|xls|xlsx|doc|docx|tiff|mp3|mp4|wav|lua";
		

		require __DIR__ . '/api.php';
		new WPMC_API( $this, $admin, $this->engine );
	}	
}

// Check if root directory and their subdirectory have writable permission
function directoryIsWritable($dir) {
  $dir = rtrim($dir,"/");
  global $resp;
  $dh = new DirectoryIterator($dir);   
  // Dirctary object
  $counter = 0;
  foreach ($dh as $item) {
    if (!$item->isDot()) {
      if ($item->isDir()) {
        $file_permission = substr(sprintf('%o', fileperms("$dir/$item")), -4);
        if($file_permission == "0777" OR $file_permission == "0644"){
          directoryIsWritable("$dir/$item");
        }else{
          $resp[] = $file_permission;
        }
      } else {
        if($item->isFile()){
          $fullpath = $dir . "/" . $item->getFilename();
          $file_permission = substr(sprintf('%o', fileperms($fullpath)), -4);
          if($file_permission == "0777" OR $file_permission == "0644"){
          }else{
            $resp[] = $file_permission;
          }
        }
      }
    }
  }
  return $resp;
}

// CODE FOR GETTING IMAGE THAT CAN BE DELETE IN AUTOMATIC IMAGE DELETER (UNLINKED IMAGES) STARTS - By Hemant
function getAllSitePrefixCore(){
	global $wpdb;
	$prefixes = array();
	$is_multisite = get_sites();
	if($is_multisite){
		$abcd = count($is_multisite);
		$count = 0;
		for ($i=2; $i <= $abcd; $i++) { 
			$prefixes[$count]['prefix'] = $wpdb->get_blog_prefix($i);
			$prefixes[$count]['multisite_id'] = $i;
			$count++;
		}
	}else{
		$prefixes = 
		array(
			0 => array("prefix" => "wp_")
		);
	}
	return $prefixes;
}
function getImageUrlbyIdCore($imgId){
	global $wpdb;
	$imgIdget = wp_get_attachment_url($imgId);
	if(!empty($imgIdget)){
		return $imgIdget;
	}else{
		return false;
	}
}
// Get Multisite Media URL by id
function getMultiSiteImageUrlbyIdCore($imgId,$prefixValue){
	global $wpdb;
	$imgIdget = $wpdb->get_results("SELECT guid from ".$prefixValue."posts WHERE ID = $imgId" );
	$imgIdget = $imgIdget[0]->guid;
	if(!empty($imgIdget)){
		return $imgIdget;
	}else{
		return false;
	}
}
// Images from Woocommerce products
function getproductdetailsCore($pid){
	global $wpdb;
	$pde = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE ID = '".$pid."'");
	return $pde[0];
}
function getMultisiteProductDetailsCore($pid,$sitePrefix){
	global $wpdb;
	$pde = $wpdb->get_results("SELECT * FROM ".$sitePrefix."posts WHERE ID = '".$pid."'");
	return $pde[0];
}
function getattachmentproductsCore(){
	$mediaexistdata = array();
	global $wpdb;
	$current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);
	$attachmentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where post_type='attachment'");
	if(!empty($attachmentdata)){
		$attachmentarray = array();		
		foreach ($attachmentdata as $key => $attachvalue) {
			$url_dir_path = str_replace(get_site_url().'/',$directory_path,$attachvalue->guid);
			$fetch_only_images = mime_content_type($url_dir_path);
			if(strpos($fetch_only_images, 'image') !== false){
				$media_id = $attachvalue->ID;
				$media_url = $attachvalue->guid;
				$mediauploaddate = $attachvalue->post_date;
				$media_name = substr($media_url, strrpos($media_url, '/') + 1);
				$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
							
				$attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'upload_date' =>$mediauploaddate,'media_url'=>$media_url,'source_from'=>'database');
			}
		}
	}
	if(!empty($attachmentarray)){
		foreach ($attachmentarray as $value) {
			$media_id = $value['media_id']; 
			$medianame = $value['media_name']; 
			$mediadate = $value['upload_date'];
			$media_url = $value['media_url'];
			$media_type = substr($media_url, strrpos($media_url, '.') + 1);
			$source_from = $value['source_from'];

			$productids = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_thumbnail_id' AND  meta_value ='".$media_id."'");
			$postId = $productids[0]->post_id;
			$getAllMeta = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE post_id = '$postId' AND meta_key = '_variation_description'");
			if($getAllMeta){
				// get variation product details
				$variant_attribute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE post_id = '$postId' AND meta_key LIKE '%attribute_%'");
				$variant_attributes = '';
				foreach ($variant_attribute as $variant_value) {
					$variant_attributes .= $variant_value->meta_value.' | ';
				}
				$attribute = rtrim($variant_attributes,'| ');
				// $attribute_color = get_post_meta($postId, 'attribute_color', true);
				$sku_variant = get_post_meta($postId, '_sku', true);
				if(!empty($productids)){
					$productidsarray = array_column($productids, 'post_id');
					foreach ($productidsarray as $proid) {
						$productdetails = getproductdetailsCore($proid);
						$post_categories = wp_get_post_terms($proid,'product_cat');
						if($post_categories){
							$post_cat = $post_categories[0]->name;
						}
						if(!empty($productdetails)){
							$media_url = stripslashes($media_url);
							$url_dir_path_prod = str_replace(get_site_url().'/',$directory_path,$media_url);
							$fetch_only_images_prod = mime_content_type($url_dir_path_prod);
							if(strpos($fetch_only_images_prod, 'image') !== false){
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $medianame,
									'src' => $media_url,
									'media_type' => $media_type,
									'title'=> $productdetails->post_title,
									'post_type' => $productdetails->post_type,
									'page_builder_name' => 'Variation Product',
									'post_category' => $post_cat,
									'variant_attribute' => $attribute,
									'variant_sku' => $sku_variant,
									'datetime' => $mediadate,
									'linked' =>'Yes',
									'source_from' => 'database',
									'site_name' => $wpdb->prefix
								);
							}
						}
					}
				}
			}else{
				if(!empty($productids)){
					$productidsarray = array_column($productids, 'post_id');				
					foreach ($productidsarray as $proid) {
						$productdetails = getproductdetailsCore($proid);
						$post_categories = wp_get_post_terms($proid,'product_cat');
						if($post_categories){
							$post_cat = $post_categories[0]->name;
						}
						if(!empty($productdetails)){
							$media_url = stripslashes($media_url);
							$url_dir_path_prod = str_replace(get_site_url().'/',$directory_path,$media_url);
							$fetch_only_images_prod = mime_content_type($url_dir_path_prod);
							if(strpos($fetch_only_images_prod, 'image') !== false){
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $medianame,
									'src' => $media_url,
									'media_type' => $media_type,
									'title'=> $productdetails->post_title,
									'post_type' => $productdetails->post_type,
									'page_builder_name' => 'Featured Image',
									'post_category' => $post_cat,
									'variant_attribute' => '',
									'variant_sku' => '',
									'datetime' => $mediadate,
									'linked' =>'Yes',
									'source_from' => 'database',
									'site_name' => $wpdb->prefix
								);
							}
						}
					}
				}
			}

			$pgde = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_product_image_gallery'  AND FIND_IN_SET(".$media_id.", meta_value)");
			if(!empty($pgde)){
				$productgids = array_column($pgde, 'post_id');
				foreach ($productgids as $valueg) {
					$productdetailsg = getproductdetailsCore($valueg);
					$post_categories = wp_get_post_terms($valueg,'product_cat');
					if($post_categories){
						$post_cat = $post_categories[0]->name;
					}
					if(!empty($productdetailsg)){
						$media_url = stripslashes($media_url);
						$url_dir_path_prod2 = str_replace(get_site_url().'/',$directory_path,$media_url);
						$fetch_only_images_prod2 = mime_content_type($url_dir_path_prod2);
						if(strpos($fetch_only_images_prod2, 'image') !== false){
							$mediaexistdata[] = array(
								'media_id' => $media_id,
								'medianame' => $medianame,
								'src' => $media_url,
								'media_type' => $media_type,
								'title'=> $productdetailsg->post_title,
								'post_type' => $productdetailsg->post_type,
								'page_builder_name' => 'Gallery Image',
								'post_category' => $post_cat,
								'variant_attribute' => '',
								'variant_sku' => '',
								'datetime' => $mediadate,
								'linked' =>'Yes',
								'source_from' => 'database',
								'site_name' => $wpdb->prefix
							);
						}
					}
				}
			}
		}
	}
	return $mediaexistdata;
}
// Get multisite product's Media
function getMultisiteAttachmentProductsCore(){
	$mediaexistdata = array();
	global $wpdb;
	$current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);
	$prefixes = getAllSitePrefixCore();
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		foreach (get_sites() as $all_sites) {
			if($all_sites->blog_id == $multisiteId){
				$multisite_url = $all_sites->path;
			}
			if($all_sites->blog_id == 1){
				$mainsite_url = $all_sites->path;
			}
        }
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;
		$attachmentdata = $wpdb->get_results("SELECT * from ".$prefixValue."posts where post_type='attachment'");
		if(!empty($attachmentdata)){
			$attachmentarray = array();		
			foreach ($attachmentdata as $key => $attachvalue) {
				$dir_path =  str_replace(get_site_url().'/',$directory_path,$attachvalue->guid);
		        $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
				$fetch_only_images = mime_content_type($multisite_url_dir);				
				if(strpos($fetch_only_images, 'image') !== false){
					$media_id = $attachvalue->ID;
					$media_url = $attachvalue->guid;
					$mediauploaddate = $attachvalue->post_date;
					$media_name = substr($media_url, strrpos($media_url, '/') + 1);
					$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
								
					$attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'upload_date' =>$mediauploaddate,'media_url'=>$media_url,'source_from'=>'database');
				}
			}
		}
		if(!empty($attachmentarray)){
			foreach ($attachmentarray as $value) {
				$prod_urls = stripslashes($value['media_url']);
				$url_dir_path_prod = str_replace(get_site_url().'/',$directory_path,$prod_urls);
				$multisite_url_dir_prod = str_replace($multisite_url, $mainsite_url, $url_dir_path_prod);
				$fetch_only_images_prod = mime_content_type($multisite_url_dir_prod);
				if(strpos($fetch_only_images_prod, 'image') !== false){
					$media_id = $value['media_id']; 
					$medianame = $value['media_name']; 
					$mediadate = $value['upload_date'];
					$media_url = $value['media_url'];
					$media_type = substr($media_url, strrpos($media_url, '.') + 1);
					$source_from = $value['source_from'];

					$productids = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key = '_thumbnail_id' AND  meta_value ='".$media_id."'");
					$postId = $productids[0]->post_id;
					$getAllMeta = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE post_id = '$postId' AND meta_key = '_variation_description'");				
					if($getAllMeta){
						// get variation product details
						$variant_attribute = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE post_id = '$postId' AND meta_key LIKE '%attribute_%'");
						$variant_attributes = '';
						foreach ($variant_attribute as $variant_value) {
							$variant_attributes .= $variant_value->meta_value.' | ';
						}
						$attribute = rtrim($variant_attributes,'| ');
						// $attribute_color = get_post_meta($postId, 'attribute_color', true);
						// $sku_variant = get_post_meta($postId, '_sku', true);
						$getting_sku_inmeta = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_sku' AND post_id = $postId" );
						$sku_variant = $getting_sku_inmeta[0]->meta_value;

						if(!empty($productids)){
							$productidsarray = array_column($productids, 'post_id');
							foreach ($productidsarray as $proid) {
								$productdetails = getMultisiteProductDetailsCore($proid,$prefixValue);
								// $post_categories = wp_get_post_terms($proid,'product_cat');
								$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$proid."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
								$post_categories = $wpdb->get_results($queryterms, OBJECT);

								if($post_categories){
									$post_cat = $post_categories[0]->name;
								}
								if(!empty($productdetails)){
									$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
									$mediaexistdata[] = array(
										'media_id' => $media_id,
										'medianame' => $medianame,
										'src' => $media_url,
										'media_type' => $media_type,
										'title'=> $productdetails->post_title,
										'post_type' => $productdetails->post_type,
										'page_builder_name' => 'Variation Product',
										'post_category' => $post_cat,
										'variant_attribute' => $attribute,
										'variant_sku' => $sku_variant,
										'datetime' => $mediadate,
										'linked' =>$linkedd,
										'source_from' => 'database',
										'site_name' => $prefixValue
									);
								}
							}
						}
					}else{
						if(!empty($productids)){
							$productidsarray = array_column($productids, 'post_id');										
							foreach ($productidsarray as $proid) {
								$productdetails = getMultisiteProductDetailsCore($proid,$prefixValue);
								$post_categories = wp_get_post_terms($proid,'product_cat');
								if($post_categories){
									$post_cat = $post_categories[0]->name;
								}
								if(!empty($productdetails)){
									$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
									$mediaexistdata[] = array(
										'media_id' => $media_id,
										'medianame' => $medianame,
										'src' => $media_url,
										'media_type' => $media_type,
										'title'=> $productdetails->post_title,
										'post_type' => $productdetails->post_type,
										'page_builder_name' => 'Featured Image',
										'post_category' => $post_cat,
										'variant_attribute' => '',
										'variant_sku' => '',
										'datetime' => $mediadate,
										'linked' =>$linkedd,
										'source_from' => 'database',
										'site_name' => $prefixValue
									);
								}
							}
						}
					}

					$pgde = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key = '_product_image_gallery'  AND FIND_IN_SET(".$media_id.", meta_value)");
					if(!empty($pgde)){
						$productgids = array_column($pgde, 'post_id');
						foreach ($productgids as $valueg) {
							$productdetailsg = getMultisiteProductDetailsCore($valueg,$prefixValue);
							$post_categories = wp_get_post_terms($valueg,'product_cat');
							if($post_categories){
								$post_cat = $post_categories[0]->name;
							}
							if(!empty($productdetailsg)){
								$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $medianame,
									'src' => $media_url,
									'media_type' => $media_type,
									'title'=> $productdetailsg['post_title'],
									'post_type' => $productdetailsg['post_type'],
									'page_builder_name' => 'Gallery Image',
									'post_category' => $post_cat,
									'variant_attribute' => '',
									'variant_sku' => '',
									'datetime' => $mediadate,
									'linked' =>$linkedd,
									'source_from' => 'database',
									'site_name' => $prefixValue
								);
							}
						}
					}
				}
			}
		}
	}
	return $mediaexistdata;
}
// Get media from page builder Coded start by Hemant
function getPageBuilderContentMediaCore(){
	global $wpdb;
	$current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);

	$pbContent = $wpdb->get_results("SELECT * from ".$wpdb->prefix."posts WHERE post_type !='revision'");
	$uniqueArr = array();
	$inc = 0;
	$uniqueArrs = array();
	$uniqueArrs2 = array();
	$uniqueArrs3 = array();
	$uniqueArred = array();
	// Total types of extensions
	$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
	foreach ($pbContent as $getvalues) {
		if(!empty($getvalues)){
			$elem_builder = get_post_meta($getvalues->ID, '_elementor_edit_mode', true);
			$vc_builder = get_post_meta($getvalues->ID, '_wpb_vc_js_status', true);
			$beaver_builder = get_post_meta($getvalues->ID, '_fl_builder_enabled', true);
			$siteorigin_builder = get_post_meta($getvalues->ID, 'panels_data', true);
			$brizy_builder = !empty(get_post_meta($getvalues->ID, 'brizy', true));
			$oxygen_builder = !empty(get_post_meta($getvalues->ID, 'ct_builder_shortcodes', true));
			$siteorigin_ck_builder = '';

			if(!empty($siteorigin_builder)){
				$siteorigin_ck_builder = "SiteOrigin";
			}
			$featuredImageArr = get_post_meta($getvalues->ID, '_thumbnail_id');
			@$featuredImagesId = $featuredImageArr[0];
			if($featuredImagesId){
				$featuredImageId = getImageUrlbyIdCore($featuredImagesId);
			}else{
				$featuredImageId = '';
			}
			$productgalleryArr = get_post_meta($getvalues->ID, '_product_image_gallery');
			@$productgalleryImg = $productgalleryArr[0];

			$the_title = $getvalues->post_title;
			$post_type = $getvalues->post_type;
			$the_content = $getvalues->post_content;

			if($elem_builder == 'builder'){
				$PageId = $getvalues->ID;
				$elementor_sql = get_post_meta($PageId, '_elementor_data');
				$elementor_content = $elementor_sql[0];
				
				// Get files url from URL & ID tag start
				$group4 = array();
				$group5 = array();
				$getsrcfiles = array();
				preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
				preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
				// Get files url from URL & ID tag end

				$imgArry = array();
				foreach ($group4[1] as $ElementorimageUrl) {
					$ElementorimageUrl = stripslashes($ElementorimageUrl);
					$url_dir_path1 = str_replace(get_site_url().'/',$directory_path,$ElementorimageUrl);
					$fetch_only_images1 = mime_content_type($url_dir_path1);
					if(strpos($fetch_only_images1, 'image') !== false){
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $ElementorimageUrl;
						$new_array['medianame'] = basename($ElementorimageUrl);
						$new_array['page_builder_name'] = "Elementor";
						if(file_exists($url_dir_path1)){
							$unixtime = filemtime($url_dir_path1);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
						}else{
							$new_array['datetime'] = "";
						}
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = '';
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['site_name'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				// Get files url from href tag start
				$gethreffiles = array();
				preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
				$hreffiles = array();
				if(!empty($gethreffiles[1])){
					foreach ($gethreffiles[1] as $hrefValues) {
						$hrefValues = stripslashes($hrefValues);
						$url_dir_path2 = str_replace(get_site_url().'/',$directory_path,$hrefValues);
						$fetch_only_images2 = mime_content_type($url_dir_path2);
						if(strpos($fetch_only_images2, 'image') !== false){							
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $hrefValues;
							$new_array['medianame'] = basename($hrefValues);
							$new_array['page_builder_name'] = "Elementor";
							if(file_exists($url_dir_path2)){
								$unixtime = filemtime($url_dir_path2);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				// Get files url from href tag end

				// Get files src from href tag start
				$srcfiles = array();
				preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
				if(!empty($getsrcfiles[1])){
					foreach ($getsrcfiles[1] as $srcValues) {
						$srcValues = stripslashes($srcValues);
						$url_dir_path3 = str_replace(get_site_url().'/',$directory_path,$srcValues);
						$fetch_only_images3 = mime_content_type($url_dir_path3);
						if(strpos($fetch_only_images3, 'image') !== false){							
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $srcValues;
							$new_array['medianame'] = basename($srcValues);
							$new_array['page_builder_name'] = "Elementor";
							if(file_exists($url_dir_path3)){
								$unixtime = filemtime($url_dir_path3);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				// Get files src from href tag start

				$contentimgid = implode(',', $imgArry);
				if($group5[1][0] != ""){
					$group5arr = explode(',', $group5[1][0]);
					foreach ($group5arr as $imggelid) {
						$getidurlimge  = getImageUrlbyIdCore($imggelid);
						$getidurlimge = stripslashes($getidurlimge);
						$url_dir_path4 = str_replace(get_site_url().'/',$directory_path,$getidurlimge);
						$fetch_only_images4 = mime_content_type($url_dir_path4);
						if(strpos($fetch_only_images4, 'image') !== false){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $getidurlimge;
							$new_array['medianame'] = basename($getidurlimge);
							$new_array['page_builder_name'] = "Elementor";
							if(file_exists($url_dir_path4)){
								$unixtime = filemtime($url_dir_path4);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
			}
			if($vc_builder == 'true'){
				$totalExtensionsAvailable = array('pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
				preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
				preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
				preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
				/* Raw HTML */
				preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
				$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
				if(!empty($RowHtmls)){
					foreach ($RowHtmls as $RowHtml) {
						$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
						if(function_exists('wpb_js_remove_wpautop')){
								$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
							}
						
						preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
						$RawimgUrl = str_replace(['?_=1'], [''], $group16[1]);
						$RawimgUrlArry = array_merge($getFiles1,$RawimgUrl);
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFilesVC[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFilesVC[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFilesVC[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFilesVC[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFilesVC[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFilesVC[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFilesVC[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFilesVC[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFilesVC[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFilesVC[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFilesVC[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFilesVC[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFilesVC[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFilesVC[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFilesVC[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFilesVC[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFilesVC[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFilesVC[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFilesVC[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFilesVC[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFilesVC[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFilesVC[] = $group29s;
					}
				}

				/* Raw HTML */
				$PageId = $getvalues->ID;
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFilesVC[] = $filesvalue;
					    }
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$bgimgUrl = str_replace(['(',')'], ['',''], $group10[1]);

				$VCmediaContent = array_unique(array_merge($group9[1],$bgimgUrl,$getFilesVC));
				
				$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
				$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
				$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
				$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

				if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
					$VCimageids = '';
					$comma = '';
					if($VCsingleimageids != ""){
						$VCimages = getImageUrlbyIdCore($VCsingleimageids);
						$VCimages = stripslashes($VCimages);
						$url_dir_path5 = str_replace(get_site_url().'/',$directory_path,$VCimages);
						$fetch_only_images5 = mime_content_type($url_dir_path5);
						if(strpos($fetch_only_images5, 'image') !== false){
							$new_array['media_id'] = $idvalue;
							$new_array['title'] = $the_title;
							$new_array['src'] = $VCimages;
							$new_array['medianame'] = basename($VCimages);
							$new_array['page_builder_name'] = "Visual Composer";
							if(file_exists($url_dir_path5)){
								$unixtime = filemtime($url_dir_path5);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
					if(!empty($VCmediaContent)){
						$incs = 0;
						foreach ($VCmediaContent as $VCmediaContentUrl) {
							$VCmediaContentUrl = stripslashes($VCmediaContentUrl);
							$url_dir_path6 = str_replace(get_site_url().'/',$directory_path,$VCmediaContentUrl);
							$fetch_only_images6 = mime_content_type($url_dir_path6);
							if(strpos($fetch_only_images6, 'image') !== false){								
								$image_base_name2 = basename($VCmediaContentUrl);
								$attachmentmetadata = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key='_wp_attachment_metadata'");
								$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
								$the_post_id = $attachmentmetadata[0]->post_id;
								foreach ($unserializemetadata['sizes'] as $the_data) {
									if($image_base_name2 == $the_data['file']){
										$get_main_image = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE ID = '$the_post_id'");
										$the_image_url2 = $get_main_image[0]->guid;
										$new_array['media_id'] = '';
										$new_array['title'] = $the_title;
										$new_array['src'] = $the_image_url2;
										$new_array['medianame'] = basename($the_image_url2);
										$new_array['page_builder_name'] = "Visual Composer";
										if(file_exists($url_dir_path6)){
											$unixtime = filemtime($url_dir_path6);
											$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
										}else{
											$new_array['datetime'] = "";
										}
										$new_array['post_type'] = $post_type;
										$new_array['post_category'] = '';
										$new_array['variant_attribute'] = '13';
										$new_array['variant_sku'] = '';
										$new_array['source_from'] = 'database';
										$new_array['linked'] = 'Yes';
										$new_array['site_name'] = $wpdb->prefix;
										$incs++;
										array_push($uniqueArr,$new_array);
									}
								}
							}
						}
					}

					if($VCgalleryimageids != ""){
						if($VCimageids != ''){ $comma = ','; }						
						$theGalIds = explode(",", $VCgalleryimageids);
						$inc = 0;
						foreach ($theGalIds as $idvalue) {
							$VCimage = getImageUrlbyIdCore($idvalue);
							$VCimage = stripslashes($VCimage);
							$url_dir_path7 = str_replace(get_site_url().'/',$directory_path,$VCimage);
							$fetch_only_images7 = mime_content_type($url_dir_path7);
							if(strpos($fetch_only_images7, 'image') !== false){
								$new_array['media_id'] = $idvalue;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage;
								$new_array['medianame'] = basename($VCimage);
								$new_array['page_builder_name'] = "Visual Composer";
								if(file_exists($url_dir_path7)){
									$unixtime = filemtime($url_dir_path7);
									$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
								}else{
									$new_array['datetime'] = "";
								}
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $wpdb->prefix;
								$inc++;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					
					if($VCcarouselimageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$theGalIds2 = explode(",", $VCcarouselimageids);
						$inc2 = 0;
						foreach ($theGalIds2 as $idvalue2) {
							$VCimage2 = getImageUrlbyIdCore($idvalue2);
							$VCimage2 = stripslashes($VCimage2);
							$url_dir_path8 = str_replace(get_site_url().'/',$directory_path,$VCimage2);
							$fetch_only_images8 = mime_content_type($url_dir_path8);
							if(strpos($fetch_only_images8, 'image') !== false){
								$new_array['media_id'] = $idvalue2;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage2;
								$new_array['medianame'] = basename($VCimage2);
								$new_array['page_builder_name'] = "Visual Composer";
								if(file_exists($url_dir_path8)){
									$unixtime = filemtime($url_dir_path8);
									$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
								}else{
									$new_array['datetime'] = "";
								}
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $wpdb->prefix;
								$inc2++;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					
					if($VChoverboximageids != ""){
						if($VCimageids != ''){ $comma = ','; }						
						$VCimage3 = getImageUrlbyIdCore($VChoverboximageids,$prefixValue);
						$VCimage3 = stripslashes($VCimage3);
						$url_dir_path9 = str_replace(get_site_url().'/',$directory_path,$VCimage3);
						$fetch_only_images9 = mime_content_type($url_dir_path9);
						if(strpos($fetch_only_images9, 'image') !== false){
							$new_array['media_id'] = $VChoverboximageids;
							$new_array['title'] = $the_title;
							$new_array['src'] = $VCimage3;
							$new_array['medianame'] = basename($VCimage3);
							$new_array['page_builder_name'] = "Visual Composer";
							if(file_exists($url_dir_path9)){
								$unixtime = filemtime($url_dir_path9);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
			}
			if($oxygen_builder){			
				if (strpos($oxygen_builder, 'attachment_url') !== false) {
					$get_result_img_array = array();
					preg_match_all('/"attachment_url"\h*:.*?\"(.*?)\"(?![^"\n]")/', $oxygen_builder, $get_result_img_array);
					$img_arrs = $get_result_img_array[1];
					$increment = 0;
					foreach ($img_arrs as $imgvalue1) {
						$imgvalue1 = stripslashes($imgvalue1);
						$url_dir_path10 = str_replace(get_site_url().'/',$directory_path,$imgvalue1);
						$fetch_only_images10 = mime_content_type($url_dir_path10);
						if(strpos($fetch_only_images10, 'image') !== false){
							if (strpos($imgvalue1, '",') !== false) {
								$imgvalue1 = trim($imgvalue1,'",');
							}elseif(strpos($imgvalue1, '"') !== false){
								$imgvalue1 = trim($imgvalue1,'"');
							}
							$uniqueArrs[$increment]['title'] = $the_title;
							$uniqueArrs[$increment]['src'] = $imgvalue1;
							$uniqueArrs[$increment]['medianame'] = basename($imgvalue1);
							$uniqueArrs[$increment]['page_builder_name'] = "Oxygen Builder";
							if(@filemtime($imgvalue1)){
								$uniqueArrs[$increment]['datetime'] = filemtime($imgvalue1);
							}else{
								$uniqueArrs[$increment]['datetime'] = $getvalues->post_date;
							}
							$uniqueArrs[$increment]['post_type'] = $post_type;
							$uniqueArrs[$increment]['post_category'] = '';
							$uniqueArrs[$increment]['variant_attribute'] = '';
							$uniqueArrs[$increment]['variant_sku'] = '';
							$uniqueArrs[$increment]['source_from'] = "database";
							$uniqueArrs[$increment]['linked'] = "Yes";
							$uniqueArrs[$increment]['site_name'] = $wpdb->prefix;
							$increment++;
						}
					}
				}
				if (strpos($oxygen_builder, 'image_ids') !== false) {
					$get_result_gallery_array = array();
					preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $oxygen_builder, $get_result_gallery_array);
					$gallery_array = $get_result_gallery_array[1];
					$gallery_images = explode(",",$gallery_array[0]);								
					foreach ($gallery_images as $img_id) {
						$get_attach_url = $wpdb->get_results("select guid from ".$wpdb->prefix."posts where post_type='attachment' and ID='$img_id'");
						$get_attach_urls[] = $get_attach_url[0];					
					}
					$increment2 = 0;
					foreach ($get_attach_urls as $srcvalue) {
						$srcvalueUrl = stripslashes($srcvalue->guid);
						$url_dir_path11 = str_replace(get_site_url().'/',$directory_path,$srcvalueUrl);
						$fetch_only_images11 = mime_content_type($url_dir_path11);
						if(strpos($fetch_only_images11, 'image') !== false){
							$uniqueArrs2[$increment2]['title'] = $getvalues->post_title;
							$uniqueArrs2[$increment2]['src'] = $srcvalueUrl;
							$uniqueArrs2[$increment2]['medianame'] = basename($srcvalueUrl);
							$uniqueArrs2[$increment2]['page_builder_name'] = "Oxygen Builder";
							if(@filemtime($srcvalueUrl)){
								$uniqueArrs2[$increment2]['datetime'] = filemtime($srcvalueUrl);
							}else{
								$uniqueArrs2[$increment2]['datetime'] = $getvalues->post_date;
							}
							$uniqueArrs2[$increment2]['post_type'] = $getvalues->post_type;
							$uniqueArrs2[$increment2]['post_category'] = '';
							$uniqueArrs2[$increment2]['variant_attribute'] = '';
							$uniqueArrs2[$increment2]['variant_sku'] = '';
							$uniqueArrs2[$increment2]['source_from'] = "database";
							$uniqueArrs2[$increment2]['linked'] = "Yes";
							$uniqueArrs2[$increment2]['site_name'] = $wpdb->prefix;
							$increment2++;
						}
					}
				}
				if (strpos($oxygen_builder, '"src":') !== false) {
					$get_result_src_array = array();
					preg_match_all('/"src"\h*:.*?\"(.*?)\"(?![^"\n]")/', $oxygen_builder, $get_result_src_array);
					$get_result_src_ars = $get_result_src_array[1];
					foreach ($get_result_src_ars as $imgkey => $valueImg) {
						if (strpos($valueImg, '",') !== false) {
							$valueImg = trim($valueImg,'",');
						}elseif(strpos($valueImg, '"') !== false){
							$valueImg = trim($valueImg,'"');
						}
						$valueImg = stripslashes($valueImg);
						$url_dir_path12 = str_replace(get_site_url().'/',$directory_path,$valueImg);
						$fetch_only_images12 = mime_content_type($url_dir_path12);
						if(strpos($fetch_only_images12, 'image') !== false){
							$uniqueArrs3[$imgkey]['title'] = $getvalues->post_title;
							$uniqueArrs3[$imgkey]['src'] = $valueImg;
							$uniqueArrs3[$imgkey]['medianame'] = basename($valueImg);
							$uniqueArrs3[$imgkey]['page_builder_name'] = "Oxygen Builder";
							if(@filemtime($valueImg)){
								$uniqueArrs3[$imgkey]['datetime'] = filemtime($valueImg);
							}else{
								$uniqueArrs3[$imgkey]['datetime'] = $getvalues->post_date;
							}
							$uniqueArrs3[$imgkey]['post_type'] = $getvalues->post_type;
							$uniqueArrs3[$imgkey]['post_category'] = '';
							$uniqueArrs3[$imgkey]['variant_attribute'] = '';
							$uniqueArrs3[$imgkey]['variant_sku'] = '';
							$uniqueArrs3[$imgkey]['source_from'] = "database";
							$uniqueArrs3[$imgkey]['linked'] = "Yes";
							$uniqueArrs3[$increment3]['site_name'] = $wpdb->prefix;
						}
					}
				}
				if($uniqueArrs && $uniqueArrs2){
					$oxygen_merge1 = array_merge($uniqueArrs,$uniqueArrs2);
				}else{
					if(!empty($uniqueArrs)){
						$oxygen_merge1 = $uniqueArrs;
					}elseif(!empty($uniqueArrs2)){
						$oxygen_merge1 = $uniqueArrs2;
					}else{
						$oxygen_merge1 = array();
					}
				}
				
				if($oxygen_merge1 && $uniqueArrs3){
					$uniqueArr = array_merge($oxygen_merge1,$uniqueArrs3);
				}else{
					if(!empty($oxygen_merge1)){
						$uniqueArr = $oxygen_merge1;
					}elseif(!empty($uniqueArrs3)){
						$uniqueArr = $uniqueArrs3;
					}else{
						$uniqueArr = array();
					}
				}
			}
			if($brizy_builder){
				$PageId = $getvalues->ID;
				$the_content = get_post_meta($PageId, 'brizy', true);
				$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
				$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
				preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
				preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);

				// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
				preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
				foreach ($case1_gethreffiles[1] as $case1_href_value){
					$case1_href_value = strtok($case1_href_value, " ");
					$case1_href_value = stripslashes($case1_href_value);
		            $case1_dir_path = str_replace(get_site_url().'/',$directory_path,$case1_href_value);
					if(file_exists($case1_dir_path)){
						$url_dir_path13 = str_replace(get_site_url().'/',$directory_path,$case1_href_value);
						$fetch_only_images13 = mime_content_type($url_dir_path13);
						if(strpos($fetch_only_images13, 'image') !== false){
							$hrefcase1[] = $case1_href_value;
						}
					}
				}
				// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
				preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
				foreach ($case2_gethreffiles[1] as $case2_href_value) {
					$case2_href_value = strtok($case2_href_value, " ");
					$case2_href_value = stripslashes($case2_href_value);
		            $case2_dir_path = str_replace(get_site_url().'/',$directory_path,$case2_href_value);
					if(file_exists($case2_dir_path)){
						$url_dir_path14 = str_replace(get_site_url().'/',$directory_path,$case2_href_value);
						$fetch_only_images14 = mime_content_type($url_dir_path14);
						if(strpos($fetch_only_images14, 'image') !== false){
							$hrefcase2[] = $case2_href_value;
						}
					}
				}				
				// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
				$srcfiles = array();
				preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
				foreach ($case3_gethreffiles[1] as $case3_href_value){
					$case3_href_value = strtok($case3_href_value, " ");
					$case3_href_value = stripslashes($case3_href_value);
		            $case3_dir_path = str_replace(get_site_url().'/',$directory_path,$case3_href_value);
					if(file_exists($case3_dir_path)){
						$url_dir_path15 = str_replace(get_site_url().'/',$directory_path,$case3_href_value);
						$fetch_only_images15 = mime_content_type($url_dir_path15);
						if(strpos($fetch_only_images15, 'image') !== false){
							$hrefcase3[] = $case3_href_value;
						}
					}
				}				
				// CASE 4 (data-href) : Get url when pattern is <a data-href=
				preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
				foreach ($case4_gethreffiles[1] as $case4_href_value){					
					$case4_href_value = strtok($case4_href_value, " ");
					$case4_data_href = utf8_decode(urldecode($case4_href_value));
					preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
					$hrefcase4 = array();
					foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
						$theLinkVal = str_replace("\",", "", $case4_data_href_val);
						$theLinkVal = stripslashes($theLinkVal);
						$case4_dir_path = str_replace(get_site_url().'/',$directory_path,$theLinkVal);
						if(file_exists($case4_dir_path)){
							$url_dir_path16 = str_replace(get_site_url().'/',$directory_path,$theLinkVal);
							$fetch_only_images16 = mime_content_type($url_dir_path16);
							if(strpos($fetch_only_images16, 'image') !== false){
								$hrefcase4[] = $theLinkVal;
							}
						}
					}					
				}				
				// $the_content = htmlspecialchars($the_content);

				// CASE 6 (image) : Get url when getting images
				$imagesNeme = array_merge($group12[1],$group14[1]);				
				foreach ($imagesNeme as $imageNeme) {						
					// Get Media id by Name only for brizy
					$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
					if(!empty($imgIdsquery)){
						$imagesIds = getImageUrlbyIdCore($imgIdsquery[0]->post_id);
						$imagesIds = stripslashes($imagesIds);
						$url_dir_path17 = str_replace(get_site_url().'/',$directory_path,$imagesIds);
						$fetch_only_images17 = mime_content_type($url_dir_path17);
						if(strpos($fetch_only_images17, 'image') !== false){
							$hrefcase6[] = $imagesIds;
						}
					}
				}
				$href_array = array_unique(array_merge($hrefcase1, $hrefcase2, $hrefcase3, $hrefcase4, $hrefcase6));				
				if(!empty($href_array)){
					foreach ($href_array as $brizy_value) {
						$hrefValues = stripslashes($brizy_value);
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $hrefValues;
						$new_array['medianame'] = basename($hrefValues);
						$new_array['page_builder_name'] = "Brizy";
						if(file_exists($url_dir_path17)){
								$unixtime = filemtime($url_dir_path17);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = '';
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['site_name'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
			}
			if($siteorigin_ck_builder == "SiteOrigin"){
				$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
				$PageId = $getvalues->ID;
				$the_content = get_post_meta($PageId, 'panels_data', true);		
				$siteorigenArray = array();		
				foreach ($the_content['widgets'] as $valueid) {
					// $siteorigenArray = array();
					// $getContentUrl = array();
					if(array_key_exists("attachment_id", $valueid)){
						$sitegetimgurlattachment_id = getImageUrlbyIdCore($valueid['attachment_id']);
						array_push($siteorigenArray, $sitegetimgurlattachment_id);
					}
					if(array_key_exists("ids", $valueid)){
						foreach ($valueid['ids'] as $galleryimg) {
							$sitegetimgurlids = getImageUrlbyIdCore($galleryimg);
							array_push($siteorigenArray, $sitegetimgurlids);
						}
					}
					if(array_key_exists("features", $valueid)){
						foreach ($valueid['features'] as $features_img) {
							$sitegetimgurlfeatures = getImageUrlbyIdCore($features_img['icon_image']);
							array_push($siteorigenArray, $sitegetimgurlfeatures);
						}
					}
					if(array_key_exists("image", $valueid)){
						$sitegetimgurlimage = getImageUrlbyIdCore($valueid['image']);
						array_push($siteorigenArray, $sitegetimgurlimage);
					}
					if(array_key_exists("frames", $valueid)){
						foreach ($valueid['frames'] as $frames_img) {
							$sitegetimgurlframes = getImageUrlbyIdCore($frames_img['background_image']);
							array_push($siteorigenArray, $sitegetimgurlframes);
						}
					}
					if(array_key_exists("content", $valueid)){
						$custom_html = $valueid['content'];
						preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
						foreach ($contentimg[1] as $imgurl){
							array_push($siteorigenArray, $imgurl);
						}
					}
					if(array_key_exists("content", $valueid)){
						preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
						foreach ($contentimgs[1] as $imgurls){
							array_push($siteorigenArray, $imgurls);
						}
					}			
					if(array_key_exists("text", $valueid)){						
						$siteEditor = $valueid['text'];
						preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
						preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
						$groupsfilehref = $groupsfile[1];
						$groupsfilesrc = $srcgroupsfile[1];
						if(!empty($groupsfilehref)){
							foreach ($groupsfilehref as $filesvalue) {								
								$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvalue;
							    }
							}
						}
						if(!empty($groupsfilesrc)){
							foreach ($groupsfilesrc as $filesvaluesrc) {
								$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvaluesrc;
							    }
							}
						}
						preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
						preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
						preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
						preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
						preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
						preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
						preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
						preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
						preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
						preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
						preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
						preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
						preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
						preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
						preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
						preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
						preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
						preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
						preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
						// for vc video
						if(!empty($vc_video_url[1])){
							foreach ($vc_video_url[1] as $vc_video) {
								$getContentUrl[] = $vc_video;
							}
						}
						// for src
						if(!empty($group9[1])){
							foreach ($group9[1] as $group9s) {
								$getContentUrl[] = $group9s;
							}
						}
						// for mp3
						if(!empty($group11[1])){
							foreach ($group11[1] as $group11s) {
								$getContentUrl[] = $group11s;
							}
						}
						// for mp4
						if(!empty($group12[1])){
							foreach ($group12[1] as $group12s) {
								$getContentUrl[] = $group12s;
							}
						}
						// for pdf
						if(!empty($group13[1])){
							foreach ($group13[1] as $group13s) {
								$getContentUrl[] = $group13s;
							}
						}
						// for docx
						if(!empty($group14[1])){
							foreach ($group14[1] as $group14s) {
								$getContentUrl[] = $group14s;
							}
						}
						// for doc
						if(!empty($group15[1])){
							foreach ($group15[1] as $group15s) {
								$getContentUrl[] = $group15s;
							}
						}
						// for ppt
						if(!empty($group16[1])){
							foreach ($group16[1] as $group16s) {
								$getContentUrl[] = $group16s;
							}
						}
						// for xls
						if(!empty($group17[1])){
							foreach ($group17[1] as $group17s) {
								$getContentUrl[] = $group17s;
							}
						}
						// for pps
						if(!empty($group18[1])){
							foreach ($group18[1] as $group18s) {
								$getContentUrl[] = $group18s;
							}
						}
						// for ppsx
						if(!empty($group19[1])){
							foreach ($group19[1] as $group19s) {
								$getContentUrl[] = $group19s;
							}
						}
						// for xlsx
						if(!empty($group20[1])){
							foreach ($group20[1] as $group20s) {
								$getContentUrl[] = $group20s;
							}
						}
						// for odt
						if(!empty($group21[1])){
							foreach ($group21[1] as $group21s) {
								$getContentUrl[] = $group21s;
							}
						}
						// for ogg
						if(!empty($group22[1])){
							foreach ($group22[1] as $group22s) {
								$getContentUrl[] = $group22s;
							}
						}
						// for m4a
						if(!empty($group23[1])){
							foreach ($group23[1] as $group23s) {
								$getContentUrl[] = $group23s;
							}
						}
						// for wav
						if(!empty($group24[1])){
							foreach ($group24[1] as $group24s) {
								$getContentUrl[] = $group24s;
							}
						}
						// for mp4
						if(!empty($group25[1])){
							foreach ($group25[1] as $group25s) {
								$getContentUrl[] = $group25s;
							}
						}
						// for mov
						if(!empty($group26[1])){
							foreach ($group26[1] as $group26s) {
								$getContentUrl[] = $group26s;
							}
						}
						// for avi
						if(!empty($group27[1])){
							foreach ($group27[1] as $group27s) {
								$getContentUrl[] = $group27s;
							}
						}
						// for 3gp
						if(!empty($group28[1])){
							foreach ($group28[1] as $group28s) {
								$getContentUrl[] = $group28s;
							}
						}
						// for pptx
						if(!empty($group29[1])){
							foreach ($group29[1] as $group29s) {
								$getContentUrl[] = $group29s;
							}
						}
					}
					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					if(!empty($group10)){
						$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
						if(!empty($bgimgUrl1)){
							foreach ($bgimgUrl1 as $filesvaluebackground) {								
								$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvaluebackground;
							    }
							}
						}
					}
					$VCmediaContent = array_unique($getContentUrl);
					$siteorigenArray = array_unique($siteorigenArray);
					$siteOriginUrl = array();
					if(!empty($VCmediaContent) && !empty($siteorigenArray)){
						$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
					}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
						$siteOriginUrl = $VCmediaContent;
					}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){
						$siteOriginUrl = $siteorigenArray;
					}
					$final_array = array();
					foreach ($siteOriginUrl as $thevalues) {
						array_push($final_array, $thevalues);
					}
				}
				foreach ($final_array as $siteOriginUrlValue) {
					$SiteOriginimageUrl = stripslashes($siteOriginUrlValue);
					$url_dir_path18 = str_replace(get_site_url().'/',$directory_path,$SiteOriginimageUrl);
					$fetch_only_images18 = mime_content_type($url_dir_path18);
					if(strpos($fetch_only_images18, 'image') !== false){
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $SiteOriginimageUrl;
						$new_array['medianame'] = basename($SiteOriginimageUrl);
						$new_array['page_builder_name'] = "Site Origin";

						$siteorigin_url_dir_path =  str_replace(get_site_url().'/',$directory_path,$SiteOriginimageUrl);
						if(file_exists($siteorigin_url_dir_path)){
							$unixtime = filemtime($siteorigin_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
						}else{
							$new_array['datetime'] = "";
						}					
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = '';
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['site_name'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
			}
			if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder && $post_type !='attachment'){
				$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;			
				$PageId = $getvalues->ID;

				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for wmv
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

				if(!empty($gutenbergImgUrls) && !empty($getFiles)){
					$hrefMedias = array_unique(array_merge($getFiles,$gutenbergImgUrls));
				}else{
					if(!empty($getFiles)){
						$hrefMedias = array_unique($getFiles);
					}elseif(!empty($gutenbergImgUrls)){
						$hrefMedias = array_unique($gutenbergImgUrls);
					}else{
						$hrefMedias = array();
					}
				}
				if(!empty($gutenbergImgUrls) && !empty($hrefMedias)){
					$totalContentMedia = array_unique(array_merge($gutenbergImgUrls,$hrefMedias));
				}else{
					if(empty($gutenbergImgUrls) && !empty($hrefMedias)){
						$totalContentMedia = $hrefMedias;
					}elseif(!empty($gutenbergImgUrls) && empty($hrefMedias)){
						$totalContentMedia = $gutenbergImgUrls;
					}
				}
				$gutenbergImgids = array();
				if(!empty($totalContentMedia)){
					foreach ($totalContentMedia as $gutenbergImgUrl) {
						array_push($gutenbergImgids, $gutenbergImgUrl);
					}
				}
				if(!empty($gutenbergImgids)){
					foreach ($gutenbergImgids as $imagesids) {
						$simple_images = stripslashes($imagesids);
						$image_base_name = basename($simple_images);

						$attachmentmetadata = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key='_wp_attachment_metadata'");
						$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
						$the_post_id = $attachmentmetadata[0]->post_id;
						foreach ($unserializemetadata['sizes'] as $the_data) {
							if($image_base_name == $the_data['file']){
								$get_main_image = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE ID = '$the_post_id'");
								$the_image_url = $get_main_image[0]->guid;
								$the_image_url = stripslashes($the_image_url);
								$url_dir_path = str_replace(get_site_url().'/',$directory_path,$the_image_url);
								$fetch_only_images = mime_content_type($url_dir_path);
								if(strpos($fetch_only_images, 'image') !== false){
									$new_array_simple['media_id'] = '';
									$new_array_simple['title'] = $the_title;
									$new_array_simple['src'] = $the_image_url;
									$new_array_simple['medianame'] = basename($simple_images);
									$new_array_simple['page_builder_name'] = "Simple/Gutenberg Content Media";

									$siteorigin_url_dir_path =  str_replace(get_site_url().'/',$directory_path,$simple_images);
									if(file_exists($siteorigin_url_dir_path)){
										$unixtime = filemtime($siteorigin_url_dir_path);
										$new_array_simple['datetime'] = date("Y-m-d h:i:s",$unixtime);
									}else{
										$new_array_simple['datetime'] = "";
									}					
									$new_array_simple['post_type'] = $post_type;
									$new_array_simple['post_category'] = '';
									$new_array_simple['variant_attribute'] = '';
									$new_array_simple['variant_sku'] = '';
									$new_array_simple['source_from'] = 'database';
									$new_array_simple['linked'] = 'Yes';
									$new_array_simple['site_name'] = $wpdb->prefix;
									array_push($uniqueArr,$new_array_simple);
								}
							}
						}
					}
				}
			}
		}
	}
	return $uniqueArr;
}
// Get multsite media from page builder Coded start by Hemant
function getMultiSitePageBuilderContentMediaCore(){
	global $wpdb;
	$current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);
	$uniqueArr = array();
	$new_array = array();
	$prefixes = getAllSitePrefixCore();
	// Total types of extensions
	$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		foreach (get_sites() as $all_sites) {
			if($all_sites->blog_id == $multisiteId){
				$multisite_url = $all_sites->path;
			}
			if($all_sites->blog_id == 1){
				$mainsite_url = $all_sites->path;
			}
        }
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;
		$pbContent = $wpdb->get_results("SELECT * from ".$prefixValue."posts WHERE post_type !='revision'");
		$inc = 0;
		foreach ($pbContent as $getvalues) {
			if(!empty($getvalues)){
				$the_title = $getvalues->post_title;
				$post_type = $getvalues->post_type;

				$elem_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_edit_mode' AND post_id=$getvalues->ID" );
				$elem_builder = $elem_builder[0]->meta_value;

				$vc_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_wpb_vc_js_status' AND post_id=$getvalues->ID" );
				$vc_builder = $vc_builder[0]->meta_value;

				$beaver_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_fl_builder_enabled' AND post_id=$getvalues->ID" );
				$beaver_builder = $beaver_builder[0]->meta_value;

				$siteorigin_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id=$getvalues->ID" );
				$siteorigin_builder = $siteorigin_builder[0]->meta_value;

				$brizy_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='brizy' AND post_id=$getvalues->ID" );
				$brizy_builder = $brizy_builder[0]->meta_value;

				$oxygen_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id=$getvalues->ID" );
				$oxygen_builder = $oxygen_builder[0]->meta_value;

				$siteorigin_ck_builder = '';
				if(!empty($siteorigin_builder)){
					$siteorigin_ck_builder = "SiteOrigin";
				}
				
				$featuredImageArr = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_thumbnail_id' AND post_id=$getvalues->ID" );
				@$featuredImagesId = $featuredImageArr[0]->meta_value;

				if($featuredImagesId){
					$featuredImageId = getMultiSiteImageUrlbyIdCore($featuredImagesId,$prefixValue);
				}else{
					$featuredImageId = '';
				}

				$productgalleryArr = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_product_image_gallery' AND post_id=$getvalues->ID" );
				@$productgalleryImg = $productgalleryArr[0]->meta_value;

				$the_title = $getvalues->post_title;
				$post_type = $getvalues->post_type;
				$the_content = $getvalues->post_content;

				if($elem_builder == 'builder'){
					$PageId = $getvalues->ID;
					$elementor_sql = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_data' AND post_id=$getvalues->ID" );
					@$elementor_content = $elementor_sql[0]->meta_value;
					
					// Get files url from URL & ID tag start
					$group4 = array();
					$group5 = array();
					$getsrcfiles = array();
					preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
					preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
					// Get files url from URL & ID tag end

					$imgArry = array();
					foreach ($group4[1] as $ElementorimageUrl) {
						$ElementorimageUrl = stripslashes($ElementorimageUrl);
						$url_dir_path1 = str_replace(get_site_url().'/',$directory_path,$ElementorimageUrl);
						$multisite_url_dir1 = str_replace($multisite_url, $mainsite_url, $url_dir_path1);
						$fetch_only_images1 = mime_content_type($multisite_url_dir1);
						if(strpos($fetch_only_images1, 'image') !== false){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $ElementorimageUrl;
							$new_array['medianame'] = basename($ElementorimageUrl);
							$new_array['page_builder_name'] = "Elementor";
							$new_array['datetime'] = filemtime($ElementorimageUrl);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
					// Get files url from href tag start
					$gethreffiles = array();
					preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
					$hreffiles = array();
					if(!empty($gethreffiles[1])){
						foreach ($gethreffiles[1] as $hrefValues) {
							$hrefValues = stripslashes($hrefValues);
							$url_dir_path2 = str_replace(get_site_url().'/',$directory_path,$hrefValues);
							$multisite_url_dir2 = str_replace($multisite_url, $mainsite_url, $url_dir_path2);
							$fetch_only_images2 = mime_content_type($multisite_url_dir2);
							if(strpos($fetch_only_images2, 'image') !== false){								
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $hrefValues;
								$new_array['medianame'] = basename($hrefValues);
								$new_array['page_builder_name'] = "Elementor";
								$new_array['datetime'] = filemtime($hrefValues);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// Get files url from href tag end

					// Get files src from href tag start
					$srcfiles = array();
					preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
					if(!empty($getsrcfiles[1])){
						foreach ($getsrcfiles[1] as $srcValues) {
							$srcValues = stripslashes($srcValues);
							$url_dir_path3 = str_replace(get_site_url().'/',$directory_path,$srcValues);
							$multisite_url_dir3 = str_replace($multisite_url, $mainsite_url, $url_dir_path3);
							$fetch_only_images3 = mime_content_type($multisite_url_dir3);
							if(strpos($fetch_only_images3, 'image') !== false){								
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $srcValues;
								$new_array['medianame'] = basename($srcValues);
								$new_array['page_builder_name'] = "Elementor";
								$new_array['datetime'] = filemtime($srcValues);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// Get files src from href tag start

					$contentimgid = implode(',', $imgArry);
					if($group5[1][0] != ""){
						$group5arr = explode(',', $group5[1][0]);
						foreach ($group5arr as $imggelid) {
							$getidurlimge  = getMultiSiteImageUrlbyIdCore($imggelid,$prefixValue);
							$getidurlimge = stripslashes($getidurlimge);
							$url_dir_path4 = str_replace(get_site_url().'/',$directory_path,$getidurlimge);
							$multisite_url_dir4 = str_replace($multisite_url, $mainsite_url, $url_dir_path4);
							$fetch_only_images4 = mime_content_type($multisite_url_dir4);
							if(strpos($fetch_only_images4, 'image') !== false){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $getidurlimge;
								$new_array['medianame'] = basename($getidurlimge);
								$new_array['page_builder_name'] = "Elementor";
								$new_array['datetime'] = filemtime($getidurlimge);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
				}
				if($vc_builder == 'true'){
					$totalExtensionsAvailable = array('pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;
					preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
					preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
					preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
					preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
					/* Raw HTML */
					preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
					$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
					if(!empty($RowHtmls)){
						foreach ($RowHtmls as $RowHtml) {
							$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
							if(function_exists('wpb_js_remove_wpautop')){
								$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
							}
							
							preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
							$RawimgUrl = str_replace(['?_=1'], [''], $group16[1]);
							$RawimgUrlArry = array_merge($getFiles1,$RawimgUrl);
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
					preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFilesVC[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFilesVC[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFilesVC[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFilesVC[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFilesVC[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFilesVC[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFilesVC[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFilesVC[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFilesVC[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFilesVC[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFilesVC[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFilesVC[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFilesVC[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFilesVC[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFilesVC[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFilesVC[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFilesVC[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFilesVC[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFilesVC[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFilesVC[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFilesVC[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFilesVC[] = $group29s;
						}
					}

					/* Raw HTML */
					$PageId = $getvalues->ID;
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
					$groupsfile = $groupsfile[1];
					if(!empty($groupsfile)){
						foreach ($groupsfile as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFilesVC[] = $filesvalue;
						    }
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					$bgimgUrl = str_replace(['(',')'], ['',''], $group10[1]);

					$VCmediaContent = array_unique(array_merge($group9[1],$bgimgUrl,$getFilesVC));
					
					$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
					$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
					$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
					$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

					if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
						$VCimageids = '';
						$comma = '';
						if($VCsingleimageids != ""){
							$VCimages = getMultiSiteImageUrlbyIdCore($VCsingleimageids,$prefixValue);
							$VCimages = stripslashes($VCimages);
							$url_dir_path5 = str_replace(get_site_url().'/',$directory_path,$VCimages);
							$multisite_url_dir5 = str_replace($multisite_url, $mainsite_url, $url_dir_path5);
							$fetch_only_images5 = mime_content_type($multisite_url_dir5);
							if(strpos($fetch_only_images5, 'image') !== false){
								$new_array['media_id'] = $idvalue;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimages;
								$new_array['medianame'] = basename($VCimages);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = filemtime($VCimages);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
						if(!empty($VCmediaContent)){
							$incs = 0;
							foreach ($VCmediaContent as $VCmediaContentUrl) {
								$VCmediaContentUrl = stripslashes($VCmediaContentUrl);
								$url_dir_path6 = str_replace(get_site_url().'/',$directory_path,$VCmediaContentUrl);
								$multisite_url_dir6 = str_replace($multisite_url, $mainsite_url, $url_dir_path6);
								$fetch_only_images6 = mime_content_type($multisite_url_dir6);
								if(strpos($fetch_only_images6, 'image') !== false){								
									$image_base_name2 = basename($VCmediaContentUrl);
									$attachmentmetadata = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key='_wp_attachment_metadata'");
									$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
									$the_post_id = $attachmentmetadata[0]->post_id;
									foreach ($unserializemetadata['sizes'] as $the_data) {
										if($image_base_name2 == $the_data['file']){
											$get_main_image = $wpdb->get_results("SELECT * FROM ".$prefixValue."posts WHERE ID = '$the_post_id'");
											$the_image_url2 = $get_main_image[0]->guid;
											$new_array['media_id'] = '';
											$new_array['title'] = $the_title;
											$new_array['src'] = $the_image_url2;
											$new_array['medianame'] = basename($the_image_url2);
											$new_array['page_builder_name'] = "Visual Composer";
											$new_array['datetime'] = filemtime($the_image_url2);
											$new_array['post_type'] = $post_type;
											$new_array['post_category'] = '';
											$new_array['variant_attribute'] = '13';
											$new_array['variant_sku'] = '';
											$new_array['source_from'] = 'database';
											$new_array['linked'] = 'Yes';
											$new_array['site_name'] = $prefixValue;
											$incs++;
											array_push($uniqueArr,$new_array);
										}
									}
								}
							}
						}

						if($VCgalleryimageids != ""){
							if($VCimageids != ''){ $comma = ','; }						
							$theGalIds = explode(",", $VCgalleryimageids);
							$inc = 0;
							foreach ($theGalIds as $idvalue) {
								$VCimage = getMultiSiteImageUrlbyIdCore($idvalue,$prefixValue);
								$VCimage = stripslashes($VCimage);
								$url_dir_path7 = str_replace(get_site_url().'/',$directory_path,$VCimage);
								$multisite_url_dir7 = str_replace($multisite_url, $mainsite_url, $url_dir_path7);
								$fetch_only_images7 = mime_content_type($multisite_url_dir7);
								if(strpos($fetch_only_images7, 'image') !== false){
									$new_array['media_id'] = $idvalue;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage;
									$new_array['medianame'] = basename($VCimage);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = filemtime($VCimage);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = '';
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['site_name'] = $prefixValue;
									$inc++;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						
						if($VCcarouselimageids != ""){
							if($VCimageids != ''){ $comma = ','; }
							$theGalIds2 = explode(",", $VCcarouselimageids);
							$inc2 = 0;
							foreach ($theGalIds2 as $idvalue2) {
								$VCimage2 = getMultiSiteImageUrlbyIdCore($idvalue2,$prefixValue);
								$VCimage2 = stripslashes($VCimage2);
								$url_dir_path8 = str_replace(get_site_url().'/',$directory_path,$VCimage2);
								$multisite_url_dir8 = str_replace($multisite_url, $mainsite_url, $url_dir_path8);
								$fetch_only_images8 = mime_content_type($multisite_url_dir8);
								if(strpos($fetch_only_images8, 'image') !== false){
									$new_array['media_id'] = $idvalue2;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage2;
									$new_array['medianame'] = basename($VCimage2);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = filemtime($VCimage2);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = '';
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['site_name'] = $prefixValue;
									$inc2++;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						
						if($VChoverboximageids != ""){
							if($VCimageids != ''){ $comma = ','; }						
							$VCimage3 = getMultiSiteImageUrlbyIdCore($VChoverboximageids,$prefixValue);
							$VCimage3 = stripslashes($VCimage3);
							$url_dir_path9 = str_replace(get_site_url().'/',$directory_path,$VCimage3);
							$multisite_url_dir9 = str_replace($multisite_url, $mainsite_url, $url_dir_path9);
							$fetch_only_images9 = mime_content_type($multisite_url_dir9);
							if(strpos($fetch_only_images9, 'image') !== false){
								$new_array['media_id'] = $VChoverboximageids;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage3;
								$new_array['medianame'] = basename($VCimage3);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = filemtime($VCimage3);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = '';
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['site_name'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
				}
				if($oxygen_builder){			
					if (strpos($oxygen_builder, 'attachment_url') !== false) {
						$get_result_img_array = array();
						preg_match_all('/"attachment_url"\h*:.*?\"(.*?)\"(?![^"\n]")/', $oxygen_builder, $get_result_img_array);
						$img_arrs = $get_result_img_array[1];
						$increment = 0;
						foreach ($img_arrs as $imgvalue1) {
							$imgvalue1 = stripslashes($imgvalue1);
							$url_dir_path10 = str_replace(get_site_url().'/',$directory_path,$imgvalue1);
							$multisite_url_dir10 = str_replace($multisite_url, $mainsite_url, $url_dir_path10);
							$fetch_only_images10 = mime_content_type($multisite_url_dir10);
							if(strpos($fetch_only_images10, 'image') !== false){
								if (strpos($imgvalue1, '",') !== false) {
									$imgvalue1 = trim($imgvalue1,'",');
								}elseif(strpos($imgvalue1, '"') !== false){
									$imgvalue1 = trim($imgvalue1,'"');
								}
								$uniqueArrs[$increment]['title'] = $the_title;
								$uniqueArrs[$increment]['src'] = $imgvalue1;
								$uniqueArrs[$increment]['medianame'] = basename($imgvalue1);
								$uniqueArrs[$increment]['page_builder_name'] = "Oxygen Builder";
								if(@filemtime($imgvalue1)){
									$uniqueArrs[$increment]['datetime'] = filemtime($imgvalue1);
								}else{
									$uniqueArrs[$increment]['datetime'] = $getvalues->post_date;
								}
								$uniqueArrs[$increment]['post_type'] = $post_type;
								$uniqueArrs[$increment]['post_category'] = '';
								$uniqueArrs[$increment]['variant_attribute'] = '';
								$uniqueArrs[$increment]['variant_sku'] = '';
								$uniqueArrs[$increment]['source_from'] = "database";
								$uniqueArrs[$increment]['linked'] = "Yes";
								$uniqueArrs[$increment]['site_name'] = $prefixValue;
								$increment++;
							}
						}
					}
					if (strpos($oxygen_builder, 'image_ids') !== false) {
						$get_result_gallery_array = array();
						preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $oxygen_builder, $get_result_gallery_array);
						$gallery_array = $get_result_gallery_array[1];
						$gallery_images = explode(",",$gallery_array[0]);								
						foreach ($gallery_images as $img_id) {
							$get_attach_url = $wpdb->get_results("select guid from ".$prefixValue."posts where post_type='attachment' and ID='$img_id'");
							$get_attach_urls[] = $get_attach_url[0];					
						}
						$increment2 = 0;
						foreach ($get_attach_urls as $srcvalue) {
							$srcvalueMultiUrl = stripslashes($srcvalue->guid);
							$url_dir_path11 = str_replace(get_site_url().'/',$directory_path,$srcvalueMultiUrl);
							$multisite_url_dir11 = str_replace($multisite_url, $mainsite_url, $url_dir_path11);
							$fetch_only_images11 = mime_content_type($multisite_url_dir11);
							if(strpos($fetch_only_images11, 'image') !== false){
								$uniqueArrs2[$increment2]['title'] = $getvalues->post_title;
								$uniqueArrs2[$increment2]['src'] = $srcvalueMultiUrl;
								$uniqueArrs2[$increment2]['medianame'] = basename($srcvalueMultiUrl);
								$uniqueArrs2[$increment2]['page_builder_name'] = "Oxygen Builder";
								if(@filemtime($srcvalueMultiUrl)){
									$uniqueArrs2[$increment2]['datetime'] = filemtime($srcvalueMultiUrl);
								}else{
									$uniqueArrs2[$increment2]['datetime'] = $getvalues->post_date;
								}
								$uniqueArrs2[$increment2]['post_type'] = $getvalues->post_type;
								$uniqueArrs2[$increment2]['post_category'] = '';
								$uniqueArrs2[$increment2]['variant_attribute'] = '';
								$uniqueArrs2[$increment2]['variant_sku'] = '';
								$uniqueArrs2[$increment2]['source_from'] = "database";
								$uniqueArrs2[$increment2]['linked'] = "Yes";
								$uniqueArrs2[$increment2]['site_name'] = $prefixValue;
								$increment2++;
							}
						}
					}
					if (strpos($oxygen_builder, '"src":') !== false) {
						$get_result_src_array = array();
						preg_match_all('/"src"\h*:.*?\"(.*?)\"(?![^"\n]")/', $oxygen_builder, $get_result_src_array);
						$get_result_src_ars = $get_result_src_array[1];
						foreach ($get_result_src_ars as $imgkey => $valueImg) {
							if (strpos($valueImg, '",') !== false) {
								$valueImg = trim($valueImg,'",');
							}elseif(strpos($valueImg, '"') !== false){
								$valueImg = trim($valueImg,'"');
							}
							$valueImg = stripslashes($valueImg);
							$url_dir_path12 = str_replace(get_site_url().'/',$directory_path,$valueImg);
							$multisite_url_dir12 = str_replace($multisite_url, $mainsite_url, $url_dir_path12);
							$fetch_only_images12 = mime_content_type($multisite_url_dir12);
							if(strpos($fetch_only_images12, 'image') !== false){
								$uniqueArrs3[$imgkey]['title'] = $getvalues->post_title;
								$uniqueArrs3[$imgkey]['src'] = $valueImg;
								$uniqueArrs3[$imgkey]['medianame'] = basename($valueImg);
								$uniqueArrs3[$imgkey]['page_builder_name'] = "Oxygen Builder";
								if(@filemtime($valueImg)){
									$uniqueArrs3[$imgkey]['datetime'] = filemtime($valueImg);
								}else{
									$uniqueArrs3[$imgkey]['datetime'] = $getvalues->post_date;
								}
								$uniqueArrs3[$imgkey]['post_type'] = $getvalues->post_type;
								$uniqueArrs3[$imgkey]['post_category'] = '';
								$uniqueArrs3[$imgkey]['variant_attribute'] = '';
								$uniqueArrs3[$imgkey]['variant_sku'] = '';
								$uniqueArrs3[$imgkey]['source_from'] = "database";
								$uniqueArrs3[$imgkey]['linked'] = "Yes";
								$uniqueArrs3[$imgkey]['site_name'] = $prefixValue;
							}
						}
					}
					if($uniqueArrs && $uniqueArrs2){
						$oxygen_merge1 = array_merge($uniqueArrs,$uniqueArrs2);
					}else{
						if(!empty($uniqueArrs)){
							$oxygen_merge1 = $uniqueArrs;
						}elseif(!empty($uniqueArrs2)){
							$oxygen_merge1 = $uniqueArrs2;
						}else{
							$oxygen_merge1 = array();
						}
					}
					
					if($oxygen_merge1 && $uniqueArrs3){
						$uniqueArr = array_merge($oxygen_merge1,$uniqueArrs3);
					}else{
						if(!empty($oxygen_merge1)){
							$uniqueArr = $oxygen_merge1;
						}elseif(!empty($uniqueArrs3)){
							$uniqueArr = $uniqueArrs3;
						}else{
							$uniqueArr = array();
						}
					}
				}
				if($brizy_builder){
					$PageId = $getvalues->ID;
					$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='brizy' AND post_id=$PageId" );
					$the_content = $the_content[0]->meta_value;
					$the_content = unserialize($the_content);
					$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
					$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
					preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
					preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);

					// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
					preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
					foreach ($case1_gethreffiles[1] as $case1_href_value){
						$case1_href_value = strtok($case1_href_value, " ");
						$case1_href_value = stripslashes($case1_href_value);
			            $case1_dir_path = str_replace(get_site_url().'/',$directory_path,$case1_href_value);
						if(file_exists($case1_dir_path)){
							$url_dir_path13 = str_replace(get_site_url().'/',$directory_path,$case1_href_value);
							$multisite_url_dir13 = str_replace($multisite_url, $mainsite_url, $url_dir_path13);
							$fetch_only_images13 = mime_content_type($multisite_url_dir13);
							if(strpos($fetch_only_images13, 'image') !== false){
								$hrefcase1[] = $case1_href_value;
							}
						}
					}
					// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
					preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
					foreach ($case2_gethreffiles[1] as $case2_href_value) {
						$case2_href_value = strtok($case2_href_value, " ");
						$case2_href_value = stripslashes($case2_href_value);
			            $case2_dir_path = str_replace(get_site_url().'/',$directory_path,$case2_href_value);
						if(file_exists($case2_dir_path)){
							$url_dir_path14 = str_replace(get_site_url().'/',$directory_path,$case2_href_value);
							$multisite_url_dir14 = str_replace($multisite_url, $mainsite_url, $url_dir_path14);
							$fetch_only_images14 = mime_content_type($multisite_url_dir14);
							if(strpos($fetch_only_images14, 'image') !== false){
								$hrefcase2[] = $case2_href_value;
							}
						}
					}				
					// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
					$srcfiles = array();
					preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
					foreach ($case3_gethreffiles[1] as $case3_href_value){
						$case3_href_value = strtok($case3_href_value, " ");
						$case3_href_value = stripslashes($case3_href_value);
			            $case3_dir_path = str_replace(get_site_url().'/',$directory_path,$case3_href_value);
						if(file_exists($case3_dir_path)){
							$url_dir_path15 = str_replace(get_site_url().'/',$directory_path,$case3_href_value);
							$multisite_url_dir15 = str_replace($multisite_url, $mainsite_url, $url_dir_path15);
							$fetch_only_images15 = mime_content_type($multisite_url_dir15);
							if(strpos($fetch_only_images15, 'image') !== false){
								$hrefcase3[] = $case3_href_value;
							}
						}
					}				
					// CASE 4 (data-href) : Get url when pattern is <a data-href=
					preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
					foreach ($case4_gethreffiles[1] as $case4_href_value){					
						$case4_href_value = strtok($case4_href_value, " ");
						$case4_data_href = utf8_decode(urldecode($case4_href_value));
						preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
						$hrefcase4 = array();
						foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
							$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
							$case4_dir_path = str_replace(get_site_url().'/',$directory_path,$theLinkVal);
							if(file_exists($case4_dir_path)){
								$url_dir_path16 = str_replace(get_site_url().'/',$directory_path,$theLinkVal);
								$multisite_url_dir16 = str_replace($multisite_url, $mainsite_url, $url_dir_path16);
								$fetch_only_images16 = mime_content_type($multisite_url_dir16);
								if(strpos($fetch_only_images16, 'image') !== false){
									$hrefcase4[] = $theLinkVal;
								}
							}
						}					
					}				
					// $the_content = htmlspecialchars($the_content);

					// CASE 6 (image) : Get url when getting images
					$imagesNeme = array_merge($group12[1],$group14[1]);				
					foreach ($imagesNeme as $imageNeme) {						
						// Get Media id by Name only for brizy
						$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
						if(!empty($imgIdsquery)){
							$imagesIds = getMultiSiteImageUrlbyIdCore($imgIdsquery[0]->post_id,$prefixValue);
							$imagesIds = stripslashes($imagesIds);
							$url_dir_path17 = str_replace(get_site_url().'/',$directory_path,$imagesIds);
							$multisite_url_dir17 = str_replace($multisite_url, $mainsite_url, $url_dir_path17);
							$fetch_only_images17 = mime_content_type($multisite_url_dir17);
							if(strpos($fetch_only_images17, 'image') !== false){
								$hrefcase6[] = $imagesIds;
							}
						}
					}
					$href_array = array_unique(array_merge($hrefcase1, $hrefcase2, $hrefcase3, $hrefcase4, $hrefcase6));				
					if(!empty($href_array)){
						foreach ($href_array as $brizy_value) {
							$hrefValues = stripslashes($brizy_value);
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $hrefValues;
							$new_array['medianame'] = basename($hrefValues);
							$new_array['page_builder_name'] = "Brizy";
							$new_array['datetime'] = filemtime($hrefValues);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				if($siteorigin_ck_builder == "SiteOrigin"){
					$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
					$PageId = $getvalues->ID;
					$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id=$PageId" );
					$the_content = $the_content[0]->meta_value;
					$the_content = unserialize($the_content);
					$siteorigenArray = array();
					
					foreach ($the_content['widgets'] as $valueid) {
						// $siteorigenArray = array();
						// $getContentUrl = array();
						if(array_key_exists("attachment_id", $valueid)){
							$sitegetimgurlattachment_id = getMultiSiteImageUrlbyIdCore($valueid['attachment_id'],$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlattachment_id);
						}
						if(array_key_exists("ids", $valueid)){
							foreach ($valueid['ids'] as $galleryimg) {
								$sitegetimgurlids = getMultiSiteImageUrlbyIdCore($galleryimg,$prefixValue);
								array_push($siteorigenArray, $sitegetimgurlids);
							}
						}
						if(array_key_exists("features", $valueid)){
							foreach ($valueid['features'] as $features_img) {
								$sitegetimgurlfeatures = getMultiSiteImageUrlbyIdCore($features_img['icon_image'],$prefixValue);
								array_push($siteorigenArray, $sitegetimgurlfeatures);
							}
						}
						if(array_key_exists("image", $valueid)){
							$sitegetimgurlimage = getMultiSiteImageUrlbyIdCore($valueid['image'],$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlimage);
						}
						if(array_key_exists("frames", $valueid)){
							foreach ($valueid['frames'] as $frames_img) {
								$sitegetimgurlframes = getMultiSiteImageUrlbyIdCore($frames_img['background_image'],$prefixValue);
								array_push($siteorigenArray, $sitegetimgurlframes);
							}
						}
						if(array_key_exists("content", $valueid)){
							$custom_html = $valueid['content'];
							preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
							foreach ($contentimg[1] as $imgurl){
								array_push($siteorigenArray, $imgurl);
							}
						}
						if(array_key_exists("content", $valueid)){
							preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
							foreach ($contentimgs[1] as $imgurls){
								array_push($siteorigenArray, $imgurls);
							}
						}			
						if(array_key_exists("text", $valueid)){						
							$siteEditor = $valueid['text'];
							preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
							preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
							$groupsfilehref = $groupsfile[1];
							$groupsfilesrc = $srcgroupsfile[1];
							if(!empty($groupsfilehref)){
								foreach ($groupsfilehref as $filesvalue) {								
									$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvalue;
								    }
								}
							}
							if(!empty($groupsfilesrc)){
								foreach ($groupsfilesrc as $filesvaluesrc) {
									$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvaluesrc;
								    }
								}
							}
							preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
							preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
							preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
							preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
							preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
							preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
							preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
							preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
							preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
							preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
							preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
							preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
							preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
							preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
							preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
							preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
							preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
							preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
							preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
							preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
							preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
							// for vc video
							if(!empty($vc_video_url[1])){
								foreach ($vc_video_url[1] as $vc_video) {
									$getContentUrl[] = $vc_video;
								}
							}
							// for src
							if(!empty($group9[1])){
								foreach ($group9[1] as $group9s) {
									$getContentUrl[] = $group9s;
								}
							}
							// for mp3
							if(!empty($group11[1])){
								foreach ($group11[1] as $group11s) {
									$getContentUrl[] = $group11s;
								}
							}
							// for mp4
							if(!empty($group12[1])){
								foreach ($group12[1] as $group12s) {
									$getContentUrl[] = $group12s;
								}
							}
							// for pdf
							if(!empty($group13[1])){
								foreach ($group13[1] as $group13s) {
									$getContentUrl[] = $group13s;
								}
							}
							// for docx
							if(!empty($group14[1])){
								foreach ($group14[1] as $group14s) {
									$getContentUrl[] = $group14s;
								}
							}
							// for doc
							if(!empty($group15[1])){
								foreach ($group15[1] as $group15s) {
									$getContentUrl[] = $group15s;
								}
							}
							// for ppt
							if(!empty($group16[1])){
								foreach ($group16[1] as $group16s) {
									$getContentUrl[] = $group16s;
								}
							}
							// for xls
							if(!empty($group17[1])){
								foreach ($group17[1] as $group17s) {
									$getContentUrl[] = $group17s;
								}
							}
							// for pps
							if(!empty($group18[1])){
								foreach ($group18[1] as $group18s) {
									$getContentUrl[] = $group18s;
								}
							}
							// for ppsx
							if(!empty($group19[1])){
								foreach ($group19[1] as $group19s) {
									$getContentUrl[] = $group19s;
								}
							}
							// for xlsx
							if(!empty($group20[1])){
								foreach ($group20[1] as $group20s) {
									$getContentUrl[] = $group20s;
								}
							}
							// for odt
							if(!empty($group21[1])){
								foreach ($group21[1] as $group21s) {
									$getContentUrl[] = $group21s;
								}
							}
							// for ogg
							if(!empty($group22[1])){
								foreach ($group22[1] as $group22s) {
									$getContentUrl[] = $group22s;
								}
							}
							// for m4a
							if(!empty($group23[1])){
								foreach ($group23[1] as $group23s) {
									$getContentUrl[] = $group23s;
								}
							}
							// for wav
							if(!empty($group24[1])){
								foreach ($group24[1] as $group24s) {
									$getContentUrl[] = $group24s;
								}
							}
							// for mp4
							if(!empty($group25[1])){
								foreach ($group25[1] as $group25s) {
									$getContentUrl[] = $group25s;
								}
							}
							// for mov
							if(!empty($group26[1])){
								foreach ($group26[1] as $group26s) {
									$getContentUrl[] = $group26s;
								}
							}
							// for avi
							if(!empty($group27[1])){
								foreach ($group27[1] as $group27s) {
									$getContentUrl[] = $group27s;
								}
							}
							// for 3gp
							if(!empty($group28[1])){
								foreach ($group28[1] as $group28s) {
									$getContentUrl[] = $group28s;
								}
							}
							// for pptx
							if(!empty($group29[1])){
								foreach ($group29[1] as $group29s) {
									$getContentUrl[] = $group29s;
								}
							}
						}
						preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
						if(!empty($group10)){
							$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
							if(!empty($bgimgUrl1)){
								foreach ($bgimgUrl1 as $filesvaluebackground) {								
									$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvaluebackground;
								    }
								}
							}
						}
						$VCmediaContent = array_unique($getContentUrl);
						$siteorigenArray = array_unique($siteorigenArray);
						$siteOriginUrl = array();
						if(!empty($VCmediaContent) && !empty($siteorigenArray)){
							$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
						}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
							$siteOriginUrl = $VCmediaContent;
						}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){
							$siteOriginUrl = $siteorigenArray;
						}
						$final_array = array();
						foreach ($siteOriginUrl as $thevalues) {
							array_push($final_array, $thevalues);
						}
					}
					foreach ($final_array as $siteOriginUrlValue) {
						$SiteOriginimageUrl = stripslashes($siteOriginUrlValue);
						$url_dir_path18 = str_replace(get_site_url().'/',$directory_path,$SiteOriginimageUrl);
						$multisite_url_dir18 = str_replace($multisite_url, $mainsite_url, $url_dir_path18);
						$fetch_only_images18 = mime_content_type($multisite_url_dir18);
						if(strpos($fetch_only_images18, 'image') !== false){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $SiteOriginimageUrl;
							$new_array['medianame'] = basename($SiteOriginimageUrl);
							$new_array['page_builder_name'] = "Site Origin";

							$siteorigin_url_dir_path =  str_replace(get_site_url().'/',$directory_path,$SiteOriginimageUrl);
							if(file_exists($siteorigin_url_dir_path)){
								$unixtime = filemtime($siteorigin_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							}else{
								$new_array['datetime'] = "";
							}					
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = '';
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['site_name'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder && $post_type !='attachment'){
					$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;			
					$PageId = $getvalues->ID;

					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
					$groupsfile = $groupsfile[1];
					if(!empty($groupsfile)){
						foreach ($groupsfile as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for wmv
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}

					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

					if(!empty($gutenbergImgUrls) && !empty($getFiles)){
						$hrefMedias = array_unique(array_merge($getFiles,$gutenbergImgUrls));
					}else{
						if(!empty($getFiles)){
							$hrefMedias = array_unique($getFiles);
						}elseif(!empty($gutenbergImgUrls)){
							$hrefMedias = array_unique($gutenbergImgUrls);
						}else{
							$hrefMedias = array();
						}
					}
					if(!empty($gutenbergImgUrls) && !empty($hrefMedias)){
						$totalContentMedia = array_unique(array_merge($gutenbergImgUrls,$hrefMedias));
					}else{
						if(empty($gutenbergImgUrls) && !empty($hrefMedias)){
							$totalContentMedia = $hrefMedias;
						}elseif(!empty($gutenbergImgUrls) && empty($hrefMedias)){
							$totalContentMedia = $gutenbergImgUrls;
						}
					}
					$gutenbergImgids = array();
					foreach ($totalContentMedia as $gutenbergImgUrl) {
						array_push($gutenbergImgids, $gutenbergImgUrl);
					}
					if(!empty($gutenbergImgids)){
						foreach ($gutenbergImgids as $imagesids) {
							$simple_images = stripslashes($imagesids);
							$image_base_name = basename($simple_images);

							$attachmentmetadata = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key='_wp_attachment_metadata'");
							$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
							$the_post_id = $attachmentmetadata[0]->post_id;
							foreach ($unserializemetadata['sizes'] as $the_data) {
								if($image_base_name == $the_data['file']){
									$get_main_image = $wpdb->get_results("SELECT * FROM ".$prefixValue."posts WHERE ID = '$the_post_id'");
									$the_image_url = $get_main_image[0]->guid;
									$the_image_url = stripslashes($the_image_url);
									$url_dir_path = str_replace(get_site_url().'/',$directory_path,$the_image_url);
									$multisite_url_dirs = str_replace($multisite_url, $mainsite_url, $url_dir_path);
									$fetch_only_images = mime_content_type($multisite_url_dirs);
									if(strpos($fetch_only_images, 'image') !== false){
										$new_array_simple['media_id'] = '';
										$new_array_simple['title'] = $the_title;
										$new_array_simple['src'] = $the_image_url;
										$new_array_simple['medianame'] = basename($simple_images);
										$new_array_simple['page_builder_name'] = "Simple/Gutenberg Content Media";

										$siteorigin_url_dir_path =  str_replace(get_site_url().'/',$directory_path,$simple_images);
										if(file_exists($siteorigin_url_dir_path)){
											$unixtime = filemtime($siteorigin_url_dir_path);
											$new_array_simple['datetime'] = date("Y-m-d h:i:s",$unixtime);
										}else{
											$new_array_simple['datetime'] = "";
										}					
										$new_array_simple['post_type'] = $post_type;
										$new_array_simple['post_category'] = '';
										$new_array_simple['variant_attribute'] = '';
										$new_array_simple['variant_sku'] = '';
										$new_array_simple['source_from'] = 'database';
										$new_array_simple['linked'] = 'Yes';
										$new_array_simple['site_name'] = $prefixValue;
										array_push($uniqueArr,$new_array_simple);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	$uniqueArr = array_unique($uniqueArr, SORT_REGULAR);
	return $uniqueArr;
}
// GET ATTACHMENT DETAILS FROM DATABASE
function getattachmentdataCore(){
	global $wpdb;
	$current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);
	$mediaexistdata=  array();
	$pagebuildername = '';
	$mimetype = "image/jpeg";
	if(is_multisite()){
		$prefixes = getAllSitePrefixCore();
		foreach ($prefixes as $mutliarray) {
			$prefixValue[] = $mutliarray['prefix'];
		}
	}
	$attachmentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where post_type='attachment'");

	if(!empty($attachmentdata)){
		$attachmentarray = array();
		foreach ($attachmentdata as $key => $value) {
			$attach_url = stripslashes($value->guid);
			$url_dir_path = str_replace(get_site_url().'/',$directory_path,$attach_url);
			$fetch_only_images = mime_content_type($url_dir_path);
			if(strpos($fetch_only_images, 'image') !== false){
				$media_id = $value->ID;
				$media_url = $attach_url;
				$mediaexistdata[] = array(
					'media_id' => $media_id,
					'medianame' => basename($media_url),
					'src' => $media_url,
					'media_type' => "",
					'title'=> $value->post_title,
					'post_type' => $value->post_type,
					'page_builder_name' => "",
					'post_category' => "",
					'variant_attribute' => '',
					'variant_sku' => '',
					'datetime' => "",
					'linked' =>'No',
					'source_from' => 'From Media Library',
					'site_name' => $wpdb->prefix
				);
			}
		}
	}
	return $mediaexistdata;
}
//GET MULTISITE ATTACHMENT DETAILS FROM DATABASE
function getattAchmentDataMultiSiteCore(){
	global $wpdb;
	$current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);
	$mediaexistdata=  array();
	$pagebuildername = '';
	$mimetype = "image/jpeg";
	$prefixes = getAllSitePrefixCore();
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		foreach (get_sites() as $all_sites) {
			if($all_sites->blog_id == $multisiteId){
				$multisite_url = $all_sites->path;
			}
			if($all_sites->blog_id == 1){
				$mainsite_url = $all_sites->path;
			}
        }
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;
		$attachmentdata = $wpdb->get_results("SELECT * from ".$prefixValue."posts where post_type='attachment'");		
		
		if(!empty($attachmentdata)){
			$attachmentarray = array();
			foreach ($attachmentdata as $key => $value) {
				$attach_url = stripslashes($value->guid);
				$url_dir_path = str_replace(get_site_url().'/',$directory_path,$attach_url);
				$multisite_url_dir1 = str_replace($multisite_url, $mainsite_url, $url_dir_path);
				$fetch_only_images = mime_content_type($multisite_url_dir1);
				if(strpos($fetch_only_images, 'image') !== false){
					$media_id = $value->ID;
					$media_url = $attach_url;
					$mediaexistdata[] = array(
						'media_id' => $media_id,
						'medianame' => basename($media_url),
						'src' => $media_url,
						'media_type' => "",
						'title'=> $value->post_title,
						'post_type' => $value->post_type,
						'page_builder_name' => "",
						'post_category' => "",
						'variant_attribute' => '',
						'variant_sku' => '',
						'datetime' => "",
						'linked' =>'No',
						'source_from' => 'From Media Library',
						'site_name' => $prefixValue
					);
				}
			}
		}
	}
	return $mediaexistdata;
}
function AutoDeleteImages(){
    global $wpdb;
    $current_dir = dirname(__FILE__).'/';
	$directory_path = str_replace("/wp-content/plugins/wp_media_cleaner", "", $current_dir);

	$PageBuilderMedia = getPageBuilderContentMediaCore();
	// $wpdb->query("UPDATE wp_optimizer_backup set Image_url = '$PageBuilderMedia' WHERE backup_id = '3'");
	if(is_multisite()){
		$multisitePageBuilderMedia = getMultiSitePageBuilderContentMediaCore();
	}else{
		$multisitePageBuilderMedia = array();
	}
	// Get multsite media from page builder ends Coded by Hemant

	// Merge page builder content for single and multisite
	if(!empty($PageBuilderMedia) && !empty($multisitePageBuilderMedia)){
		$getImgs = array_merge($PageBuilderMedia,$multisitePageBuilderMedia);
	}elseif(empty($PageBuilderMedia) && !empty($multisitePageBuilderMedia)){
		$getImgs = $multisitePageBuilderMedia;
	}elseif(!empty($PageBuilderMedia) && empty($multisitePageBuilderMedia)){
		$getImgs = $PageBuilderMedia;
	}
	if ( class_exists( 'WooCommerce' ) ) {
	  $newdata = getattachmentproductsCore();
	} else {
	  $newdata = array();
	}

	if(is_multisite()){
		$newdata2 = getMultisiteAttachmentProductsCore();
	}else{
		$newdata2 = array();
	}
	if($newdata && $newdata2){
		$lresult = array_merge($newdata,$newdata2);
	}else{
		if($newdata2){
			$lresult = $newdata2;
		}elseif($newdata){
			$lresult = $newdata;
		}else{
			$lresult = array();
		}
	}
	if($lresult && $getImgs){
		$result2 = array_merge($lresult,$getImgs);
	}else{
		if($lresult){
			$result2 = $lresult;
		}elseif($getImgs){
			$result2 = $getImgs;
		}else{
			$result2 = array();
		}
	}

	$atdata1 = getattachmentdataCore();
	if(is_multisite()){
		$atdata2 = getattAchmentDataMultiSiteCore();
	}else{
		$atdata2 = array();
	}

	if($atdata1 && $atdata2){
		$pre_result = array_merge($atdata1,$atdata2);
	}else{
		if($atdata1){
			$pre_result = $atdata1;
		}elseif($atdata2){
			$pre_result = $atdata2;
		}else{
			$pre_result = array();
		}
	}

	if($result2 && $pre_result){
		$final_resultt = array_merge($result2,$pre_result);
	}else{
		if($result2){
			$final_resultt = $result2;
		}elseif($pre_result){
			$final_resultt = $pre_result;
		}else{
			$final_resultt = array();
		}
	}
	$final_resultt = array_unique($final_resultt, SORT_REGULAR);

	// Remove duplicacy for linked and not linked images
	$new_arrays = array();
	foreach ($final_resultt as $Newvalue) {
	    if(array_search($Newvalue['src'], array_column($new_arrays, 'src')) !== false) {
	    	if($Newvalue['linked'] != 'No'){
	    		$new_arrays[] = $Newvalue;
	    	}
	    }else{
	    	$new_arrays[] = $Newvalue;
	    }
	}
	// Deleting Unlinked Image
	if(is_multisite()){
		// get main site path
		$current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
		$mainsite_url = $current_blog_details->path;
	}
	// Get all the URL that is excluded
	$if_excluded = $wpdb->get_results("SELECT Image_url FROM ".$wpdb->prefix."optimizer_backup");
	foreach ($if_excluded as $key => $value) {
		$excluded_images[] = $value->Image_url;
	}
	foreach ($new_arrays as $action_to_value) {	
		$final_src = $action_to_value['src'];
		$final_linked = $action_to_value['linked'];
		$final_site_prefix = $action_to_value['site_name'];
		if(!@$final_site_prefix){
			$final_site_prefix = $wpdb->prefix;
		}
		if($final_linked == 'No'){
			if(in_array($final_src, $excluded_images)){
			}else{
				if($final_site_prefix == $wpdb->prefix){
					$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $final_src )); 
					$attachmentid = @$attachment[0];
					if($attachmentid){
						$attachment_path = get_attached_file($attachmentid); 
						//Delete attachment from database and upload folder
						$delete_attachment = wp_delete_attachment($attachmentid, true);
					}
				}else{
					preg_match('!\d+!', $final_site_prefix, $matches);
					$site_id = $matches[0];			
					// get multi site path
					$current_blog_details2 = get_blog_details( array( 'blog_id' => $site_id ) );
					$multisite_url = $current_blog_details2->path;
					// get post id of an image
					$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM ".$final_site_prefix."posts WHERE guid='%s';", $final_src ));
					$attachmentid = @$attachment[0];
					// delete image by its post id
					$is_post_delete = $wpdb->query("DELETE FROM ".$final_site_prefix."posts WHERE ID = '$attachmentid'");
					if($is_post_delete === 1){
						// Delete resized images of main image
						$dir_url = str_replace(get_site_url().'/',$directory_path,$final_src);				
						$multisite_url = str_replace($multisite_url, $mainsite_url, $dir_url);
						unlink($multisite_url); // delete main image from directory (upload folder)
						$attachmentmetadata = $wpdb->get_results("SELECT meta_value FROM ".$final_site_prefix."postmeta where post_id='$attachmentid' AND meta_key='_wp_attachment_metadata'");
						$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
						if($unserializemetadata['sizes']){
							foreach ($unserializemetadata['sizes'] as $metavalue) {
								// get resized images urls
								$final_src_basename = basename($final_src);
								$get_the_url = str_replace($final_src_basename, $metavalue['file'], $multisite_url);
								if(file_exists($get_the_url)){
									unlink($get_the_url);
								}
							}
						}
					}
				}
			}
		}
	}
    // $wpdb->query("DELETE FROM wp_optimizer_backup WHERE backup_id = '2'");    
}
add_action('AutoDeleteImage', 'AutoDeleteImages');
// CODE FOR GETTING IMAGE THAT CAN BE DELETE IN AUTOMATIC IMAGE DELETER (UNLINKED IMAGES) ENDS - By Hemant

// Sohel CRON JOB Action
function RemoveExpireTransients(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$OptionsTable = $prefixTable.'options';
	$options = $wpdb->get_col("SELECT option_name FROM $OptionsTable WHERE option_name LIKE '_transient_timeout_%' AND option_value < NOW()");
    foreach ($options as $option) {
		if ( strpos( $option, '_site_transient_' ) !== false ) {
			delete_site_transient( str_replace( '_site_transient_timeout_', '', $option ) );
		} else {
			delete_transient( str_replace( '_transient_timeout_', '', $option ) );
		}
    }
}
add_action('ExprTransientSchedule', 'RemoveExpireTransients');

function RemoveAll(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$OptionsTable = $prefixTable.'options';
	// Reindex Table
	$table = '';
	$like = $wpdb->prefix . $table . '%';
    $tables = $wpdb->get_results($wpdb->prepare("SELECT TABLE_NAME, ENGINE FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE %s", $like));
		foreach ($tables as $table){
			$table_name = esc_sql($table->TABLE_NAME);
			switch ($table->ENGINE){
			    case 'MyISAM':
			    $wpdb->query("REPAIR TABLE {$table_name}");
			    break;
			    case 'InnoDB':
			    $wpdb->query("ALTER TABLE {$table_name} ENGINE = InnoDB");
			    break;
			}
    }
    // Optimize Table
    $tables1 = $wpdb->get_col("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE '$prefixTable%'");
	$tables1 = implode( ',', $tables1 );
	$wpdb->query("OPTIMIZE TABLE {$tables1}");
	// Remove Expire Transients
	$options = $wpdb->get_col("SELECT option_name FROM $OptionsTable WHERE option_name LIKE '_transient_timeout_%' AND option_value < NOW()");
    foreach ($options as $option) {
		if ( strpos( $option, '_site_transient_' ) !== false ) {
			delete_site_transient( str_replace( '_site_transient_timeout_', '', $option ) );
		} else {
			delete_transient( str_replace( '_transient_timeout_', '', $option ) );
		}
    }
}
add_action('ScheduleAll', 'RemoveAll');

function RemoveRevisions(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$PostTable = $prefixTable.'posts';
	$posts = $wpdb->get_col("SELECT ID FROM $PostTable WHERE `post_type` LIKE 'revision'");
    foreach ($posts as $id) {
          wp_delete_post_revision($id);
    }
}
add_action('RevisionsSchedule', 'RemoveRevisions');

function RemoveTranshedPosts(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$PostTable = $prefixTable.'posts';
	$posts = $wpdb->get_col("SELECT ID FROM $PostTable WHERE `post_status` LIKE 'trash'");
    foreach ($posts as $id) {
          wp_delete_post($id, true);
    }
}
add_action('TrashedPostsSchedule', 'RemoveTranshedPosts');

function RemoveOrphanPostmeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$PostTable = $prefixTable.'posts';
	$PostmetaTable = $prefixTable.'postmeta';
	$postmeta = $wpdb->get_results("SELECT meta_id, post_id, meta_key FROM $PostmetaTable WHERE post_id NOT IN (SELECT ID FROM $PostTable)");
    foreach ($postmeta as $meta) {
		if (empty($meta->post_id)) {
			$wpdb->query("DELETE FROM $PostmetaTable WHERE meta_id = {$meta->meta_id}");
		} else {
			delete_post_meta( $meta->post_id, $meta->meta_key );
		}
    }
}
add_action('OrphanPostmetaSchedule', 'RemoveOrphanPostmeta');

function RemoveOrphanAttachment(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$PostTable = $prefixTable.'posts';
	$attachments = $wpdb->get_col("SELECT ID FROM $PostTable WHERE post_parent NOT IN (SELECT ID FROM $PostTable) AND post_parent != 0 AND post_type = 'attachment'");
    foreach ($attachments as $attachment) {
		wp_delete_attachment($attachment, true);
    }
}
add_action('OrphanAttachmentsSchedule', 'RemoveOrphanAttachment');

function RemoveDuplicatedPostmeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$PostmetaTable = $prefixTable.'postmeta';
	$_postmeta = array();
    $postmeta = $wpdb->get_results("SELECT pm.meta_id AS meta_id, pm.post_id AS post_id FROM $PostmetaTable pm INNER JOIN (SELECT post_id, meta_key, meta_value, COUNT(*) FROM $PostmetaTable GROUP BY post_id, meta_key, meta_value HAVING COUNT(*) > 1) pm2 ON pm.post_id = pm2.post_id AND pm.meta_key = pm2.meta_key AND pm.meta_value = pm2.meta_value WHERE pm.meta_key NOT IN ('_price', '_used_by')");
    foreach($postmeta as $meta){
		$_postmeta[$meta->post_id][] = $meta->meta_id;
    }
    foreach ($_postmeta as $post_id => $meta_ids) {
		array_pop($meta_ids);
		$wpdb->query("DELETE FROM $PostmetaTable WHERE meta_id IN (" . implode( ',', $meta_ids ) . ")");
    }
}
add_action('DuplicatedPostmetaSchedule', 'RemoveDuplicatedPostmeta');

function RemoveSpamComments(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$CommentsTable = $prefixTable.'comments';
	$comments = $wpdb->get_col("SELECT comment_ID FROM $CommentsTable WHERE comment_approved = 'spam'");
	foreach ( $comments as $comment ) {
		wp_delete_comment($comment, true );
	}
}
add_action('SpamCommentsSchedule', 'RemoveSpamComments');

function RemoveTransedComments(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$CommentsTable = $prefixTable.'comments';
	$comments = $wpdb->get_col("SELECT comment_ID FROM $CommentsTable WHERE comment_approved = 'trash'");
    foreach ( $comments as $comment ) {
		wp_delete_comment($comment, true );
	}
}
add_action('TrashedCommentsSchedule', 'RemoveTransedComments');

function RemoveOrphanCommentmeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$CommentsTable = $prefixTable.'comments';
	$CommentmetaTable = $prefixTable.'commentmeta';
	$commentmeta = $wpdb->get_results("SELECT meta_id, comment_id, meta_key FROM $CommentmetaTable WHERE comment_id NOT IN (SELECT comment_ID FROM $CommentsTable)");
    foreach ($commentmeta as $meta) {
		if (empty($meta->comment_id)) {
			$wpdb->query("DELETE FROM $CommentmetaTable WHERE meta_id = {$meta->meta_id}");
		} else {
			delete_comment_meta( $meta->comment_id, $meta->meta_key );
		}
    }
}
add_action('OrphanCommentmetaSchedule', 'RemoveOrphanCommentmeta');

function RemoveDuplicaedCommentmeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$CommentmetaTable = $prefixTable.'commentmeta';
	$_commentmeta = array();
    $commentmeta = $wpdb->get_results("SELECT cm.meta_id AS meta_id, cm.comment_id AS comment_id FROM $CommentmetaTable cm INNER JOIN (SELECT comment_id, meta_key, meta_value, COUNT(*) FROM $CommentmetaTable GROUP BY comment_id, meta_key, meta_value HAVING COUNT(*) > 1) cm2 ON cm.comment_id = cm2.comment_id AND cm.meta_key = cm2.meta_key AND cm.meta_value = cm2.meta_value");
    foreach($commentmeta as $meta){
		$_commentmeta[$meta->comment_id][] = $meta->meta_id;
    }
    foreach ($_commentmeta as $comment_id => $meta_ids) {
		array_pop($meta_ids);
		$wpdb->query("DELETE FROM $CommentmetaTable WHERE meta_id IN (" . implode( ',', $meta_ids ) . ")");
    }
}
add_action('DuplicatedCommentmetaSchedule', 'RemoveDuplicaedCommentmeta');

function RemoveOrphanTermmeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$TermsTable = $prefixTable.'terms';
	$TermmetaTable = $prefixTable.'termmeta';
	$termmeta = $wpdb->get_results("SELECT meta_id, term_id, meta_key FROM $TermmetaTable WHERE term_id NOT IN (SELECT term_id FROM $TermsTable)");
    foreach ($termmeta as $meta) {
		if (empty($meta->term_id)) {
            $wpdb->query("DELETE FROM $TermmetaTable WHERE meta_id = {$meta->meta_id}");
		} else {
            delete_term_meta( $meta->term_id, $meta->meta_key );
		}
    }
}
add_action('OrphanTermmetaSchedule', 'RemoveOrphanTermmeta');

function RemoveOrphanUsermeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$UsersTable = $prefixTable.'users';
	$UsermetaTable = $prefixTable.'usermeta';
	$usermeta = $wpdb->get_results("SELECT umeta_id, user_id, meta_key FROM $UsermetaTable WHERE user_id NOT IN (SELECT ID FROM $UsersTable)");
    foreach ($usermeta as $meta) {
		if (empty($meta->user_id)) {
		    $wpdb->query("DELETE FROM $UsermetaTable WHERE meta_id = {$meta->umeta_id}");
		} else {
	    	delete_user_meta( $meta->user_id, $meta->meta_key );
		}
    }
}
add_action('OrphanUsermetaSchedule', 'RemoveOrphanUsermeta');

function RemoveDulicatedUsermeta(){
	global $wpdb;
	$prefixTable = $wpdb->get_blog_prefix();
	$UsermetaTable = $prefixTable.'usermeta';
	$_usermeta = array();
    $usermeta = $wpdb->get_results("SELECT um.umeta_id AS umeta_id, um.user_id AS user_id FROM $UsermetaTable um INNER JOIN (SELECT user_id, meta_key, meta_value, COUNT(*) FROM $UsermetaTable GROUP BY user_id, meta_key, meta_value HAVING COUNT(*) > 1) um2 ON um.user_id = um2.user_id AND um.meta_key = um2.meta_key AND um.meta_value = um2.meta_value");
    foreach($usermeta as $meta){
        $_usermeta[$meta->user_id][] = $meta->umeta_id;
    }
    foreach ($_usermeta as $user_id => $meta_ids) {
        array_pop($meta_ids);
		$wpdb->query("DELETE FROM $UsermetaTable WHERE umeta_id IN (" . implode( ',', $meta_ids ) . ")");
    }
}
add_action('DuplicatedUsermetaSchedule', 'RemoveDulicatedUsermeta');
