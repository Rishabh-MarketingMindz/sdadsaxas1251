<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
global $wpdb;
// get main site prefix
$main_prefix = $wpdb->get_blog_prefix(1);
// Get Media id by url

function getImgidbyURL($imgUrl){
  global $wpdb;
  $page_sites = $_POST['page_site'];
  $imgUrlquery = $wpdb->get_results("SELECT ID FROM ".$page_sites."posts WHERE guid='$imgUrl'");
  if(!empty($imgUrlquery)){
    return $imgUrlquery[0]->ID;
  }else{
    return false;
  }
}

$delArr = array();
$page_from = $_POST['page_from'];
$page_type = $_POST['page_type'];
$media_src = $_POST['media_src'];
$page_site = $_POST['page_site'];
$media_id = explode(',', $media_src);
$count_media = count($media_id);

// echo $page_from.' >> '.$page_type.' >> '.$media_src.' >> '.$page_site.' >> '.$main_prefix.' >> '.$count_media;
// echo "<pre>"; print_r($count_media);
//     echo '<br>';
$media_source = $_POST['media_source'];
$media_sources = explode(',', $media_source);
$counter = 0;
foreach ($media_sources as $sources) {
  $arr[$counter]['media_source'] = $sources;
  $counter++;
}
$counter2 = 0;
foreach ($media_id as $media_URLs) {
  $media_ids = getImgidbyURL($media_URLs);

  if($media_ids){
    $arr[$counter2]['media_found'] = 'found';
  }else{
    $arr[$counter2]['media_found'] = $media_URLs;
  }
  $attachmentdata = $wpdb->get_results("SELECT * FROM ".$page_site."posts WHERE ID='$media_ids'");
  $arr[$counter2]['post_id'] = $attachmentdata[0]->ID;
  $arr[$counter2]['post_image'] = $attachmentdata[0]->guid;
  $arr[$counter2]['post_title'] = $attachmentdata[0]->post_title;
  $arr[$counter2]['post_caption'] = $attachmentdata[0]->post_excerpt;
  $arr[$counter2]['post_description'] = $attachmentdata[0]->post_content;
  if($page_site == $main_prefix){
    $alternative_text = get_post_meta($attachmentdata[0]->ID,'_wp_attachment_image_alt',true);
  }else{
    $the_id = $attachmentdata[0]->ID;
    $alternative_text = $wpdb->get_results("SELECT meta_value from ".$page_site."postmeta WHERE meta_key='_wp_attachment_image_alt' AND post_id=$the_id" );
    $alternative_text = $alternative_text[0]->meta_value;
  }
  $arr[$counter2]['alttext'] = $alternative_text;
  $counter2++;
}
// echo "<pre>"; print_r($arr);
$data = '
<input type="hidden" name="site_prefix" value="'.$page_site.'" />
<div class="container">
  <div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-4 media_overlay_header">
      <label>Content type: </label><span class="grey"> '.$page_type.'</span> 
      <span> | </span>
      <label>Page name: </label><span class="grey"> '.$page_from.'</span>
      <h4 class="red">'.$count_media.' Media file(s) found</h4>
    </div>
  </div>
  <div class=" media_content_div">
';
foreach ($arr as $media_data) {
  if($media_data['media_found'] == 'found'){
    $post_id = $media_data['post_id'];
    $post_image = $media_data['post_image'];

    // Get media type start
    $media_type = get_headers($post_image);
    foreach ($media_type as $valued) { 
      if (strpos($valued, 'Content-Type') !== false) {
        $m_type = $valued;
      }
    }

    $variable = substr($m_type, 0, strpos($m_type, "/"));
    $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
    // Get media type end
    // echo $mediatype.'<br>';
    
    $post_title = $media_data['post_title'];
    $post_caption = $media_data['post_caption'];
    $post_description = $media_data['post_description'];
    $alttext = $media_data['alttext'];
    $media_source = $media_data['media_source'];
    $data .='
    <div class="row media_row">
      <div class="col-sm-3 col-md-3">';
        if($mediatype == 'image'){
          $data .='
          <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
        }elseif($mediatype == 'video'){
          $data .='
          <div>
            <video style="width: 100%;" controls>
              <source src="'.$post_image.'" type="video/mp4">
              Sorry, your browser doesnt support the video element.
            </video>
          </div>';
        }elseif($mediatype == "application"){
          $ext = pathinfo($post_image, PATHINFO_EXTENSION);
          if($ext == 'pdf'){
            $data .= '<embed src="'.$post_image.'" type="application/pdf">';
          }elseif($ext == 'docx' || $ext == 'doc'){
            $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
          }else{
            $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
          }
        }elseif($mediatype == "audio"){
          $data .='
          <audio controls="">
            <source src="'.$post_image.'" type="audio/ogg">
            <source src="'.$post_image.'" type="audio/mpeg">
            Your browser does not support the audio element.
          </audio>';
        }else{
          $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
        }
        $data .='
      </div>
      <input type="hidden" name="post_id[]" value="'.$post_id.'" />      
      <div class="col-sm-4 col-md-4">
        <div class="row">
          <div class="col-md-12 edit_media_col_12">
              <label class="media_label">Filename: </label><input class="edit_media_input" type="text" name="filename[]" placeholder="filename" value="'.$post_title.'">
          </div>
          <div class="col-md-12 edit_media_col_12">
              <label class="media_label">Alt text: </label><input class="edit_media_input" type="text" name="alt_text[]" placeholder="Alternative Text" value="'.$alttext.'">
            </div>
            <div class="col-md-12 edit_media_col_12">
              <label class="media_label">Caption: </label><input class="edit_media_input" type="text" name="caption[]" placeholder="Caption" value="'.$post_caption.'">
            </div>
            <div class="col-md-12 edit_media_col_12"> 
              <label class="media_label">Image Type: </label><input class="edit_media_input" type="text" name="img_type[]" value="'.$media_source.'">
            </div>
          </div>
          </div>
          <div class="col-sm-5 col-md-5">
            <label class="media_label">Description: </label>
            <textarea name="img_description[]" placeholder="Description">'.$post_description.'</textarea>
          </div>
        </div>
    ';
  }else{
    $post_image = $media_data['media_found'];
    if($post_image){
      // Get media type start
      $media_type = get_headers($post_image);
      foreach ($media_type as $valued) { 
        if (strpos($valued, 'Content-Type') !== false) {
          $m_type = $valued;
        }
      }

      $variable = substr($m_type, 0, strpos($m_type, "/"));
      $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
      // Get media type end
      
      $data .= '
      <div class="row media_row">
        <div class="col-sm-3 col-md-3">';
        if($mediatype == 'image'){
          $data .='
          <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
        }elseif($mediatype == "audio"){
          $data .='
          <audio controls="">
            <source src="'.$post_image.'" type="audio/ogg">
            <source src="'.$post_image.'" type="audio/mpeg">
            Your browser does not support the audio element.
          </audio>';
        }elseif($mediatype == 'video'){
          $data .='
          <div>
            <video style="width: 100%;" controls>
              <source src="'.$post_image.'" type="video/mp4">
              Sorry, your browser doesnt support the video element.
            </video>
          </div>';
        }elseif($mediatype == "application"){
          $ext = pathinfo($post_image, PATHINFO_EXTENSION);
          if($ext == 'pdf'){
            $data .= '<embed src="'.$post_image.'" type="application/pdf">';
          }elseif($ext == 'docx' || $ext == 'doc'){
            $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
          }
        }
        $data .='
        </div>
        <div class="col-sm-9 col-md-9 text-center">
          <strong class="red">This Image Content is Not Editable, Because It\'s Resize Image.</strong>
        </div>
      </div>';
    }
  }
}
$data .='</div></div>';
// $result = json_encode($delArr);
echo $data;