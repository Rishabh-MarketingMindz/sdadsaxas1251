<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
global $wpdb;
$count_delete = 0;

$delete_content_id = $_POST['deletecontent'];
$restore_content_id = $_POST['restorecontent'];
$action = $_POST['action'];
$site_prefix = $_POST['site_prefix'];
$site_id = $_POST['site_id'];
$prefixes = $_POST['prefixes'];
$bulk = array();
$i = 0;
foreach ($site_id  as $mediavalue) {
	$bulk[$i]['site_id'] = $mediavalue;
	$i++;
}
$j = 0;
foreach ($prefixes as $mediavalue) {
	$bulk[$j]['prefixes'] = $mediavalue;
	$j++;
}

if($action == "contentDeleteSingle"){
		$deleted_images = $wpdb->get_results("SELECT * FROM ".$site_prefix."posts WHERE post_status = 'trash' AND ID = '$delete_content_id'", ARRAY_A);
		if($deleted_images){
			$delete_post = $wpdb->query("DELETE FROM ".$site_prefix."posts WHERE ID = '$delete_content_id'");
			if($delete_post){
				$message = __("Content delete successfully","wp_media_cleaner");
				$response = array(
		    		'message'=> $message,
		    		'post_id'=> $delete_content_id,
		    		'status' => 'success'
		    	);
			}else{
				$message = __("Something went wrong, Please try again","wp_media_cleaner");
				$response = array(
		    		'message'=> $message,
		    		'status' => 'error'
		    	);
			}
		}
  
	if($site_prefix == ""){
		$delete_post = $wpdb->query("DELETE FROM ".$wpdb->prefix."posts WHERE ID = '$delete_content_id'");
		if($delete_post){
			$message = __("Content delete successfully","wp_media_cleaner");
			$response = array(
	    		'message'=> $message,
	    		'post_id'=> $delete_content_id,
	    		'status' => 'success'
	    	);
		}else{
			$message = __("Something went wrong, Please try again","wp_media_cleaner");
			$response = array(
	    		'message'=> $message,
	    		'status' => 'error'
	    	);
		}
	}
}elseif($action == "contentRestoreSingle"){
	$contentrestore = $wpdb->get_results("SELECT * FROM ".$site_prefix."posts WHERE post_status = 'trash' AND ID = '$restore_content_id'", ARRAY_A);
	if($contentrestore){
		$updaterec = $wpdb->query("UPDATE ".$site_prefix."posts SET post_status = 'publish' WHERE ID = '$restore_content_id'");
		if($updaterec){
			$message = __("Content Restore successfully","wp_media_cleaner");
			$response = array(
	    		'message'=> $message,
	    		'post_id'=> $restore_content_id,
	    		'status' => 'success'
	    	);
		}else{
			$message = __("Something went wrong, Please try again","wp_media_cleaner");
			$response = array(
	    		'message'=> $message,
	    		'status' => 'error'
	    	);
		}
	}
	if($site_prefix == ""){
		$updaterec = $wpdb->query("UPDATE ".$wpdb->prefix."posts SET post_status = 'publish' WHERE ID = '$restore_content_id'");
		if($updaterec){
			$message = __("Content Restore successfully","wp_media_cleaner");
			$response = array(
	    		'message'=> $message,
	    		'post_id'=> $restore_content_id,
	    		'status' => 'success'
	    	);
		}else{
			$message = __("Something went wrong, Please try again","wp_media_cleaner");
			$response = array(
	    		'message'=> $message,
	    		'status' => 'error'
	    	);
		}
	}
}elseif($action == "contentDeleteBulk"){
	foreach ($bulk as $media_value) {
			$site_id = $media_value['site_id'];
			$prefixes = $media_value['prefixes'];
			$deleted_images = $wpdb->get_results("SELECT * FROM ".$prefixes."posts WHERE post_status = 'trash' AND ID = '$site_id'");
			if($deleted_images){
			$delete_post = $wpdb->query("DELETE FROM ".$prefixes."posts WHERE ID = '$site_id'");
			if($delete_post){
				$message = __("Content delete successfully","wp_media_cleaner");
				$response = array(
		    		'message'=> $message,
		    		'post_id'=> $site_id,
		    		'status' => 'success'
		    	);
			}else{
				$message = __("Something went wrong, Please try again","wp_media_cleaner");
				$response = array(
		    		'message'=> $message,
		    		'status' => 'error'
		    	);
			}
		}
    }
}elseif($action == "contentRestoreBulk"){
	foreach ($bulk as $media_value) {
			$site_id = $media_value['site_id'];
			$prefixes = $media_value['prefixes'];
			$deleted_images = $wpdb->get_results("SELECT * FROM ".$prefixes."posts WHERE post_status = 'trash' AND ID = '$site_id'");
			if($deleted_images){
			$delete_post = $wpdb->query("UPDATE ".$prefixes."posts SET post_status = 'publish' WHERE ID = '$site_id'");
			if($delete_post){
				$message = __("Content Restore successfully","wp_media_cleaner");
				$response = array(
		    		'message'=> $message,
		    		'post_id'=> $site_id,
		    		'status' => 'success'
		    	);
			}else{
				$message = __("Something went wrong, Please try again","wp_media_cleaner");
				$response = array(
		    		'message'=> $message,
		    		'status' => 'error'
		    	);
			}
		}
    }
}
echo json_encode($response);
die();

?>
