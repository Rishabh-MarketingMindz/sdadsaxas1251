<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
$count_delete = 0;
$delArr = array();
$content_src = $_POST['content_src'];
$isbackup = $_POST['backup'];
foreach ($content_src as $content_data) {
	$extract[] = explode(",", $content_data);
}

if($isbackup == "yes"){
    $inc=0;
	foreach ($extract as $dataArray) {
		$post_id = $dataArray[0];
		$post_type = $dataArray[1];
		$prefixValue = $dataArray[2];
		$get_results = $wpdb->get_results("SELECT * from ".$prefixValue."posts WHERE post_type='$post_type' AND ID='$post_id' OR post_status ='publish'");
		if(!empty($get_results)){
			$main_post = $wpdb->query('UPDATE '.$prefixValue.'posts SET post_status = "trash" WHERE ID = "'.$post_id.'"');
			if(!empty($main_post)){
				// Deleting post revisions
				$child_post = $wpdb->query('UPDATE'.$prefixValue.'posts WHERE post_parent = "'.$post_id.'" AND post_type="revision" AND post_status = "trash"');
				// Deleting post meta
				$postmeta_delete = $wpdb->get_results("UPDATE ".$prefixValue."postmeta WHERE post_id='$post_id'" );
			}
			if(!empty($main_post)){
				$count_delete += 1;
				$delArr[] = $post_id.','.$post_type.','.$prefixValue;
				$inc++;
			}
		}
	}

}else{
	$inc=0;
	foreach ($extract as $dataArray) {
		$post_id = $dataArray[0];
		$post_type = $dataArray[1];
		$prefixValue = $dataArray[2];
		$get_results = $wpdb->get_results("SELECT * from ".$prefixValue."posts WHERE post_type='$post_type' AND ID='$post_id'");
		if(!empty($get_results)){
			$main_post = $wpdb->query('DELETE FROM '.$prefixValue.'posts WHERE ID = "'.$post_id.'"');
			if(!empty($main_post)){
				// Deleting post revisions
				$child_post = $wpdb->query('DELETE FROM '.$prefixValue.'posts WHERE post_parent = "'.$post_id.'" AND post_type="revision"');
				// Deleting post meta
				$postmeta_delete = $wpdb->get_results("DELETE FROM ".$prefixValue."postmeta WHERE post_id='$post_id'" );
			}
			if(!empty($main_post)){
				$count_delete += 1;
				$delArr[] = $post_id.','.$post_type.','.$prefixValue;
				$inc++;
			}
		}
	}
}
// $request_total = count($extract);
// if($request_total == $count_delete){
// 	$delArr['message'] = 'All selected files are successfully deleted.';
// 	$delArr['type'] = '1';
// }else{
// 	if($count_delete > 1){
// 		$delArr['message'] = $count_delete.' files are deleted, Some of data is not deleted due to server error.';
// 		$delArr['type'] = '1';
// 	}elseif($count_delete == 1){
// 		$delArr['message'] = '1 file is deleted, Some of data is not deleted due to server error.';
// 		$delArr['type'] = '1';
// 	}else{
// 		$delArr['message'] = 'Something went wrong, Please try again';
// 		$delArr['type'] = '2';
// 	}
// }
$result = json_encode($delArr);
echo $result;