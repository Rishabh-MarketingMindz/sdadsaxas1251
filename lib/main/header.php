<div class="WPMC_header">
	<a href="#">
		<img src="<?php echo WPMC_IMAGE . DIRECTORY_SEPARATOR . 'logo.png' ?>" class="WPMC_logo" />
	</a>
</div>
<?php
// Check if root directory and their subdirectory have writable permission
$is_writable = directoryIsWritable(ABSPATH);
if ( !empty($is_writable) ) : ?>
  <div class="WPMC_settings">
    <?php
    $_GET['type']= "access_denied";
    include(WPMC_MAIN . DIRECTORY_SEPARATOR . 'wpmc_notice.php'); ?>
  </div>
<?php
endif;