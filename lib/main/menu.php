<?php
add_action('admin_menu', 'WPMC_Menu');
 
function WPMC_Menu(){
	add_menu_page( 'WP Media Cleaner', 'Media Cleaner', 'manage_options', 'media-cleaner-setting', 'MediaCleanerSetting', '', 30);

    add_submenu_page('media-cleaner-setting', 'Settings', 'Settings', 'manage_options', 'media-cleaner-setting', 'MediaCleanerSetting' );
    add_submenu_page('media-cleaner-setting', 'Media Scanner', 'Media Scanner', 'manage_options', 'media-cleaner-media-scan', 'MediaCleanerMediaScan' );
    add_submenu_page('media-cleaner-setting', 'Content Scanner', 'Content Scanner', 'manage_options', 'media-cleaner-content-scan', 'MediaCleanerContentScan' );
    add_submenu_page('media-cleaner-setting', 'Image Optimize', 'Image Optimize', 'manage_options', 'media-cleaner-image-optimize', 'MediaCleanerImageOptimize' );
    add_submenu_page('media-cleaner-setting', 'Database Cleaner', 'Database Cleaner', 'manage_options', 'media-cleaner-database-cleaner', 'MediaCleanerDatabaseCleaner' );
    add_submenu_page('media-cleaner-setting', 'Backup', 'Backup', 'manage_options', 'media-cleaner-backup', 'MediaCleanerBackup' );
}

function MediaCleanerSetting(){
    require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'settings.php';
}
function MediaCleanerMediaScan(){
    require_once WPMC_MEDIA_SCAN . DIRECTORY_SEPARATOR . 'media-scan.php';
}
function MediaCleanerContentScan(){
    require_once WPMC_CONTENT_SCAN . DIRECTORY_SEPARATOR . 'content-scan.php';
}
function MediaCleanerImageOptimize(){
    require_once WPMC_IMAGE_OPTIMIZE . DIRECTORY_SEPARATOR . 'image-optimize.php';
}
function MediaCleanerDatabaseCleaner(){
    require_once WPMC_DATABASE_CLEANER . DIRECTORY_SEPARATOR . 'database-cleaner.php';
}
function MediaCleanerBackup(){
    require_once WPMC_BACKUP . DIRECTORY_SEPARATOR . 'backup.php';
}