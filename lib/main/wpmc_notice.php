<?php
$type = $_GET['type'];

if($type == "access_denied"):
?>
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>    
    <p><strong><?php _e("Cannot Access Directories.","wp_media_cleaner"); ?></strong>&nbsp;<?php _e("There are some directories that do not have writable permission. Please make all the directory writable.","wp_media_cleaner"); ?>
</div>
<?php elseif($type == "FS_issue") : ?>
<div class="row alert alert-danger fs_error_box">
	<div class="col-md-2">
		<img class="error-icon" src="<?php echo WPMC_URL; ?>/assets/images/error-icon.png">
	</div>
	<div class="col-md-10 fs_error_msg_box">
		<strong><?php _e("FS method is not defined.","wp_media_cleaner"); ?></strong>&nbsp;<?php _e("Your sytem is running on ".PHP_OS." operating system environment.","wp_media_cleaner"); ?><br /><?php _e("Please follow instruction from","wp_media_cleaner"); ?> <strong><?php _e("System requirements","wp_media_cleaner"); ?></strong> <?php _e("in","wp_media_cleaner"); ?> <strong><?php _e("setting page.","wp_media_cleaner"); ?></strong> <?php _e("You must define FS method in your config file.","wp_media_cleaner"); ?> <a href="<?php echo get_admin_url(); ?>admin.php?page=media-cleaner-setting"><?php _e("Go to setting page","wp_media_cleaner"); ?></a>
	</div>
</div>
<?php endif; ?>