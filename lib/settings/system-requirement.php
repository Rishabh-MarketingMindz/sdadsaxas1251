<div class="system_requirements_block">
  <div class="panel-group" id="accordion1">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion1" href="#collapse1">WordPress environment</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">          
          <ul class="list-group notice_list">
            <li class="list-group-item">The current version of the WordPress is <strong><?php echo get_bloginfo( 'version' ); ?> </strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-group" id="accordion2">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2">O.S. environment</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse in">
        <div class="panel-body">
          <?php
          $os_message = (strtoupper(substr(PHP_OS, 0, 3)) != 'WIN') ? "Which need the file permission to access directories." : "";
          $fs_message = (@FS_METHOD == "direct" && strtoupper(substr(PHP_OS, 0, 3)) != 'WIN') ? "direct." : "not defined. Please follow the steps given below:
          <ul class='system_req_ul notice_list'>
              <li>Open <strong>config.php</strong> file.</li>
              <li>Add this line <code>define('FS_METHOD', 'direct');</code> above the <code>/* That's all, stop editing! Happy publishing. */</code> and save the file. Now you can use the plugin's all the functionality.</li>
          </ul>";
          ?>
          <ul class="list-group notice_list">
            <li class="list-group-item">The system is running on <strong><?php echo PHP_OS; ?> operating system</strong> environment. <?php echo $os_message; ?></li>
            <li class="list-group-item">Currently is FS method is <?php echo $fs_message; ?></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-group" id="accordion3">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion3" href="#collapse3">File Permissions</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse in">
        <div class="panel-body">
          <ul class="list-group notice_list">
            <li class="list-group-item">
              <?php
              // Check if root directory and their subdirectory have writable permission
              $is_writable = directoryIsWritable(ABSPATH);              
              if ( !empty($is_writable) ) :
                _e("<span class='text-danger'>Some of the directories and files do not have writable permission. Please allow them permission so the plugin can access directories. You can change back to the permission once your work is completed.</span>","wp_media_cleaner");
              else :
                _e("<span class='text-success'>Directory and files have writable permission.</span>","wp_media_cleaner");
              endif; ?>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
