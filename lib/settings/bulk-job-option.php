<div class="bulk_job_form_block">
	<form action="#" method="post" class="bulk_job_option_form">
		<div class="license_form_input">
			<span><?= __('Chunk Size') ?></span>
			<input type="text" name="chunk_size" class="license_key_input">
		</div>
		<div class="bulk_job_form_description">
			<p><?= __('Split file into chunks containing the specified number of records.') ?></p>
			<p><?= __('If Empty WP Media Cleaner will calculate the best settngs') ?></p>
		</div>
	</form>
</div>