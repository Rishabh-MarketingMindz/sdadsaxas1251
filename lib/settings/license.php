<div class="license_form_block">
	<form action="#" method="post" class="license_form">
		<div class="license_form_input">
			<span><?= __('Activation') ?></span>
			<div class="col-md-6"><input autocomplete="false" type="text" id="license_key" class="license_key_input" placeholder="<?= __('Please insert license key') ?>" value="<?php echo $lisence_key ? $lisence_key : ""; ?>" <?php echo $lisence_key ? "disabled" : ""; ?>></div>
			<input type="submit" id="validate_lisence" name="submit" value="<?php echo __("Enter","wp_media_cleaner"); ?>" <?php echo $lisence_key ? "disabled" : ""; ?>>
		</div>
		<div class="license_form_action">
			<p class="if_license_activated"><?php echo $lisence_key ? __('Standard license activated') : ""; ?></p>
			<p class="license_form_action_input">
				<a href="javascript:void(0)"><?= __("Upgrade License") ?></a> | <input <?php echo $lisence_key ? "" : "disabled"; ?> id="deactivate_license" type="button" value="<?= __('Deactivate License for this site') ?>">
			</p>
		</div>
	</form>
</div>