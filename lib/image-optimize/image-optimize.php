<?php
$current_dir = dirname(__FILE__);
$current_dir = str_replace("/wp-content/plugins/wp_media_cleaner/lib/image-optimize", "", $current_dir);
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';
global $wpdb;
$keep_original_image = $wpdb->get_results("SELECT option_value FROM ".$wpdb->prefix."options WHERE option_name = 'wp_media_keep_original'");
// echo "<pre>"; print_r($keep_original_image);
@$keep_original_image = $keep_original_image[0]->option_value;
if($keep_original_image == "keep_original"){ $ori_img = "selected"; }else{ $ori_img = ""; }

// Getting all deleted images
$deleted_images = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."optimizer_backup WHERE type = 'delete_backup' OR type = 'delete_backup_meta'");

// check if Image optimizer page is active
@$image_opt = get_option('image_optimize_activate');
@$lisence_activation = get_option('WPMC_lisence_activation');
if($image_opt == "yes" && $lisence_activation == 1){
?>
<style type="text/css">
  /*CSS FOR PROGRESS LOADER BAR*/
  #myProgress {
    width: 100%;
    background-color: #fff;
    border-radius: 5px;
    overflow: hidden;
    margin-left: 10px;
    position: relative;
  }
  #myBar {    
    width: 0%;
    height: 30px;
    background-color: #f24e85;
  }
  .progress_center {
      display: flex;
      align-items: center;
  }
  #myProgress #progress_counter {
      position: absolute;
      top: 5px;
      left: 50%;
      transform: translateX(-50%);
      color: #000;
      z-index: 1;
      margin: 0;
  }
  /*CSS FOR PROGRESS LOADER BAR ENDS */

  .exclude_bulk_media{
    display: none;
  }
  /*Excluded image css*/
  .yes_excluded .img_name{
    /*background-color: rgba(229, 50, 115, 0.14) !important;*/
    color: #f24e85 !important;
  }
  table.dataTable thead tr th {
    border: none;
  }
  table.dataTable {
    border: none;
  }
  .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: 1px solid #ddd;
    border-top: 1px solid #ddd;
  }
  .btn-blue, .multiselect.dropdown-toggle.btn.btn-default {
    background-color: #4DB9AB;
    color: #fff;
  }
  .btn-red {
    background-color: #E53373;
    color: #fff;
  }
  /*small loader*/
  .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 60px;
    height: 60px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
    margin: 0 auto;
  }
   @-webkit-keyframes spin {
   0% {
   -webkit-transform: rotate(0deg);
  }
   100% {
   -webkit-transform: rotate(360deg);
  }
  }
   @keyframes spin {
   0% {
   transform: rotate(0deg);
  }
   100% {
   transform: rotate(360deg);
  }
  }
</style>
<div class="alert-box"></div>

<!--Restore Deleted Images Lists Modal -->
<div id="restoreDeletedImagesPopUp" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 920px"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <table id="restore_img" class="table table-striped table-sm text-left" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th><?php _e("Image URL","wp_media_cleaner"); ?></th>
              <th><?php _e("Action","wp_media_cleaner"); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($deleted_images as $deleted_urls) {
              $Image_url = $deleted_urls->Image_url;
              $blog_id = $deleted_urls->blog_id;
              ?>
              <tr>
                <td><?php echo $Image_url; ?></td>
                <td><strong class="red"><a class="restore_deleted_images" data-url="<?php echo $Image_url; ?>" data-blogid="<?php echo $blog_id; ?>" href="javascript:void(0)"><?php _e("Restore","wp_media_cleaner"); ?></a></strong></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
      </div>
    </div>    
  </div>
</div>
<!--Restore Deleted Images Lists Modal ends -->

<!-- Confirmation for restore deleted images Modal -->
<div id="ConfirmRestoreDeletedImg" class="modal fade" role="dialog">
  <!-- Getting image URL in the hidden field for ajax -->
  <input type="hidden" id="deleted_url">
  <input type="hidden" id="deleted_blog_id">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to restore this images ?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="ConfirmRestoreDelImgs()">Yes</button>
        <button type="button" class="btn btn-red" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for restore deleted images Modal ends -->

<!-- Confirmation for restore deleted images Modal -->
<div id="bulk_restore_original_images" class="modal fade" role="dialog">
  <!-- Getting image URL in the hidden field for ajax -->
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to restore selected original images ?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="ConfirmRestoreOriginalImages()">Yes</button>
        <button type="button" class="btn btn-red" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for restore deleted images Modal ends -->

<!-- Confirm taking media backup Modal -->
<div id="mediaDeleter" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Do you want to make a backup before deleting your files?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="DeleteFilteredImages('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" id="no" class="btn btn-red" onclick="DeleteFilteredImages('no')"><?php _e("No, Delete them","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Confirm taking media backup Modal ends --> 

<!-- Confirmation for bulk optimize images Modal -->
<div id="bulkMediaOptimizer" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to optimize selected images ?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="BulkOptimizeImages('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("No","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for bulk optimize images Modal ends --> 

<!-- Confirmation for single delete image Modal -->
<div id="singleMediaDelete" class="modal fade" role="dialog"> 
  <!-- Getting image URL in the hidden field for ajax -->
  <input type="hidden" id="singleImageSrc">
  <input type="hidden" id="singleDeleteImagePrefix">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Do you want to make a backup before deleting this file? ','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="SingleMediaDelete('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" id="no" class="btn btn-red" onclick="SingleMediaDelete('no')"><?php _e("No, Delete it","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for single delete image Modal ends --> 

<!-- Confirmation for single delete image Modal -->
<div id="singleMediaOptimizer" class="modal fade" role="dialog"> 
  <!-- Getting image URL in the hidden field for ajax -->
  <input type="hidden" id="singleImageUrl">
  <input type="hidden" id="singleImagePrefix">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to optimize this images ?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="SingleMediaOptimizer('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("No","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for single delete image Modal ends --> 

<!-- Confirmation for deleting original image Modal -->
<div id="deleteOriginal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <input type="hidden" id="del_original_file_url">
        <h4>
          <?php _e('Are you sure you want to delete the original image? There is no way to undo this action.','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="deleteOriginalImages('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("No","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for deleting original image Modal ends --> 

<!-- Confirmation for restore backup for image Modal -->
<div id="mediaRestore" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <input type="hidden" id="restored_file_url">
        <input type="hidden" id="restored_file_prefix">
        <h4>
          <?php _e('Are you sure you want to restore the original image ?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="RestoreFilteredImages('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("No","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation for restore backup for image Modal ends -->

<!-- Exclude bulk media Modal start -->
<!-- <div id="excludeMediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to perform this action?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" onclick="excludeMedia()">Yes</button>
        <button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div> -->
<!-- Exclude bulk media Modal ends -->

<!-- filtered images resultant div -->
<div id="filtered_image_result" style="display: none">  
</div>
<div class="WPMC_media_scan"> 
  <!-- Rotating logo -->
  <div class="loading" style="display: none">
    <div class="loading_box"><?php _e("Scanning for Images","wp_media_cleaner"); ?><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span> 
      <div class="progress_center">
        <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
        <div id="myProgress">
          <span id="progress_counter"></span>
          <div id="myBar"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Rotating logo for actions-->
  <div class="action_loader">
    <div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span><?php _e("LOADING","wp_media_cleaner"); ?></span> </div>
  </div>
  <!-- Only Rotating logo -->
  <div class="loading_rotating" style="display: none;">
    <div class="loading_box"><div class="action_message"></div><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
    </div>
  </div>
  <!-- CMS usage data from w3techs.com / captured 7/6/16 -->

  <div style="display: none;">
    <!-- All message for the actions (like deltetion, exclusion, optimization...etc) -->
    <span class="action_message_bulk_optimization"><?php _e("Optimizing Images","wp_media_cleaner"); ?></span>
    <span class="action_message_bulk_deletion"><?php _e("Deleting Images","wp_media_cleaner"); ?></span>
    <span class="action_message_bulk_restore_image"><?php _e("Restoring Original Images","wp_media_cleaner"); ?></span>
    <span class="action_message_optimization"><?php _e("Optimizing This Images","wp_media_cleaner"); ?></span>
    <span class="action_message_exclusion"><?php _e("Excluding Files","wp_media_cleaner"); ?></span> 
    <span class="action_message_remove_exclusion"><?php _e("Removing Exclusion","wp_media_cleaner"); ?></span>
    <span class="action_message_exclusion_both"><?php _e("Excluding Files/Removing Exclusion","wp_media_cleaner"); ?></span>
    <span class="action_message_remove_original_image"><?php _e("Removing Original Image","wp_media_cleaner"); ?></span>
    <span class="action_message_restore_image"><?php _e("Restoring This Original Images","wp_media_cleaner"); ?></span>
    <span class="action_message_deletion"><?php _e("Deleting This Images","wp_media_cleaner"); ?></span>
  </div>

  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="scan_images" type="button" onclick="scanImages()"><?php _e("Ready to scan","wp_media_cleaner"); ?></button>
      </li>
      <li> <span class="text"><?php _e("Filter:","wp_media_cleaner"); ?></span>
        <select name="fileTypeOpt[]" multiple id="fileTypeOpts" class="btn btn-blue" style="display: none;">
        </select>
        <select name="fileTypeOptDemo[]" multiple id="fileTypeOptDemo" class="btn btn-blue">
          <option><?php echo __("Click on Ready to Scan","wp_media_cleaner"); ?></option>
        </select>
      </li>
      <li>
        <input type="button" id="datePicker" name="daterange" value="Date Picker" class="btn btn-blue" />
      </li>
      <li>
        <select name="postTypeOpt[]" class="btn btn-blue" multiple id="postTypeOpt" style="display: none;">
        </select>
        <select name="postTypeOptDemo[]" class="btn btn-blue" multiple id="postTypeOptDemo">
          <option><?php echo __("Click on Ready to Scan","wp_media_cleaner"); ?></option>
        </select>
      </li>
      <li>
        <label class="gray"><span id="total_files"></span><span id="no_files"><?php _e("No","wp_media_cleaner"); ?></span> <?php _e("media files found","wp_media_cleaner"); ?></label>
        <label class="red"><?php _e("Original size","wp_media_cleaner"); ?> <span id="media_size"></span><span id="no_size">- 0.0 MB</span> - <?php _e("Compressed size","wp_media_cleaner"); ?> <span id="media_size_compressed"></span><span id="no_size_compressed">0.0MB</span></label>
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="abcd" class="table custom_center table-striped table-sm table_design" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th><input name="show_all_check" type="checkbox"></th>
          <th><?php _e("Images Files","wp_media_cleaner"); ?>
          </th>
          <th><?php _e("URL","wp_media_cleaner"); ?></th>
          <th align="center"><?php _e("Upload Date","wp_media_cleaner"); ?></th>
          <th><?php _e("Original size <br>and compression","wp_media_cleaner"); ?></th>
        </tr>
      </thead>
      <tbody id="media_scanned_result">
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="display: none;">&nbsp;</td>
          <td style="display: none;">&nbsp;</td>
          <td colspan="5" align="center"><strong class="red"><?php _e("Ready to scan !","wp_media_cleaner"); ?></strong></td>
          <td style="display: none;">&nbsp;</td>
          <td style="display: none;">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-12">
    <div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
      <span class="processbar_results"></span> </div>
  </div>
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-blue img_optimize_btn" type="button" data-toggle="modal" data-target="#bulkMediaOptimizer"><?php _e("Optimize","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-red delete_button" id="" type="button" data-toggle="modal" data-target="#mediaDeleter"><?php _e("Delete original Images","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-red restoreOriginalImages" type="button" data-toggle="modal" data-target="#bulk_restore_original_images"><?php _e("Restore original Images","wp_media_cleaner"); ?></button>
      </li>
      <!--
      <li>
        <button class="btn btn-red" type="button" data-toggle="modal" data-target="#restoreDeletedImagesPopUp"><?php _e("Restore deleted Images","wp_media_cleaner"); ?></button>
      </li> -->
      <li>
        <button class="btn btn-red exclude_bulk_media" type="button" onclick="excludeMedia()"><?php _e("Exclude Media","wp_media_cleaner"); ?></button>
        <button class="btn btn-red show_excluded_media" type="button"><?php _e("Show Excluded Media","wp_media_cleaner"); ?></button>
      </li>
      <li> <span class="text"><?php _e("Settings:","wp_media_cleaner"); ?></span> <span class="dropdown">
        <button class="btn btn-blue dropdown-toggle" id="img_compression" data-toggle="dropdown" type="button"><?php _e("Compression","wp_media_cleaner"); ?></button>
        <div class="dropdown-menu range_compression" role="menu" aria-labelledby="img_compression"> 
          <!-- Here will be the extension's compression quality results --> 
        </div>
        </span> </li>
      <li>
        <select name="wp_options" class="btn btn-blue" multiple id="wp_options">
          <option value="keep_original" <?php echo $ori_img; ?>><?php _e("Keep Original","wp_media_cleaner"); ?></option>
        </select>
      </li>
      <li>
        <button class="btn btn-blue" id="showfilteredimages" type="button"><?php _e("Show Filtered Media","wp_media_cleaner"); ?></button>
        <button class="btn btn-blue" id="showAllFilteredImages" type="button"><?php _e("Show All Media","wp_media_cleaner"); ?></button>
      </li>
    </ul>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
<script type="text/javascript">
  jQuery(window).load(function(){
    setTimeout(function() {
        jQuery('.action_loader').fadeOut('fast');
    }, 3000);
  });
  // Select and  Deselect all checkbox 
  jQuery("input[name=show_all_check]").change(function(){
    if(jQuery(this).prop("checked") == true){
      jQuery(".delete_button").prop("disabled",false);
      jQuery(".img_optimize_btn").prop("disabled",false);
      jQuery(".restoreOriginalImages").prop("disabled",false);
      jQuery("input[name=show_all_check]").prop('checked', true);
      jQuery("input[name=check_list1]").prop('checked', true);
      jQuery("input[name=check_list]").prop('checked', true);
      var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
      if(atLeastOneIsChecked){
        // Check if selected row is excluded
        var row_excluded = jQuery('input[name=check_list]:checked').map(function(){
          var has_class = jQuery(this).parent().parent().attr('class');
          if(has_class.includes("yes_excluded")){
            return "excluded";
          }else{
            return "no_excluded";
          }
        }).get();
        var if_excluded = jQuery.inArray( "excluded", row_excluded );
        if(if_excluded != -1){
          jQuery(".exclude_bulk_media").html("Remove Exclusion");
          jQuery(".show_excluded_media").hide();
          jQuery(".exclude_bulk_media").show();
        }else{
          jQuery(".exclude_bulk_media").html("Exclude Media");
          jQuery(".show_excluded_media").hide();
          jQuery(".exclude_bulk_media").show();
        }
      }else{
        jQuery(".show_excluded_media").show();
        jQuery(".exclude_bulk_media").hide();
      }
    }else{
      jQuery(".delete_button").prop("disabled",true);
      jQuery(".img_optimize_btn").prop("disabled",true);
      jQuery(".restoreOriginalImages").prop("disabled",true);
      jQuery("input[name=show_all_check]").prop('checked', false);
      jQuery("input[name=check_list1]").prop('checked', false);
      jQuery("input[name=check_list]").prop('checked', false);
      jQuery(".show_excluded_media").show();
      jQuery(".exclude_bulk_media").hide();
    }
  });
  jQuery(document).ready(function() {
    // Show filtered images
    jQuery("#showfilteredimages").click(function(){
      jQuery("#media_scanned_result input[name=check_list]:checkbox:not(:checked)").parent().parent().hide();
      jQuery("#media_scanned_result input[name=check_list1]:checked").parent().parent().show();
    });
    // Get back to filtered all images
    jQuery("#showAllFilteredImages").on('click',function(){
      if(jQuery("#datePicker").val() != 'Date Picker'){
        var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
        dependFilter(startDate,endDate);
      }else{
        dependFilter();
      }
    });
    // Show only excluded media
    jQuery(document).on("click",".show_excluded_media",function(e) {
      jQuery("#media_scanned_result *input[name=check_list][data-excluded='Exclude']").map(function(){
        return jQuery(this).attr('name', 'check_list1');
      }).get();
      jQuery("#media_scanned_result *input[name=check_list1][data-excluded='Exclude']").map(function(){
        return jQuery(this).attr('name', 'check_list1');
      }).get();
      jQuery("#media_scanned_result input[name=check_list][data-excluded='Exclude']").parent().parent().hide();
      jQuery("#media_scanned_result input[name=check_list1][data-excluded='Exclude']").parent().parent().hide();

      jQuery("#media_scanned_result input[name=check_list][data-excluded='Excluded']").parent().parent().show();
      jQuery("#media_scanned_result input[name=check_list1][data-excluded='Excluded']").parent().parent().show();
    });
    // Dummy filter for showing before scanning
    jQuery('#fileTypeOptDemo').multiselect({
      nonSelectedText: 'File Type',
      allSelectedText: 'File Type',
      onChange: function(option, checked) {          
      }
    });
    jQuery('#postTypeOptDemo').multiselect({
      nonSelectedText: 'Post Type',
      allSelectedText: 'Post Type',
      onChange: function(option, checked) {          
      }
    });

    jQuery(".delete_button").prop("disabled",true);
    jQuery(".img_optimize_btn").prop("disabled",true);
    jQuery(".restoreOriginalImages").prop("disabled",true);

    // Show only excluded media
    // jQuery(document).on("click",".show_excluded_media",function(e) {
    //   jQuery("#filtered_image_result").html("");
    //   var checkValues = jQuery('input[name=check_list]').map(function(){
    //     if_excluded = jQuery(this).data("excluded");
    //     if(if_excluded == "Excluded"){
    //             var vals = jQuery(this).parent().parent().html();
    //             jQuery("#filtered_image_result").append('<tr role="row" class="odd">'+vals+'</tr>');
    //         }
    //       }).get();
    //       var get_result = jQuery("#filtered_image_result").html();
    //       jQuery("#media_scanned_result").html(get_result);
    // });    
    
    jQuery(document).on("click","input[name=check_list]",function() {
      // Disable action button if checkbox not selected
      var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
      if(atLeastOneIsChecked){
        jQuery(".delete_button").prop("disabled",false);
        jQuery(".img_optimize_btn").prop("disabled",false);
        jQuery(".restoreOriginalImages").prop("disabled",false);

        // Check if selected row is excluded
        var row_excluded = jQuery('input[name=check_list]:checked').map(function(){
          var has_class = jQuery(this).parent().parent().attr('class');
          if(has_class.includes("yes_excluded")){
            return "excluded";
          }else{
            return "no_excluded";
          }
        }).get();
        var if_excluded = jQuery.inArray( "excluded", row_excluded );
        if(if_excluded != -1){
          jQuery(".exclude_bulk_media").html("Remove Exclusion");
          jQuery(".show_excluded_media").hide();
          jQuery(".exclude_bulk_media").show();
        }else{
          jQuery(".exclude_bulk_media").html("Exclude Media");
          jQuery(".show_excluded_media").hide();
          jQuery(".exclude_bulk_media").show();
        }
      }else{
        jQuery(".show_excluded_media").show();
        jQuery(".exclude_bulk_media").hide();
        jQuery(".delete_button").prop("disabled",true);
        jQuery(".img_optimize_btn").prop("disabled",true);
        jQuery(".restoreOriginalImages").prop("disabled",true);
      }
    });
    // Restore Deleted Images
    jQuery(".restore_deleted_images").click(function(){
      jQuery("#ConfirmRestoreDeletedImg").modal('toggle');
      var blog_id = jQuery(this).data("blogid");
      jQuery("#deleted_blog_id").attr('value', '').val(blog_id);
      var url = jQuery(this).data("url");
      jQuery("#deleted_url").attr('value', '').val(url);      
    });
    // Fetch quality slider values from ajax start
    jQuery.ajax({
        url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
        type: "POST",
          data: {
        type: 'get_extensions_quality'
      },
      success: function(response) {
        jQuery(".range_compression").empty().append(response);
          },
          error: function (ErrorResponse) {
              // console.log(ErrorResponse);
          }
      });
      // Fetch quality slider values from ajax ends
      var $chkboxes = jQuery('.chkbox');
      var lastChecked = null;
      jQuery(document).on("click",".chkbox",function(e) {       
          if (!lastChecked) {
              lastChecked = this;          
              return;
          }
          if (e.shiftKey) {           
              var start = jQuery('.chkbox').index(this);
              var end = jQuery('.chkbox').index(lastChecked);
              // console.log(end);
              
              jQuery('.chkbox').slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
          }
          lastChecked = this;
      });
      jQuery(document).on("change",".range-slider_range",function(e) {        
        var quality_value = jQuery(this).val();
        var image_type = jQuery(this).data("format");
        jQuery.ajax({
          type: "POST",
          dataType: "json",
          url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
          data: {
            type: "update_quality",
            compress_amount: quality_value,
            image_type: image_type

          },
          cache: false,
          success: function(response){
          },
          error: function (jqXHR, exception) {
            alert("Something went wrong. Please try again.");
          }
        });
      });
      // Single exclude functionality script
      jQuery(document).on("click",".singleExclude",function(e){
        jQuery(".loading_rotating .action_message").empty();
        jQuery('.loading_rotating').show();
        var is_excluded = jQuery(this).data("excluded");
        var media = jQuery(this).data("media");
        var action_to = "";
        if(is_excluded == "Excluded"){
          jQuery(this).html("Exclude");
          jQuery(this).data('excluded','Exclude');
          action_to = "remove_excluded";
          jQuery(this).parents("tr").removeClass("yes_excluded");
        }else{
          action_to = "insert_excluded";
          jQuery(this).html("Excluded");
          jQuery(this).data('excluded','Excluded');
          jQuery(this).parents("tr").addClass("yes_excluded");
        }
        jQuery.ajax({
        type: "POST",
        url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
        // dataType: "json",
        data: {
          type: "excluded_media",
          media_src: media,
          exclude_action: action_to

        },
        cache: false,
        success: function(response){
          jQuery('.loading_rotating').hide();
          if(response == 'success'){
            if(is_excluded){
                jQuery(this).html("Exclude");
                jQuery(this).data('excluded','Exclude');
              }else{
                jQuery(this).html("Excluded");
                jQuery(this).data('excluded','Excluded');
              }
            }else{
              if(is_excluded){
                jQuery(this).html("Excluded");
                jQuery(this).data('excluded','Excluded');
              }else{
                jQuery(this).html("Exclude");
                jQuery(this).data('excluded','Exclude');
              }
            }
        },
        error: function (jqXHR, exception) {
          jQuery('.loading_rotating').hide();
          if(is_excluded){
              jQuery(this).html("Excluded");
              jQuery(this).data('excluded','Excluded');
            }else{
              jQuery(this).html("Exclude");
              jQuery(this).data('excluded','Exclude');
            }
          alert("Something went wrong. Please try again.");
        }
      });
      })
  });
    jQuery('#getfilteredimages').toggle(function(){
          jQuery("input[name=check_list]").prop('checked', true);
          jQuery(this).text("Deselect Filtered Images");
      },function(){
          jQuery("input[name=check_list]").prop('checked', false);
          jQuery(this).text("Select Filtered Images");
    })
    jQuery(document).on("click",".delete_original",function(e){
      jQuery("#deleteOriginal").modal('toggle');
      var get_image_url = jQuery(this).data("url");
      jQuery("#del_original_file_url").val(get_image_url);
    });
    jQuery(document).on("click",".restore_img",function(e){
      jQuery("#mediaRestore").modal('toggle');
      var get_image_url = jQuery(this).data("url");
      var get_image_prefix = jQuery(this).data("prefix");
      jQuery("#restored_file_url").val(get_image_url);
      jQuery("#restored_file_prefix").val(get_image_prefix);
    });
    // Script to put single image url value in hidden field: Delete single image
    jQuery(document).on("click",".singleDel",function(e){
      var media = jQuery(this).data("media");
      var prefix = jQuery(this).data("prefix");
      jQuery("#singleImageSrc").val(media);
      jQuery("#singleDeleteImagePrefix").val(prefix);
    });    
    // Script to put single image url value in hidden field: Optimize single image
    jQuery(document).on("click",".singleOptimize",function(e){
      var media = jQuery(this).data("media");
      var prefix = jQuery(this).data("prefix");
      jQuery("#singleImageUrl").val(media);
      jQuery("#singleImagePrefix").val(prefix);
    });
    // Script for image compression quality
  var rangeSlider = function () {
    var slider = $('.range-slider'),
    range = $('.range-slider_range'),
    value = $('.range-slider_value');
    slider.each(function () {
      value.each(function () {
        var value = $(this).prev().attr('value');
        $(this).html(value);
      });
      range.on('input', function () {
        $(this).next(value).html(this.value);
      });
    });
  };
  rangeSlider();

  function deleteOriginalImages($option){
    // showing loader for loader start
    var popup_message = jQuery(".action_message_bulk_deletion").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends

    if($option == 'yes'){
      var image_url = jQuery("#del_original_file_url").val();
      jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
      data: {
        type: "delete_original",
        media_src: image_url
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        jQuery("#deleteOriginal").modal('toggle');
        jQuery("#del_original_file_url").val("");
      },
      error: function (jqXHR, exception) {
        jQuery(".loading_rotating").hide();
        alert("Something went wrong. Please try again.");
        jQuery("#deleteOriginal").modal('toggle');
        jQuery("#del_original_file_url").val("");
      }
    });
    }else{
      jQuery("#deleteOriginal").modal('toggle');
    }
  }
  function RestoreFilteredImages($option){
    // showing loader for loader start
    var popup_message = jQuery(".action_message_restore_image").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends
    jQuery("#mediaRestore").modal('toggle');
    if($option == 'yes'){
      var image_url = jQuery("#restored_file_url").val();
      var image_prefix = jQuery("#restored_file_prefix").val();
      
      jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
      data: {
        type: "restore_original_image",
        action: "single_restore_original_image",
        media_src: image_url,
        media_prefix: image_prefix
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        jQuery("#restored_file_url").val("");
        if(response){
          jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }else{
          jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong. Please try again.</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }        
      },
      error: function (jqXHR, exception) {
        jQuery("#restored_file_url").val("");
        jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong. Please try again.</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
    }
  }
  function scanImages(){
    var fileTypeOpt = jQuery('#fileTypeOpt option:selected').length;
    if(fileTypeOpt>0){
      jQuery('#fileTypeOpt').multiselect('deselectAll', false);
      jQuery('#fileTypeOpt').multiselect('updateButtonText');
    }

    var postTypeOpt = jQuery('#postTypeOpt option:selected').length;
    if(postTypeOpt>0){
      jQuery('#postTypeOpt').multiselect('deselectAll', false);
      jQuery('#postTypeOpt').multiselect('updateButtonText');
    }

    // Progress bar start
    var i = 0;
    var myvar = 33;
    var width = 0;
    jQuery("#myBar").css("background-color","#f24e85");
    jQuery("#progress_counter").text("0%");
    jQuery("#myBar").css("width","0%");
    jQuery(".loading").show();
    jQuery("#myBar").show();
    if (i == 0) {
        i = 1;
        time_var = window.setInterval(function(){
        myvar = parseInt(myvar) +  1950;
        var id = setInterval(frame, myvar);
        jQuery("#progress_counter").empty().text(width+'%');
        console.log(width);
        if(width == 100){           
          clearInterval(id);
          clearInterval(time_var);
        }
      }, 100);
        var elem = document.getElementById("myBar");          
        // var id = setInterval(frame, myvar);
        function frame() {            
        if (width >= 100) {
          // clearInterval(id);
          i = 0;
        } else {
          if (width <= 97 || width == 99) {
            width++;
            elem.style.width = width + "%";
          }
        }
        }
    }
    // Progress bar ends

    // Get all the available extension from database
    var homeurl = '<?php echo $current_dir; ?>';
    jQuery.ajax({
      url: ajaxurl,
      type: "POST",
        data: {
      action: 'wpmc_get_image_extensions',
      type: 'only_images',
      home_url: homeurl
    },
    success: function(response) {
      // console.log(response);
      jQuery('#fileTypeOpts').empty().append(response);
      jQuery('#fileTypeOpts').multiselect({
        nonSelectedText: 'File Type',
        allSelectedText: 'File Type',
        onChange: function(option, checked) {             
        }
      });
      jQuery('#fileTypeOptDemo').siblings(".btn-group").hide();
    },
    error: function (ErrorResponse) {
      // console.log(ErrorResponse);
    }
    });

    // Get all post types in filters    
    jQuery.ajax({
      url: ajaxurl,
      data: {
      action: 'wpmc_get_cpt',
      },
      success: function(response) {
        jQuery('#postTypeOpt').empty().append(response);
        jQuery('#postTypeOpt').multiselect({
          nonSelectedText: 'Post Type',
          allSelectedText: 'Post Type',
          onChange: function(option, checked) {
          }
        });
        jQuery('#postTypeOptDemo').siblings(".btn-group").hide();
      },
      error: function (ErrorResponse) {
      }
    });

    var filetypes = jQuery('select[name="fileTypeOpt[]"]').val();
    var linked_status = jQuery('select[name="linkedOpt[]"]').val();
    var daterange = jQuery('input[name="daterange"]').val();
    var posttypefilter = jQuery('select[name="postTypeOpt[]"]').val();
    var daterangearr = daterange.split('-');
    var start_date = daterangearr[0];
    var end_date = daterangearr[1];
    jQuery('#abcd').dataTable().fnDestroy();
    jQuery('#abcd').DataTable({
        'ajax': {
        type: 'POST',
        'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/image-optimize.php',
        data: {
          request_type: "scan_media",
          "filetypes": filetypes,
          "start_date": start_date,
          "end_date": end_date,
          "linked_status": linked_status,
          "posttypefilter": posttypefilter 
        },
        "dataSrc": function (json) {
          jQuery("#media_size").html(json.media_size);
          jQuery("#no_size").hide();
          jQuery("#media_size_compressed").html(json.media_size_opt);
          jQuery("#no_size_compressed").hide();
          jQuery("#total_files").html(json.total_files);
          jQuery("#no_files").hide();
          if(jQuery.isEmptyObject(json.data)){
            jQuery('.dataTables_empty').text('No media found');
          }
          jQuery(".progress-bar").css("width",json.percentage_bar+"%");
          jQuery(".processbar_results").html(json.percentage_bar+"% Optimized");
          return json.data;
        },
        complete: function(){
          // For Progress bar loader
          width = 100;
          jQuery("#myBar").css("background-color","#4DB9AB");
          jQuery("#myBar").css("width","100%");
          setTimeout(function() {
              jQuery('.loading').fadeOut('fast');
          }, 1000);
          // For Progress bar loader ends

          if(jQuery("#datePicker").val() != 'Date Picker'){
            var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
            var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
            dependFilter(startDate,endDate);
          }else{
            dependFilter();
          }
          setTimeout(function() {
            // First take backup of all scanned media files
            var backup_media = jQuery("#media_scanned_result").html();
            jQuery("#backup_media_result").empty().html(backup_media);
          }, 2000);
        },
      },
      "paging": false,
      "info": false,
      "scrollY": "400px",
        "scrollCollapse": true,
        "language": {
          searchPlaceholder: "Search media",
      }
    });
  }
  function GetCheckboxSelected(){
    jQuery(".loading_rotating").show();
    var checkValues = jQuery('input[name=check_list]:checked').map(function()
        {
            return jQuery(this).val();
        }).get();
      jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/add_to_media.php",
      data: {
        request_type: "add_to_media",
        media_src: checkValues
      },
      cache: false,
      success: function(res){
        jQuery(".loading_rotating").hide();
        var obj = jQuery.parseJSON(res);
        var i = 0;
        jQuery.each(obj, function(key, value ) {
          jQuery("input[type=checkbox][value='"+value+"']").prop("checked", false);
          i++;
        });
        jQuery( ".alert-box" ).addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) added to library</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
      },
      error: function (res) {
        jQuery(".loading_rotating").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
  }
  function DeleteFilteredImages($backup){
    jQuery('#mediaDeleter').modal('toggle');

    // showing loader for loader start
    var popup_message = jQuery(".action_message_bulk_deletion").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends
    if($backup == 'yes'){
      var isbackup = 'yes';
    }else{
      var isbackup = 'no';
    }
    var checkValues = jQuery('input[name=check_list]:checked').map(function(){
            return jQuery(this).val();
        }).get();
    var website_prefix = jQuery('input[name=check_list]:checked').map(function(){
            return jQuery(this).data('prefix');
        }).get();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/delete_media.php",
      data: {
        backup: isbackup,
        media_src: checkValues,
        site_prefix : website_prefix,
        request_type: 'bulk_delete'
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        var obj = jQuery.parseJSON(response);
        var i = 0;
        jQuery.each(obj, function(key, value ) {
          jQuery("input[type=checkbox][value='"+value+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800).remove();
          i++;
        });
        jQuery( ".alert-box" ).addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) deleted</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        jQuery('input:checkbox').removeAttr('checked');
      },
      error: function (jqXHR, exception) {
        jQuery(".loading_rotating").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
  }
  function SingleMediaOptimizer($backup){
    jQuery('#singleMediaOptimizer').modal('toggle');
    
    // showing loader for loader start
    var popup_message = jQuery(".action_message_optimization").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends
    if($backup == 'yes'){
      var isbackup = 'yes';
    }else{
      var isbackup = 'no';
    }
    var media = jQuery("#singleImageUrl").val();
    var prefix = jQuery("#singleImagePrefix").val();
    var imgQuality = jQuery('input[name^=image_quality]').map(function(idx, elem) {
      return jQuery(this).val()+','+jQuery(this).data('format');
    }).get();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/ajax_optimize_media.php",
      data: {
        backup: isbackup,
        media_src: media,
        media_prefix: prefix,
        opt_quality: imgQuality,
        request_type: 'single_image_opt'
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        jQuery("#singleImageUrl").val('');        
        jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Image successfully optimized</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
      },
      error: function (jqXHR, exception) {
        jQuery(".loading_rotating").hide();
        jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
  }
  function SingleMediaDelete(option){
    var backup_img ='';
    if(option == 'yes'){
      backup_img = 'yes';
    }else if(option == 'no'){
      backup_img = 'no';
    }
    jQuery('#singleMediaDelete').modal('toggle');    
    // showing loader for loader start
    var popup_message = jQuery(".action_message_deletion").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends
    var media = jQuery("#singleImageSrc").val();
    var prefix = jQuery("#singleDeleteImagePrefix").val();
    
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/delete_media.php",
      dataType: "json",
      data: {
        media_src: media,
        backup: backup_img,
        website_prefix : prefix,
        request_type: 'single_media_delete'
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        if(response.status == "success"){
          jQuery("#singleImageSrc").val('');
          jQuery("input[type=checkbox][value='"+response.url+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800).remove();
          jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }else{
          jQuery("#singleImageSrc").val('');
          jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }
      },
      error: function (jqXHR, exception) {
        jQuery(".loading_rotating").hide();
        jQuery("#singleImageSrc").val('');
        jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
  }
  function BulkOptimizeImages($backup){
    // showing loader for loader start
    var popup_message = jQuery(".action_message_bulk_optimization").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends

    jQuery('#bulkMediaOptimizer').modal('toggle');    
    if($backup == 'yes'){
      var isbackup = 'yes';
    }else{
      var isbackup = 'no';
    }
    // var imgQuality = jQuery("#optimizeQuality").val();
    var imgQuality = jQuery('input[name^=image_quality]').map(function(idx, elem) {
      return jQuery(this).val()+','+jQuery(this).data('format');
    }).get();

    var checkValues = jQuery('input[name=check_list]:checked').map(function(){
        return jQuery(this).val();
    }).get();

    var prefix = jQuery('input[name=check_list]:checked').map(function(){
        return jQuery(this).data('prefix');
    }).get();

    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/ajax_optimize_media.php",
      data: {
        request_type: "bulk_optimization",
        backup: isbackup,
        image_src: checkValues,
        prefixes: prefix,
        opt_quality: imgQuality
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Images successfully optimized</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      },
      error: function (jqXHR, exception) {
        jQuery(".loading_rotating").hide();
        jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
  }
  function ConfirmRestoreDelImgs(){
    var image_url = jQuery("#deleted_url").val();
    var blogid = jQuery("#deleted_blog_id").val();
    jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
      data: {
        type: "restore_deleted_image",
        media_src: image_url,
        blog_id: blogid
      },
      cache: false,
      success: function(response){
        if(response.status == "success"){          
          jQuery('#ConfirmRestoreDeletedImg').modal('toggle');
          jQuery("#deleted_blog_id").val("");
          jQuery("#deleted_url").val("");
          // jQuery("input[type=checkbox][value='"+response.url+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800);
          jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }else{
          jQuery('#ConfirmRestoreDeletedImg').modal('toggle');
          jQuery("#deleted_blog_id").val("");
          jQuery("#deleted_url").val("");
          jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }
      },
      error: function (jqXHR, exception) {
        alert("Something went wrong. Please try again.");
        jQuery("#deleted_blog_id").val("");
        jQuery("#deleted_url").val("");
      }
    });
  }
  function ConfirmRestoreOriginalImages(){
    jQuery('#bulk_restore_original_images').modal('toggle');
    // showing loader for loader start
    var popup_message = jQuery(".action_message_bulk_restore_image").html();
    jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
    // showing loader for loader ends

    var image_url = jQuery('input[name=check_list]:checked').map(function(){
      return jQuery(this).val();
    }).get();

    var prefix = jQuery('input[name=check_list]:checked').map(function(){
      return jQuery(this).data('prefix');
    }).get();

    jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
      data: {
        type: "restore_original_image",
        action: "bulk_restore_original_image",
        media_src: image_url,
        media_prefix: prefix
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        if(response.status == "success"){
          jQuery("#deleted_blog_id").val("");
          jQuery("#deleted_url").val("");
          // jQuery("input[type=checkbox][value='"+response.url+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800);
          jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }else{
          jQuery("#deleted_blog_id").val("");
          jQuery("#deleted_url").val("");
          jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response.message+'</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }
      },
      error: function (jqXHR, exception) {
        jQuery(".loading_rotating").hide();
        jQuery('#bulk_restore_original_images').modal('toggle');
        alert("Something went wrong. Please try again.");
        jQuery("#deleted_blog_id").val("");
        jQuery("#deleted_url").val("");
      }
    });
  }
  function excludeMedia(){
    // check if checked row has Excluded files
    var yes, no, action;
    var if_excluded = [];
    jQuery.each(jQuery("input[name='check_list']:checked"), function(){
            var data = jQuery(this).parent().parent().attr('class');
      if(data.includes("yes_excluded")){
        yes = "yes";
      }else{
        no = "no";
      }
      if(yes && no){
        if_excluded = "both";
      }else{
        if(yes){
          if_excluded = "yes";
        }else{
          if_excluded = "no";
        }
      }
    });
    // showing loader for deletion start
    if(if_excluded == "both"){
      action = ".action_message_exclusion_both";
    }
    if(if_excluded == "yes"){
      action = ".action_message_remove_exclusion";
    }
    if(if_excluded == "no"){
      action = ".action_message_exclusion";
    }
    var deletion_message = jQuery(action).html();
    jQuery(".loading_rotating .action_message").empty().html(deletion_message).parent().parent().show();
    // showing loader for deletion ends

    var media_url = jQuery('input[name=check_list]:checked').map(function(){
      return jQuery(this).val();
    }).get();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
      // dataType: "json",
      data: {
        type: "excluded_media",
        action: "bulk_media_exclude",
        media_src: media_url
      },
      cache: false,
      success: function(response){        
        var obj = jQuery.parseJSON(response);
        jQuery.each(obj, function(key, value ) {
          if(value.actions == "excluded"){
            setTimeout(function() {
              jQuery("input[type=checkbox][value='"+value.url+"']").attr('name', 'check_list');
              jQuery("input[type=checkbox][value='"+value.url+"']").attr('data-excluded','Excluded');
              jQuery("input[type=checkbox][value='"+value.url+"']").parent().parent().addClass("yes_excluded");
            }, 1000);
          }else{
            setTimeout(function() {
              jQuery("input[type=checkbox][value='"+value.url+"']").attr('name', 'check_list');
              jQuery("input[type=checkbox][value='"+value.url+"']").attr('data-excluded','Exclude');
              jQuery("input[type=checkbox][value='"+value.url+"']").parent().parent().removeClass("yes_excluded");
            }, 1000);
          }
        });
        jQuery('.loading_rotating').hide();
        jQuery("input[name=check_list]").prop('checked', false);
        jQuery(".show_excluded_media").show();
        jQuery(".exclude_bulk_media").hide();
      },
      error: function (jqXHR, exception) {
        jQuery('.loading_rotating').hide();
        jQuery("input[name=check_list]").prop('checked', false);
        alert("Something went wrong. Please try again.");
        jQuery(".show_excluded_media").show();
        jQuery(".exclude_bulk_media").hide();
      }
    });
  }
  jQuery( document ).ajaxSuccess(function(event, xhr, options) {
    try {
        var parse = jQuery.parseJSON(xhr.responseText);
        // Add red backkground color to all excluded tr
      jQuery('[data-excluded="Excluded"]').parents("tr").addClass("yes_excluded");
    } catch(e) {
        rangeSlider();
    }
  });
  jQuery(document).ready(function(){
    // Setting: Options dropdown
    jQuery('#wp_options').multiselect({
        nonSelectedText: 'Options',
        allSelectedText: 'Options',
    });
    jQuery("#wp_options").change(function(){
      var keepOriginal = jQuery(this).val();
      jQuery.ajax({
          url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
          type: "POST",
            data: {
          type: 'update_original_image',
          keep_original: keepOriginal

        },
        success: function(response) {
          
            },
            error: function (ErrorResponse) {
                // console.log(ErrorResponse);
            }
        });
    })
    // Get extension with compression quality for slider
    jQuery("#img_compression").click(function(){
      jQuery.ajax({
          url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
          type: "POST",
            data: {
          type: 'get_extensions_quality'
        },
        success: function(response) {
          jQuery(".range_compression").empty().append(response);
            },
            error: function (ErrorResponse) {
                // console.log(ErrorResponse);
            }
        });
    })
  });
  jQuery('#linkedOpt').on('change',function(){
    var linked_status = jQuery(this).val().toString();
    if(linked_status == "no"){
      jQuery('#abcd tbody tr').each(function(){
        // console.log('tr');
        // console.log(jQuery('td:eq(4)', this));
      });
      // console.log('if condition');
    }
    // console.log(linked_status);
  });

  /* media scan filter start */
  jQuery(document).ready(function(){
    jQuery(document).on("change","#fileTypeOpts",function() {
      if(jQuery("#datePicker").val() != 'Date Picker'){
        var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
        dependFilter(startDate,endDate);
      }else{
        dependFilter();
      }
    });
    jQuery(document).on("change","#linkedOpt",function() {
      if(jQuery("#datePicker").val() != 'Date Picker'){
        var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
        dependFilter(startDate,endDate);
      }else{
        dependFilter();
      }
    });
    jQuery(document).on("change","#postTypeOpt",function() {
      if(jQuery("#datePicker").val() != 'Date Picker'){
        var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
        dependFilter(startDate,endDate);
      }else{
        dependFilter();
      }
    });
  });
  jQuery(document).on("click",".daterangepicker .applyBtn",function() {
    var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
    dependFilter(startDate,endDate);
  });
  jQuery(document).on("click",".daterangepicker .cancelBtn",function() {
    if(jQuery("#datePicker").val() != 'Date Picker'){
      var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
      var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
      dependFilter(startDate,endDate);
    }else{
      dependFilter();
    }
  });
  function dependFilter(startDate,endDate){
    var filterValue, filterValueArr, matchArray, found, found1, found2, found3, fileTypeOpt, linkedOptArr, postTypeOptArr, startDateArr, endDateArr, fDate,lDate,cDate;
    var fileTypeOpt = jQuery("#fileTypeOpts").val();
    var linkedOpt = jQuery("#linkedOpt").val();
    var postTypeOpt = jQuery("#postTypeOpt").val();
    var startDate = startDate;
    var endDate = endDate;
    var mainFilter = [];
    if(fileTypeOpt != null){
      fileTypeOpt = jQuery.map(fileTypeOpt, function(n,i){return n.toLowerCase();});
      mainFilter['fileTypeOpt'] = fileTypeOpt;
    }
    if(linkedOpt != null){
      linkedOpt = jQuery.map(linkedOpt, function(n,i){return n.toLowerCase();});
      mainFilter['linkedOpt'] = linkedOpt;
    }
    if(postTypeOpt != null){
      postTypeOpt = jQuery.map(postTypeOpt, function(n,i){return n.toLowerCase();});
      mainFilter['postTypeOpt'] = postTypeOpt;
    }
    if(startDate != null && endDate != null){
      mainFilter['startDate'] = startDate;
      mainFilter['endDate'] = endDate;
    }
    fileTypeOptArr = mainFilter.fileTypeOpt;
    linkedOptArr = mainFilter.linkedOpt;
    postTypeOptArr = mainFilter.postTypeOpt;
    startDateArr = mainFilter.startDate;
    endDateArr = mainFilter.endDate;
    if(fileTypeOptArr == null){
      fileTypeOptArr = [];
    }
    if(linkedOptArr == null){
      linkedOptArr = [];
    }
    if(postTypeOptArr == null){
      postTypeOptArr = [];
    }
    if(startDateArr == null && endDateArr == null){
      startDateArr = [];
      endDateArr = [];
    }
    var tt = 0;
    jQuery("#media_scanned_result tr td").find("#att-filter").each(function(){
      var getDiv = jQuery(this).parent().parent();
      if(fileTypeOptArr.length != 0 || linkedOptArr.length != 0 || postTypeOptArr.length != 0 || startDateArr.length != 0 || endDateArr.length != 0){
        filterValue = jQuery(this).val();
        filterValueArr = filterValue.split(',');
        filterValueArr = jQuery.map(filterValueArr, function(n,i){return n.toLowerCase();});
        if(fileTypeOptArr.length != 0){
          found = fileTypeOptArr.some(r=> filterValueArr.includes(r));
        }else{
          found = true;
        }
        if(linkedOptArr.length != 0){
          found1 = linkedOptArr.some(r=> filterValueArr.includes(r));
        }else{
          found1 = true;
        }
        if(postTypeOptArr.length != 0){
          found2 = postTypeOptArr.some(r=> filterValueArr.includes(r));
        }else{
          found2 = true;
        }
        if(startDateArr.length != 0 || endDateArr.length != 0){
          postDate = filterValueArr[1];
          fDate = Date.parse(startDateArr);
            lDate = Date.parse(endDateArr);
            cDate = Date.parse(postDate);
            if((cDate <= lDate && cDate >= fDate)) {
                found3 = true;
            }else{
              found3 = false;
            }
        }else{
          found3 = true;
        }
        if(found && found1 && found2 && found3){
          tt++;
          getDiv.show();
          getDiv.find('.sorting_1 input').attr('class', 'chkbox');
          getDiv.find('.sorting_1 input').attr('name', 'check_list');
        }else{
          getDiv.hide();
          getDiv.find('.sorting_1 input').attr('class', 'chkbox1');
          getDiv.find('.sorting_1 input').attr('name', 'check_list1');
        }
      }else{
        tt++;
        getDiv.show();
        getDiv.find('.sorting_1 input').attr('class', 'chkbox');
        getDiv.find('.sorting_1 input').attr('name', 'check_list');
      }
    });
    if(tt == 0){
      jQuery("#media_scanned_result .no_media_text").remove();
      var no_media_found = '<tr class="no_media_text"><td data-th="&nbsp;">&nbsp;</td><td data-th="Images Files ">&nbsp;</td><td data-th="URL">&nbsp;</td><td data-th="Upload Date">&nbsp;</td><td data-th="Original size and compression">&nbsp;</td></tr><tr class="no_media_text"> <td data-th="&nbsp;">&nbsp;</td><td data-th="Images Files ">&nbsp;</td><td style="text-align: right;" align="center" data-th="URL"><strong class="red"> <strong>Ready to scan</strong> !</strong></td><td data-th="Upload Date">&nbsp;</td><td data-th="Original size and compression">&nbsp;</td></tr><tr class="no_media_text"> <td data-th="&nbsp;">&nbsp;</td><td data-th="Images Files ">&nbsp;</td><td data-th="URL">&nbsp;</td><td data-th="Upload Date">&nbsp;</td><td data-th="Original size and compression">&nbsp;</td></tr>';
      jQuery("#media_scanned_result").append(no_media_found);
    }else{
      jQuery("#media_scanned_result .no_media_text").remove();
    }
  }

  /* media scan filter end */
</script>
<?php
}else{
  if($media_scan != "yes" && $lisence_activation == 1){
    ?>
    <div class="WPMC_media_scan">
      <div class="disable_section">
        <h3><?php _e("Image optimizer page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_cleaner"); ?></h3>
        <a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Go to settings","wp_media_cleaner"); ?></a>
      </div>
    </div>
    <?php
  }elseif($media_scan == "yes" && $lisence_activation == 0){
    $admin_url = get_admin_url();
    ?>
    <div class="WPMC_media_scan">
      <div class="disable_section">
        <h3><?php _e("Please activate your license to access image optimizer page","wp_media_cleaner"); ?></h3>
        <a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Activate Now","wp_media_cleaner"); ?></a>
      </div>
    </div>
    <?php
  }
  
}
?>