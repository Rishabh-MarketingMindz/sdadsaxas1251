<link rel="stylesheet" type="text/css" href="../../assets/css/WPMC-style.css">
<link rel='stylesheet' id='bootstrap-css'  href='../../assets/css/bootstrap.min.css' type='text/css' media='all' />
<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
global $wpdb;
// get main site prefix
$main_prefix = $wpdb->get_blog_prefix(1);
// Get Media id by url
function getImgidbyURL($imgUrl){
  global $wpdb;
  $page_sites = $_POST['page_site'];
  $imgUrlquery = $wpdb->get_results("SELECT ID FROM ".$page_sites."posts WHERE guid='$imgUrl'");
  if(!empty($imgUrlquery)){
    return $imgUrlquery[0]->ID;
  }else{
    return false;
  }
}

if(isset($_POST['submit'])){
  // When data updated (saved)
  $page_type = $_POST['page_type'];
  $page_from = $_POST['page_from'];
  $count_media = $_POST['count_media'];

  $main_prefix = $wpdb->get_blog_prefix(1);
  $mediaurl = $_POST['media_url'];
  $mediasource = $_POST['media_source'];  
  $title = $_POST['filename'];
  $alternative = $_POST['alt_text'];
  $image_caption = $_POST['caption'];
  $description = $_POST['img_description'];
  $post_id = $_POST['post_id'];
  $website_prefix = $_POST['site_prefix'];

  $count=0;
  foreach ($mediaurl as $media_url) {
    $arr[$count]['media_url'] = $media_url;
    $count++;
  }

  $counter=0;
  foreach ($title as $filename) {
    $arr[$counter]['filename'] = $filename;
    $counter++;
  }

  $counter2=0;
  foreach ($alternative as $alt_text) {
    $arr[$counter2]['alt_text'] = $alt_text;
    $counter2++;
  }

  $counter3=0;
  foreach ($image_caption as $caption) {
    $arr[$counter3]['caption'] = $caption;
    $counter3++;
  }

  $counter4=0;
  foreach ($description as $img_description) {
    $arr[$counter4]['img_description'] = $img_description;
    $counter4++;
  }

  $counter5=0;
  foreach ($post_id as $postid) {
    $arr[$counter5]['postid'] = $postid;
    $counter5++;
  }

  $counter6=0;
  foreach ($mediasource as $media_source) {
    $arr[$counter6]['media_source'] = $media_source;
    $counter6++;
  }
  // echo "<pre>"; print_r($arr);
  // echo "<br>";
  $resultdata = '
  <style type="text/css">
    table.dataTable thead tr th {
      border: none;
    }
    table.dataTable {
      border: none;
    }
    .dataTables_wrapper.no-footer .dataTables_scrollBody {
      border-bottom: 1px solid #ddd;
      border-top: 1px solid #ddd;
    }
    .btn-blue, .multiselect.dropdown-toggle.btn.btn-default {
      background-color: #4DB9AB;
      color: #fff;
    }
    .btn-red {
      background-color: #E53373;
      color: #fff;
    }
    /* Absolute Center Spinner */
    .loading {
      position: fixed;
      z-index: 999;
      height: 2em;
      width: 2em;
      overflow: visible;
      margin: auto;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
    }
    /* Transparent Overlay */
    .loading:before {
      content: "";
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(18, 199, 132, 0.88);
    }
    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
      /* hide "loading..." text */
      font: 0/0 a;
      color: #fff;
      text-shadow: none;
      background-color: rgba(18, 199, 132, 0.88);
      border: 0;
    }
    .loading:not(:required):after {
      content: "";
      display: block;
      font-size: 10px;
      width: 1em;
      height: 1em;
      margin-top: -0.5em;
      -webkit-animation: spinner 1500ms infinite linear;
      -moz-animation: spinner 1500ms infinite linear;
      -ms-animation: spinner 1500ms infinite linear;
      -o-animation: spinner 1500ms infinite linear;
      animation: spinner 1500ms infinite linear;
      border-radius: 0.5em;
      -webkit-box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
      box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, #E53373 -1.5em 0 0 0, #E53373 -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
    }

      /* Animation */
      @-webkit-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @-moz-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @-o-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
    .alert-box {
      display: none;
      position: fixed;
      width: 17%;
      height: 60px;
      z-index: 99999999999;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      padding: 15px;
      margin-bottom: 20px;
      border: 1px solid transparent;
      border-radius: 4px;
    }
    .success {
      color: #3c763d;
      background-color: #beffa4;
      border-color: #73d026;
    }
    .unsuccess {
      color: #4a4a4a;
      background-color: #ffa4a4;
      border-color: #d02626;
    }
    /*small loader*/
    .loader {
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 60px;
      height: 60px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
      margin: 0 auto;
    }
      /* Safari */
      @-webkit-keyframes spin {
     0% {
     -webkit-transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
    }
    }
     @keyframes spin {
     0% {
     transform: rotate(0deg);
    }
     100% {
     transform: rotate(360deg);
    }
    }
    /* Tooltip container */
    .tooltipstext{
      display: none;
    }
    .tooltip_inline{
      display: inline;
      text-align: center;
      margin: 0 auto;
    }
    .tooltip_inline_child{
      display: inline-block;
      padding: 10px 4px;
      text-align: center;
      margin: 0 auto;
    }
    .tooltip_content{
      max-height: 180px; 
      overflow-y: scroll;
    }
    p.tooltip_title {
      font-size: 20px;
    }
    .tooltip_button a{
      color: #fff;
    }
  </style>
  <div class="modal-content">
    <form type="form" method="post" id="updateMediaData">
      <div class="modal-body media_results">
        <input type="hidden" name="site_prefix" value="'.$website_prefix.'" />
        <input type="hidden" name="page_type" value="'.$page_type.'" />
        <input type="hidden" name="page_from" value="'.$page_from.'" />
        <input type="hidden" name="count_media" value="'.$count_media.'" />
        <div class="container">
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4 media_overlay_header">
              <label>Content type: </label><span class="grey"> '.$page_type.'</span> 
              <span> | </span>
              <label>Page name: </label><span class="grey"> '.$page_from.'</span>
              <h4 class="red">'.$count_media.' Media file(s) found</h4>
            </div>
          </div>
          <div class=" media_content_div">
            ';
          foreach ($arr as $media_value) {
            $media_urls = $media_value['media_url'];
            $postid = $media_value['postid'];
            $title = $media_value['filename'];
            $alt_text = $media_value['alt_text'];
            $caption = $media_value['caption'];
            $img_description = $media_value['img_description'];
            $media_source = $media_value['media_source'];            
            if($postid != '-na-'){
              $my_post = array(
                'ID'           => $postid,
                'post_title'   => $title,
                'post_content' => $img_description,
                'post_excerpt' => $caption,
              );
              // Update the post into the database
              if($website_prefix == $main_prefix){
                $result = wp_update_post( $my_post );
              }else{
                $table_name = $website_prefix.'posts';
                $result = $wpdb->query($wpdb->prepare('UPDATE `'.$table_name.'` SET `post_content`= "'.$img_description.'", `post_excerpt`="'.$caption.'", `post_title`="'.$title.'" WHERE ID ="'.$postid.'"'));
                $result = 1;
              }
              $res='';
              if($alt_text){
                if($website_prefix == $main_prefix){
                  $old_alt = get_post_meta($postid,'_wp_attachment_image_alt',true);
                }else{
                  $table_name = $website_prefix.'postmeta';
                  $alternative_text = $wpdb->get_results('SELECT meta_value from '.$table_name.' WHERE meta_key = "_wp_attachment_image_alt" AND post_id = "'.$postid.'"');
                  $old_alt = $alternative_text[0]->meta_value;
                }   
                if($old_alt == $alt_text){
                  $result2 = 'same';
                }else{
                  if($website_prefix == $main_prefix){
                    $result2 = update_post_meta( $postid, '_wp_attachment_image_alt', $alt_text);
                  }else{
                    $table_name = $website_prefix.'postmeta';
                    if($old_alt){
                      $result2 = $wpdb->query($wpdb->prepare('UPDATE '.$table_name.' SET meta_value = "'.$alt_text.'" WHERE post_id ="'.$postid.'" AND meta_key ="_wp_attachment_image_alt"'));
                      $result2 = 1;
                    }else{
                      $result2 = $wpdb->query($wpdb->prepare('INSERT INTO '.$table_name.' (meta_key,meta_value,post_id)VALUES("_wp_attachment_image_alt", "'.$alt_text.'", "'.$postid.'")'));
                      $result2 = 1;
                    }
                    
                  }
                }
              }else{
                $result2 = 'empty';
              }
            }
            $post_id = $postid;
            $post_title = $title;
            $post_caption = $caption;
            $post_description = $img_description;
            $alttext = $alt_text;
            $post_image = $media_urls;

            if($post_id != '-na-'){
              // Get media type start
              $media_type = get_headers($post_image);
              foreach ($media_type as $valued) { 
                if (strpos($valued, 'Content-Type') !== false) {
                  $m_type = $valued;
                }
              }

              $variable = substr($m_type, 0, strpos($m_type, "/"));
              $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
              // Get media type end
              $resultdata .='
              <input type="hidden" name="media_url[]" value="'.$post_image.'">
              <div class="row media_row">
                <div class="col-sm-3 col-md-3">';
                  if($mediatype == 'image'){
                    $resultdata .='
                    <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
                  }elseif($mediatype == 'video'){
                    $resultdata .='
                    <div>
                      <video style="width: 100%;" controls>
                        <source src="'.$post_image.'" type="video/mp4">
                        Sorry, your browser doesnt support the video element.
                      </video>
                    </div>';
                  }elseif($mediatype == "application"){
                    $ext = pathinfo($post_image, PATHINFO_EXTENSION);
                    if($ext == 'pdf'){
                      $resultdata .= '<embed src="'.$post_image.'" type="application/pdf">';
                    }elseif($ext == 'docx' || $ext == 'doc'){
                      $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }else{
                      $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }
                  }elseif($mediatype == "audio"){
                    $resultdata .='
                    <audio controls="">
                      <source src="'.$post_image.'" type="audio/ogg">
                      <source src="'.$post_image.'" type="audio/mpeg">
                      Your browser does not support the audio element.
                    </audio>';
                  }else{
                    $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                  }
                  $resultdata .='
                </div>
                <input type="hidden" name="post_id[]" value="'.$post_id.'" />      
                <div class="col-sm-4 col-md-4">
                  <div class="row">
                    <div class="col-md-12 edit_media_col_12">
                        <label class="media_label">Filename: </label><input class="edit_media_input" type="text" name="filename[]" placeholder="filename" value="'.$post_title.'">
                    </div>
                    <div class="col-md-12 edit_media_col_12">
                        <label class="media_label">Alt text: </label><input class="edit_media_input" type="text" name="alt_text[]" placeholder="Alternative Text" value="'.$alttext.'">
                      </div>
                      <div class="col-md-12 edit_media_col_12">
                        <label class="media_label">Caption: </label><input class="edit_media_input" type="text" name="caption[]" placeholder="Caption" value="'.$post_caption.'">
                      </div>
                      <div class="col-md-12 edit_media_col_12"> 
                        <label class="media_label">Image Type: </label><input class="edit_media_input" type="text" name="img_type[]" value="'.$media_source.'" readonly>
                      </div>
                    </div>
                    </div>
                    <div class="col-sm-5 col-md-5">
                      <label class="media_label">Description: </label>
                      <textarea name="img_description[]" placeholder="Description">'.$post_description.'</textarea>
                    </div>
                  </div>
              ';
            }else{                
              if($post_image){
                // Get media type start
                $media_type = get_headers($post_image);
                foreach ($media_type as $valued) { 
                  if (strpos($valued, 'Content-Type') !== false) {
                    $m_type = $valued;
                  }
                }
                $variable = substr($m_type, 0, strpos($m_type, "/"));
                $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
                // Get media type end
                
                $resultdata .= '
                <div class="row media_row">
                  <div class="col-sm-3 col-md-3">';
                  if($mediatype == 'image'){
                    $resultdata .='
                    <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
                  }elseif($mediatype == "audio"){
                    $resultdata .='
                    <audio controls="">
                      <source src="'.$post_image.'" type="audio/ogg">
                      <source src="'.$post_image.'" type="audio/mpeg">
                      Your browser does not support the audio element.
                    </audio>';
                  }elseif($mediatype == 'video'){
                    $resultdata .='
                    <div>
                      <video style="width: 100%;" controls>
                        <source src="'.$post_image.'" type="video/mp4">
                        Sorry, your browser doesnt support the video element.
                      </video>
                    </div>';
                  }elseif($mediatype == "application"){
                    $ext = pathinfo($post_image, PATHINFO_EXTENSION);
                    if($ext == 'pdf'){
                      $resultdata .= '<embed src="'.$post_image.'" type="application/pdf">';
                    }elseif($ext == 'docx' || $ext == 'doc'){
                      $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }
                  }
                  $resultdata .='
                  </div>
                  <div class="col-sm-9 col-md-9 text-center">
                    <strong class="red">This Media Content is Not Editable, Because It is a Resize Image.</strong>
                  </div>
                </div>
                <input type="hidden" name="media_url[]" value="'.$post_image.'">
                <input type="hidden" name="post_id[]" value="-na-">
                <input type="hidden" name="filename[]" value="-na-">
                <input type="hidden" name="alt_text[]" value="-na-">
                <input type="hidden" name="caption[]" value="-na-">
                <input type="hidden" name="img_type[]" value="-na-">
                <input type="hidden" name="img_description[]" value="-na-">
                ';
              }
            }
          }
  $resultdata .='
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="container">
          <input type="submit" name="submit" class="btn btn-blue" value="Save">
          <input type="submit" name="submit2" class="btn btn-blue" value="Save and Exit">
          <button type="button" class="btn btn-red" onclick="window.close();">Cancel</button>
        </div>
      </div>
    </form>
  </div>';
  echo $resultdata;
}elseif(isset($_POST['submit2'])){
  // When data updated (saved)
  $page_type = $_POST['page_type'];
  $page_from = $_POST['page_from'];
  $count_media = $_POST['count_media'];

  $main_prefix = $wpdb->get_blog_prefix(1);
  $mediaurl = $_POST['media_url'];
  $mediasource = $_POST['media_source'];  
  $title = $_POST['filename'];
  $alternative = $_POST['alt_text'];
  $image_caption = $_POST['caption'];
  $description = $_POST['img_description'];
  $post_id = $_POST['post_id'];
  $website_prefix = $_POST['site_prefix'];

  $count=0;
  foreach ($mediaurl as $media_url) {
    $arr[$count]['media_url'] = $media_url;
    $count++;
  }

  $counter=0;
  foreach ($title as $filename) {
    $arr[$counter]['filename'] = $filename;
    $counter++;
  }

  $counter2=0;
  foreach ($alternative as $alt_text) {
    $arr[$counter2]['alt_text'] = $alt_text;
    $counter2++;
  }

  $counter3=0;
  foreach ($image_caption as $caption) {
    $arr[$counter3]['caption'] = $caption;
    $counter3++;
  }

  $counter4=0;
  foreach ($description as $img_description) {
    $arr[$counter4]['img_description'] = $img_description;
    $counter4++;
  }

  $counter5=0;
  foreach ($post_id as $postid) {
    $arr[$counter5]['postid'] = $postid;
    $counter5++;
  }

  $counter6=0;
  foreach ($mediasource as $media_source) {
    $arr[$counter6]['media_source'] = $media_source;
    $counter6++;
  }
  // echo "<pre>"; print_r($arr);
  // echo "<br>";
  $resultdata = '
  <style type="text/css">
    table.dataTable thead tr th {
      border: none;
    }
    table.dataTable {
      border: none;
    }
    .dataTables_wrapper.no-footer .dataTables_scrollBody {
      border-bottom: 1px solid #ddd;
      border-top: 1px solid #ddd;
    }
    .btn-blue, .multiselect.dropdown-toggle.btn.btn-default {
      background-color: #4DB9AB;
      color: #fff;
    }
    .btn-red {
      background-color: #E53373;
      color: #fff;
    }
    /* Absolute Center Spinner */
    .loading {
      position: fixed;
      z-index: 999;
      height: 2em;
      width: 2em;
      overflow: visible;
      margin: auto;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
    }
    /* Transparent Overlay */
    .loading:before {
      content: "";
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(18, 199, 132, 0.88);
    }
    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
      /* hide "loading..." text */
      font: 0/0 a;
      color: #fff;
      text-shadow: none;
      background-color: rgba(18, 199, 132, 0.88);
      border: 0;
    }
    .loading:not(:required):after {
      content: "";
      display: block;
      font-size: 10px;
      width: 1em;
      height: 1em;
      margin-top: -0.5em;
      -webkit-animation: spinner 1500ms infinite linear;
      -moz-animation: spinner 1500ms infinite linear;
      -ms-animation: spinner 1500ms infinite linear;
      -o-animation: spinner 1500ms infinite linear;
      animation: spinner 1500ms infinite linear;
      border-radius: 0.5em;
      -webkit-box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
      box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, #E53373 -1.5em 0 0 0, #E53373 -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
    }

      /* Animation */
      @-webkit-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @-moz-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @-o-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
    .alert-box {
      display: none;
      position: fixed;
      width: 17%;
      height: 60px;
      z-index: 99999999999;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      padding: 15px;
      margin-bottom: 20px;
      border: 1px solid transparent;
      border-radius: 4px;
    }
    .success {
      color: #3c763d;
      background-color: #beffa4;
      border-color: #73d026;
    }
    .unsuccess {
      color: #4a4a4a;
      background-color: #ffa4a4;
      border-color: #d02626;
    }
    /*small loader*/
    .loader {
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 60px;
      height: 60px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
      margin: 0 auto;
    }
      /* Safari */
      @-webkit-keyframes spin {
     0% {
     -webkit-transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
    }
    }
     @keyframes spin {
     0% {
     transform: rotate(0deg);
    }
     100% {
     transform: rotate(360deg);
    }
    }
    /* Tooltip container */
    .tooltipstext{
      display: none;
    }
    .tooltip_inline{
      display: inline;
      text-align: center;
      margin: 0 auto;
    }
    .tooltip_inline_child{
      display: inline-block;
      padding: 10px 4px;
      text-align: center;
      margin: 0 auto;
    }
    .tooltip_content{
      max-height: 180px; 
      overflow-y: scroll;
    }
    p.tooltip_title {
      font-size: 20px;
    }
    .tooltip_button a{
      color: #fff;
    }
  </style>
  <div class="modal-content">
    <form type="form" method="post" id="updateMediaData">
      <div class="modal-body media_results">
        <input type="hidden" name="site_prefix" value="'.$website_prefix.'" />
        <input type="hidden" name="page_type" value="'.$page_type.'" />
        <input type="hidden" name="page_from" value="'.$page_from.'" />
        <input type="hidden" name="count_media" value="'.$count_media.'" />
        <div class="container">
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4 media_overlay_header">
              <label>Content type: </label><span class="grey"> '.$page_type.'</span> 
              <span> | </span>
              <label>Page name: </label><span class="grey"> '.$page_from.'</span>
              <h4 class="red">'.$count_media.' Media file(s) found</h4>
            </div>
          </div>
          <div class=" media_content_div">
            ';
          $doesvoucherexists = false;
          foreach ($arr as $media_value) {
            $media_urls = $media_value['media_url'];
            $postid = $media_value['postid'];
            $title = $media_value['filename'];
            $alt_text = $media_value['alt_text'];
            $caption = $media_value['caption'];
            $img_description = $media_value['img_description'];
            $media_source = $media_value['media_source'];            
            if($postid != '-na-'){
              $my_post = array(
                'ID'           => $postid,
                'post_title'   => $title,
                'post_content' => $img_description,
                'post_excerpt' => $caption,
              );
              // Update the post into the database
              if($website_prefix == $main_prefix){
                $result = wp_update_post( $my_post );
              }else{
                $table_name = $website_prefix.'posts';
                $result = $wpdb->query($wpdb->prepare('UPDATE `'.$table_name.'` SET `post_content`= "'.$img_description.'", `post_excerpt`="'.$caption.'", `post_title`="'.$title.'" WHERE ID ="'.$postid.'"'));
                $result = 1;
              }
              $res='';
              if($alt_text){
                if($website_prefix == $main_prefix){
                  $old_alt = get_post_meta($postid,'_wp_attachment_image_alt',true);
                }else{
                  $table_name = $website_prefix.'postmeta';
                  $alternative_text = $wpdb->get_results('SELECT meta_value from '.$table_name.' WHERE meta_key = "_wp_attachment_image_alt" AND post_id = "'.$postid.'"');
                  $old_alt = $alternative_text[0]->meta_value;
                }   
                if($old_alt == $alt_text){
                  $result2 = 'same';
                }else{
                  if($website_prefix == $main_prefix){
                    $result2 = update_post_meta( $postid, '_wp_attachment_image_alt', $alt_text);
                  }else{
                    $table_name = $website_prefix.'postmeta';
                    if($old_alt){
                      $result2 = $wpdb->query($wpdb->prepare('UPDATE '.$table_name.' SET meta_value = "'.$alt_text.'" WHERE post_id ="'.$postid.'" AND meta_key ="_wp_attachment_image_alt"'));
                      $result2 = 1;
                    }else{
                      $result2 = $wpdb->query($wpdb->prepare('INSERT INTO '.$table_name.' (meta_key,meta_value,post_id)VALUES("_wp_attachment_image_alt", "'.$alt_text.'", "'.$postid.'")'));
                      $result2 = 1;
                    }
                    
                  }
                }
              }else{
                $result2 = 'empty';
              }
            }
            $post_id = $postid;
            $post_title = $title;
            $post_caption = $caption;
            $post_description = $img_description;
            $alttext = $alt_text;
            $post_image = $media_urls;

            if($post_id != '-na-'){
              // Get media type start
              $media_type = get_headers($post_image);
              foreach ($media_type as $valued) { 
                if (strpos($valued, 'Content-Type') !== false) {
                  $m_type = $valued;
                }
              }

              $variable = substr($m_type, 0, strpos($m_type, "/"));
              $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
              // Get media type end
              $resultdata .='
              <input type="hidden" name="media_url[]" value="'.$post_image.'">
              <div class="row media_row">
                <div class="col-sm-3 col-md-3">';
                  if($mediatype == 'image'){
                    $resultdata .='
                    <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
                  }elseif($mediatype == 'video'){
                    $resultdata .='
                    <div>
                      <video style="width: 100%;" controls>
                        <source src="'.$post_image.'" type="video/mp4">
                        Sorry, your browser doesnt support the video element.
                      </video>
                    </div>';
                  }elseif($mediatype == "application"){
                    $ext = pathinfo($post_image, PATHINFO_EXTENSION);
                    if($ext == 'pdf'){
                      $resultdata .= '<embed src="'.$post_image.'" type="application/pdf">';
                    }elseif($ext == 'docx' || $ext == 'doc'){
                      $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }else{
                      $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }
                  }elseif($mediatype == "audio"){
                    $resultdata .='
                    <audio controls="">
                      <source src="'.$post_image.'" type="audio/ogg">
                      <source src="'.$post_image.'" type="audio/mpeg">
                      Your browser does not support the audio element.
                    </audio>';
                  }else{
                    $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                  }
                  $resultdata .='
                </div>
                <input type="hidden" name="post_id[]" value="'.$post_id.'" />      
                <div class="col-sm-4 col-md-4">
                  <div class="row">
                    <div class="col-md-12 edit_media_col_12">
                        <label class="media_label">Filename: </label><input class="edit_media_input" type="text" name="filename[]" placeholder="filename" value="'.$post_title.'">
                    </div>
                    <div class="col-md-12 edit_media_col_12">
                        <label class="media_label">Alt text: </label><input class="edit_media_input" type="text" name="alt_text[]" placeholder="Alternative Text" value="'.$alttext.'">
                      </div>
                      <div class="col-md-12 edit_media_col_12">
                        <label class="media_label">Caption: </label><input class="edit_media_input" type="text" name="caption[]" placeholder="Caption" value="'.$post_caption.'">
                      </div>
                      <div class="col-md-12 edit_media_col_12"> 
                        <label class="media_label">Image Type: </label><input class="edit_media_input" type="text" name="img_type[]" value="'.$media_source.'" readonly>
                      </div>
                    </div>
                    </div>
                    <div class="col-sm-5 col-md-5">
                      <label class="media_label">Description: </label>
                      <textarea name="img_description[]" placeholder="Description">'.$post_description.'</textarea>
                    </div>
                  </div>
              ';
            }else{                
              if($post_image){
                // Get media type start
                $media_type = get_headers($post_image);
                foreach ($media_type as $valued) { 
                  if (strpos($valued, 'Content-Type') !== false) {
                    $m_type = $valued;
                  }
                }
                $variable = substr($m_type, 0, strpos($m_type, "/"));
                $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
                // Get media type end
                
                $resultdata .= '
                <div class="row media_row">
                  <div class="col-sm-3 col-md-3">';
                  if($mediatype == 'image'){
                    $resultdata .='
                    <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
                  }elseif($mediatype == "audio"){
                    $resultdata .='
                    <audio controls="">
                      <source src="'.$post_image.'" type="audio/ogg">
                      <source src="'.$post_image.'" type="audio/mpeg">
                      Your browser does not support the audio element.
                    </audio>';
                  }elseif($mediatype == 'video'){
                    $resultdata .='
                    <div>
                      <video style="width: 100%;" controls>
                        <source src="'.$post_image.'" type="video/mp4">
                        Sorry, your browser doesnt support the video element.
                      </video>
                    </div>';
                  }elseif($mediatype == "application"){
                    $ext = pathinfo($post_image, PATHINFO_EXTENSION);
                    if($ext == 'pdf'){
                      $resultdata .= '<embed src="'.$post_image.'" type="application/pdf">';
                    }elseif($ext == 'docx' || $ext == 'doc'){
                      $resultdata .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }
                  }
                  $resultdata .='
                  </div>
                  <div class="col-sm-9 col-md-9 text-center">
                    <strong class="red">This Media Content is Not Editable, Because It is a Resize Image.</strong>
                  </div>
                </div>
                <input type="hidden" name="media_url[]" value="'.$post_image.'">
                <input type="hidden" name="post_id[]" value="-na-">
                <input type="hidden" name="filename[]" value="-na-">
                <input type="hidden" name="alt_text[]" value="-na-">
                <input type="hidden" name="caption[]" value="-na-">
                <input type="hidden" name="img_type[]" value="-na-">
                <input type="hidden" name="img_description[]" value="-na-">
                ';
              }
            }
          }
          if ($doesvoucherexists === false) {
            $resultdata .='<script>
              window.close();
            </script>';
          }
  $resultdata .='
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="container">
          <input type="submit" name="submit" class="btn btn-blue" value="Save">
          <input type="submit" name="submit2" class="btn btn-blue" value="Save and Exit">
          <button type="button" class="btn btn-red" onclick="window.close();">Cancel</button>
        </div>
      </div>
    </form>
  </div>';
  echo $resultdata;
}else{
  $delArr = array();
  $page_from = $_POST['page_fr`om'];
  $page_type = $_POST['page_type'];
  $media_src = $_POST['media_src'];
  $page_site = $_POST['page_site'];
  $media_id = explode(',', $media_src);
  $count_media = count($media_id);
  $media_source = $_POST['media_source'];
  $media_sources = explode(',', $media_source);
  $counter = 0;
  foreach ($media_sources as $sources) {
    $arr[$counter]['media_source'] = $sources;
    $counter++;
  }
  $counter2 = 0;
  foreach ($media_id as $media_URLs) {
    $media_ids = getImgidbyURL($media_URLs);

    if($media_ids){
      $arr[$counter2]['media_found'] = 'found';
    }else{
      $arr[$counter2]['media_found'] = $media_URLs;
    }
    $attachmentdata = $wpdb->get_results("SELECT * FROM ".$page_site."posts WHERE ID='$media_ids'");
    $arr[$counter2]['post_id'] = $attachmentdata[0]->ID;
    $arr[$counter2]['post_image'] = $attachmentdata[0]->guid;
    $arr[$counter2]['post_title'] = $attachmentdata[0]->post_title;
    $arr[$counter2]['post_caption'] = $attachmentdata[0]->post_excerpt;
    $arr[$counter2]['post_description'] = $attachmentdata[0]->post_content;
    if($page_site == $main_prefix){
      $alternative_text = get_post_meta($attachmentdata[0]->ID,'_wp_attachment_image_alt',true);
    }else{
      $the_id = $attachmentdata[0]->ID;
      $alternative_text = $wpdb->get_results("SELECT meta_value from ".$page_site."postmeta WHERE meta_key='_wp_attachment_image_alt' AND post_id=$the_id" );
      $alternative_text = $alternative_text[0]->meta_value;
    }
    $arr[$counter2]['alttext'] = $alternative_text;
    $counter2++;
  }
  $data = '
  <style type="text/css">
    table.dataTable thead tr th {
      border: none;
    }
    table.dataTable {
      border: none;
    }
    .dataTables_wrapper.no-footer .dataTables_scrollBody {
      border-bottom: 1px solid #ddd;
      border-top: 1px solid #ddd;
    }
    .btn-blue, .multiselect.dropdown-toggle.btn.btn-default {
      background-color: #4DB9AB;
      color: #fff;
    }
    .btn-red {
      background-color: #E53373;
      color: #fff;
    }
    /* Absolute Center Spinner */
    .loading {
      position: fixed;
      z-index: 999;
      height: 2em;
      width: 2em;
      overflow: visible;
      margin: auto;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
    }
    /* Transparent Overlay */
    .loading:before {
      content: "";
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(18, 199, 132, 0.88);
    }
    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
      /* hide "loading..." text */
      font: 0/0 a;
      color: #fff;
      text-shadow: none;
      background-color: rgba(18, 199, 132, 0.88);
      border: 0;
    }
    .loading:not(:required):after {
      content: "";
      display: block;
      font-size: 10px;
      width: 1em;
      height: 1em;
      margin-top: -0.5em;
      -webkit-animation: spinner 1500ms infinite linear;
      -moz-animation: spinner 1500ms infinite linear;
      -ms-animation: spinner 1500ms infinite linear;
      -o-animation: spinner 1500ms infinite linear;
      animation: spinner 1500ms infinite linear;
      border-radius: 0.5em;
      -webkit-box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
      box-shadow: #E53373 1.5em 0 0 0, #E53373 1.1em 1.1em 0 0, #E53373 0 1.5em 0 0, #E53373 -1.1em 1.1em 0 0, #E53373 -1.5em 0 0 0, #E53373 -1.1em -1.1em 0 0, #E53373 0 -1.5em 0 0, #E53373 1.1em -1.1em 0 0;
    }

      /* Animation */
      @-webkit-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @-moz-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @-o-keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
     @keyframes spinner {
     0% {
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
     -ms-transform: rotate(360deg);
     -o-transform: rotate(360deg);
     transform: rotate(360deg);
    }
    }
    .alert-box {
      display: none;
      position: fixed;
      width: 17%;
      height: 60px;
      z-index: 99999999999;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      padding: 15px;
      margin-bottom: 20px;
      border: 1px solid transparent;
      border-radius: 4px;
    }
    .success {
      color: #3c763d;
      background-color: #beffa4;
      border-color: #73d026;
    }
    .unsuccess {
      color: #4a4a4a;
      background-color: #ffa4a4;
      border-color: #d02626;
    }
    /*small loader*/
    .loader {
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 60px;
      height: 60px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
      margin: 0 auto;
    }
      /* Safari */
      @-webkit-keyframes spin {
     0% {
     -webkit-transform: rotate(0deg);
    }
     100% {
     -webkit-transform: rotate(360deg);
    }
    }
     @keyframes spin {
     0% {
     transform: rotate(0deg);
    }
     100% {
     transform: rotate(360deg);
    }
    }
    /* Tooltip container */
    .tooltipstext{
      display: none;
    }
    .tooltip_inline{
      display: inline;
      text-align: center;
      margin: 0 auto;
    }
    .tooltip_inline_child{
      display: inline-block;
      padding: 10px 4px;
      text-align: center;
      margin: 0 auto;
    }
    .tooltip_content{
      max-height: 180px; 
      overflow-y: scroll;
    }
    p.tooltip_title {
      font-size: 20px;
    }
    .tooltip_button a{
      color: #fff;
    }
  </style>
  <div class="modal-content">
    <form type="form" method="post" id="updateMediaData">
      <div class="modal-body media_results">
        <input type="hidden" name="site_prefix" value="'.$page_site.'" />
        <input type="hidden" name="page_type" value="'.$page_type.'" />
        <input type="hidden" name="page_from" value="'.$page_from.'" />
        <input type="hidden" name="count_media" value="'.$count_media.'" />
        <div class="container">
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4 media_overlay_header">
              <label>Content type: </label><span class="grey"> '.$page_type.'</span> 
              <span> | </span>
              <label>Page name: </label><span class="grey"> '.$page_from.'</span>
              <h4 class="red">'.$count_media.' Media file(s) found</h4>
            </div>
          </div>
          <div class=" media_content_div">
            ';
            foreach ($arr as $media_data) {
              if($media_data['media_found'] == 'found'){
                $post_id = $media_data['post_id'];
                $post_image = $media_data['post_image'];

                // Get media type start
                $media_type = get_headers($post_image);
                foreach ($media_type as $valued) { 
                  if (strpos($valued, 'Content-Type') !== false) {
                    $m_type = $valued;
                  }
                }

                $variable = substr($m_type, 0, strpos($m_type, "/"));
                $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
                // Get media type end
                
                $post_title = $media_data['post_title'];
                $post_caption = $media_data['post_caption'];
                $post_description = $media_data['post_description'];
                $alttext = $media_data['alttext'];
                $media_source = $media_data['media_source'];
                $data .='
                <input type="hidden" name="media_url[]" value="'.$post_image.'">
                <div class="row media_row">
                  <div class="col-sm-3 col-md-3">';
                    if($mediatype == 'image'){
                      $data .='
                      <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
                    }elseif($mediatype == 'video'){
                      $data .='
                      <div>
                        <video style="width: 100%;" controls>
                          <source src="'.$post_image.'" type="video/mp4">
                          Sorry, your browser doesnt support the video element.
                        </video>
                      </div>';
                    }elseif($mediatype == "application"){
                      $ext = pathinfo($post_image, PATHINFO_EXTENSION);
                      if($ext == 'pdf'){
                        $data .= '<embed src="'.$post_image.'" type="application/pdf">';
                      }elseif($ext == 'docx' || $ext == 'doc'){
                        $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                      }else{
                        $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                      }
                    }elseif($mediatype == "audio"){
                      $data .='
                      <audio controls="">
                        <source src="'.$post_image.'" type="audio/ogg">
                        <source src="'.$post_image.'" type="audio/mpeg">
                        Your browser does not support the audio element.
                      </audio>';
                    }else{
                      $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                    }
                    $data .='
                  </div>
                  <input type="hidden" name="post_id[]" value="'.$post_id.'" />      
                  <div class="col-sm-4 col-md-4">
                    <div class="row">
                      <div class="col-md-12 edit_media_col_12">
                          <label class="media_label">Filename: </label><input class="edit_media_input" type="text" name="filename[]" placeholder="filename" value="'.$post_title.'">
                      </div>
                      <div class="col-md-12 edit_media_col_12">
                          <label class="media_label">Alt text: </label><input class="edit_media_input" type="text" name="alt_text[]" placeholder="Alternative Text" value="'.$alttext.'">
                        </div>
                        <div class="col-md-12 edit_media_col_12">
                          <label class="media_label">Caption: </label><input class="edit_media_input" type="text" name="caption[]" placeholder="Caption" value="'.$post_caption.'">
                        </div>
                        <div class="col-md-12 edit_media_col_12"> 
                          <label class="media_label">Image Type: </label><input class="edit_media_input" type="text" name="img_type[]" value="'.$media_source.'" readonly>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-5 col-md-5">
                        <label class="media_label">Description: </label>
                        <textarea name="img_description[]" placeholder="Description">'.$post_description.'</textarea>
                      </div>
                    </div>
                ';
              }else{
                $post_id = $media_data['post_id'];
                $post_title = $media_data['post_title'];
                $post_caption = $media_data['post_caption'];
                $post_description = $media_data['post_description'];
                $alttext = $media_data['alttext'];
                $media_source = $media_data['media_source'];

                $post_image = $media_data['media_found'];
                if($post_image){
                  // Get media type start
                  $media_type = get_headers($post_image);
                  foreach ($media_type as $valued) { 
                    if (strpos($valued, 'Content-Type') !== false) {
                      $m_type = $valued;
                    }
                  }

                  $variable = substr($m_type, 0, strpos($m_type, "/"));
                  $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
                  // Get media type end
                  
                  $data .= '
                  <div class="row media_row">
                    <div class="col-sm-3 col-md-3">';
                    if($mediatype == 'image'){
                      $data .='
                      <div class="img-preview" style="background-image: url('.$post_image.');"></div>';
                    }elseif($mediatype == "audio"){
                      $data .='
                      <audio controls="">
                        <source src="'.$post_image.'" type="audio/ogg">
                        <source src="'.$post_image.'" type="audio/mpeg">
                        Your browser does not support the audio element.
                      </audio>';
                    }elseif($mediatype == 'video'){
                      $data .='
                      <div>
                        <video style="width: 100%;" controls>
                          <source src="'.$post_image.'" type="video/mp4">
                          Sorry, your browser doesnt support the video element.
                        </video>
                      </div>';
                    }elseif($mediatype == "application"){
                      $ext = pathinfo($post_image, PATHINFO_EXTENSION);
                      if($ext == 'pdf'){
                        $data .= '<embed src="'.$post_image.'" type="application/pdf">';
                      }elseif($ext == 'docx' || $ext == 'doc'){
                        $data .= '<div style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
                      }
                    }
                    $data .='
                    </div>
                    <div class="col-sm-9 col-md-9 text-center">
                      <strong class="red">This Media Content is Not Editable, Because It is a Resize Image.</strong>
                    </div>
                  </div>
                  <input type="hidden" name="media_url[]" value="'.$post_image.'">
                  <input type="hidden" name="media_source[]" value="'.$media_source.'">
                  <input type="hidden" name="post_id[]" value="-na-">
                  <input type="hidden" name="filename[]" value="-na-">
                  <input type="hidden" name="alt_text[]" value="-na-">
                  <input type="hidden" name="caption[]" value="-na-">
                  <input type="hidden" name="img_type[]" value="-na-">
                  <input type="hidden" name="img_description[]" value="-na-">
                  ';
                }
              }
            }
            $data .='
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="container">
          <input type="submit" name="submit" class="btn btn-blue" value="Save">
          <input type="submit" name="submit2" class="btn btn-blue" value="Save and Exit">
          <button type="button" class="btn btn-red" onclick="window.close();">Cancel</button>
        </div>
      </div>
    </form>
  </div>';
  // $result = json_encode($delArr);
  echo $data;
}
