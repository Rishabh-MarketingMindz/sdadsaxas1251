<?php
$current_dir = dirname(__FILE__);
$current_dir = str_replace("/wp-content/plugins/wp_media_cleaner/lib/media-scan", "", $current_dir);
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';

// check if content scanner page is active
@$media_scan = get_option('content_scan_activate');
@$lisence_activation = get_option('WPMC_lisence_activation');

// Restore and permament delete contents
global $wpdb;
$prefixes = array();
if(is_multisite()){
	$is_multisite = get_sites();
	$prefix = count($is_multisite);
	$count = 0;
		for($i=1 ; $i<= $prefix ;$i++){
		$prefixes[$count]['prefix'] =$wpdb->get_blog_prefix($i);
		$count++;
	}

	function array_push_assoc($array, $key, $value){
		$array[$key] = $value;
		return $array;
	}
	
	$allTrashData = array();
	foreach($prefixes as $multiplsites){
		$prefixValue = $multiplsites['prefix'];
		$deleted_images = $wpdb->get_results("SELECT * FROM ".$prefixValue."posts WHERE post_status = 'trash'", ARRAY_A);
		if(!empty($deleted_images)){
			$newDelArr = $deleted_images;			
		    foreach ($newDelArr as $newvalue) {
		    	$allTrashData[] = array_push_assoc($newvalue, 'prefix', $prefixValue);
		    }		    
		}
		$deleted_images = array_filter($allTrashData);

	 	if(!empty($_REQUEST['deletepagepost'] == 'deletepagepost')){
		 	global $wpdb;
			$row = count($_POST["users"]);
			for($i=0;$i<$row;$i++){
		    	$main_post = $wpdb->query("DELETE FROM ".$prefixValue."posts WHERE ID = '".$_POST["users"][$i]."' ");
		    	$child_post = $wpdb->query("DELETE FROM ".$prefixValue."posts WHERE post_parent = '".$_POST["users"][$i]."' OR post_type='revision' ");
		    	$postmeta_delete = $wpdb->query("DELETE FROM ".$prefixValue."postmeta WHERE post_id = '".$_POST["users"][$i]."'");
		    	header("Location: admin.php?page=media-cleaner-content-scan");
		    }
		}


		if(!empty($_REQUEST['restorepagepost']== 'restorepagepost')){  
			global $wpdb;
			$row = count($_POST["users"]);
			for($i=0;$i<$row;$i++){
				$main_post = $wpdb->query("UPDATE ".$prefixValue."posts SET post_status = 'publish' WHERE ID = '".$_POST["users"][$i]."' ");
				$child_post = $wpdb->query("UPDATE ".$prefixValue."posts WHERE post_parent = '".$_POST["users"][$i]."' AND post_type='revision' AND post_status = 'publish'");
				$postmeta_delete = $wpdb->get_results("UPDATE ".$prefixValue."postmeta WHERE post_id = '".$_POST["users"][$i]."' ");
				header("Location: admin.php?page=media-cleaner-content-scan");
			}
		}

		if(isset($_REQUEST['deletemulti'])){
	    	global $wpdb;
	    	$ids = $_REQUEST['deletemulti'];
			$main_post = $wpdb->query("DELETE FROM ".$prefixValue."posts WHERE ID = '$ids'");
			$child_post = $wpdb->query("DELETE FROM ".$prefixValue."posts WHERE post_parent = '$ids' OR post_type='revision'");
			$postmeta_delete = $wpdb->query("DELETE FROM ".$prefixValue."postmeta WHERE post_id = '$ids'");
			  header("Location: admin.php?page=media-cleaner-content-scan");
		}
		if(isset($_REQUEST['restoremulti'])){
			global $wpdb;
			$data = $_REQUEST['restoremulti'];
			$main_post = $wpdb->query("UPDATE ".$prefixValue."posts SET post_status = 'publish' WHERE ID = '$data'");
			$child_post = $wpdb->query("UPDATE ".$prefixValue."posts WHERE post_parent = '$data' AND post_type='revision' AND post_status = 'publish'");
			$postmeta_delete = $wpdb->get_results("UPDATE ".$prefixValue."postmeta WHERE post_id = '$data'");
			header("Location: admin.php?page=media-cleaner-content-scan");
		}
	}
}else{
	$deleted_images = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE post_status = 'trash'");
	if(!empty($_REQUEST['deletesingle'])){ 
	    global $wpdb;
		$row = count($_POST["site"]);
		for($i=0;$i<$row;$i++){
	    	$main_post = $wpdb->query("DELETE FROM ".$wpdb->prefix."posts WHERE ID = '".$_POST["site"][$i]."' ");
	    	$child_post = $wpdb->query("DELETE FROM ".$wpdb->prefix."posts WHERE post_parent = '".$_POST["site"][$i]."' AND post_type='revision' AND post_status = 'publish' ");
	    	$postmeta_delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = '".$_POST["site"][$i]."'");
	    	header("Location: admin.php?page=media-cleaner-content-scan");
	    }	
	}
	if(!empty($_REQUEST['restoresingle'] )) {  
		global $wpdb;
		$row = count($_POST["site"]);
		for($i=0;$i<$row;$i++){
	    	$main_post = $wpdb->query("UPDATE ".$wpdb->prefix."posts SET post_status = 'publish' WHERE ID = '".$_POST["site"][$i]."'");
	    	$child_post = $wpdb->query("UPDATE ".$wpdb->prefix."posts WHERE post_parent = '".$_POST["site"][$i]."' AND post_type='revision' AND post_status = 'publish'");
		    $postmeta_delete = $wpdb->get_results("UPDATE ".$wpdb->prefix."postmeta WHERE post_id = '".$_POST["site"][$i]."'");
		    header("Location: admin.php?page=media-cleaner-content-scan");
		}
	}
	if(isset($_REQUEST['delete'])){
		global $wpdb;
		$ids = $_REQUEST['delete'];
		$main_post = $wpdb->query("DELETE FROM ".$wpdb->prefix."posts WHERE ID = '$ids'");
		$child_post = $wpdb->query("DELETE FROM ".$wpdb->prefix."posts WHERE post_parent = '$ids' OR post_type='revision'");
		$postmeta_delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = '$ids'");
		  header("Location: admin.php?page=media-cleaner-content-scan");
	}
	if(isset($_REQUEST['restore'])){
		global $wpdb;
		$data = $_REQUEST['restore'];
		$main_post = $wpdb->query("UPDATE ".$wpdb->prefix."posts SET post_status = 'publish' WHERE ID = '$data'");
		$child_post = $wpdb->query("UPDATE ".$wpdb->prefix."posts WHERE post_parent = '$data' AND post_type='revision' AND post_status = 'publish'");
	    $postmeta_delete = $wpdb->get_results("UPDATE ".$wpdb->prefix."postmeta WHERE post_id = '$data'");
	    header("Location: admin.php?page=media-cleaner-content-scan");
	}
}

if($media_scan == "yes" && $lisence_activation == 1){
?>
<style type="text/css">
	/*CSS FOR PROGRESS LOADER BAR*/
	#myProgress {
	  width: 100%;
	  background-color: #fff;
	  border-radius: 5px;
	  overflow: hidden;
	  margin-left: 10px;
	  position: relative;
	}
	#myBar {		
		width: 0%;
		height: 30px;
		background-color: #f24e85;
	}
	.progress_center {
	    display: flex;
	    align-items: center;
	}
	#myProgress #progress_counter {
	    position: absolute;
	    top: 5px;
	    left: 50%;
	    transform: translateX(-50%);
	    color: #000;
	    z-index: 1;
	    margin: 0;
	}
	/*CSS FOR PROGRESS LOADER BAR ENDS */

	table.dataTable thead tr th {
		border: none;
	}
	table.dataTable {
		border: none;
	}
	.dataTables_wrapper.no-footer .dataTables_scrollBody {
		border-bottom: 1px solid #ddd;
		border-top: 1px solid #ddd;
	}
	.btn-blue, .multiselect.dropdown-toggle.btn.btn-default {
		background-color: #4DB9AB;
		color: #fff;
	}
	.btn-red {
		background-color: #E53373;
		color: #fff;
	}	
	/*small loader*/
	.loader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 60px;
		height: 60px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
		margin: 0 auto;
	}
	@-webkit-keyframes spin {
		0% {
		 -webkit-transform: rotate(0deg);
		}
		100% {
		 -webkit-transform: rotate(360deg);
		}
	}
	@keyframes spin {
		0% {
		 transform: rotate(0deg);
		}
		100% {
		 transform: rotate(360deg);
		}
	}
</style>
<div class="alert-box"></div>
<!-- Rotating logo -->
<div class="loading" style="display: none">
	<div class="loading_box"><?php _e("Scanning for contents","wp_media_cleaner"); ?><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span> 
		<div class="progress_center">
			<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
			<div id="myProgress">
				<span id="progress_counter"></span>
				<div id="myBar"></div>
			</div>
		</div>
	</div>
</div>
<!-- Rotating logo for actions-->
<div class="action_loader">
	<div class="loading_box">
		<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>">
        <span>LOADING...</span>
	</div>
</div>
<!-- Only Rotating logo -->
<div class="loading_rotating" style="display: none;">
	<div class="loading_box"><div class="action_message"></div><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
	</div>
</div>

<!-- filtered images resultant div -->
<div id="filtered_image_result" style="display: none">	
</div>
<!-- Confirm taking media backup Modal -->
<div id="mediaDeleter" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4><?php _e('Do you want to take backup of selected media before deleting them ?','wp_media_cleaner'); ?></h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="DeleteFilteredImages('yes')">Yes</button>
        <button type="button" id="no" class="btn btn-red" onclick="DeleteFilteredImages('no')">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Edit media modal -->
<div id="editMedia" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <form type="form" method="post" id="updateMediaData">
        <div class="modal-body media_results"> </div>
        <div class="modal-footer">
          <div class="container">
            <input type="submit" class="btn btn-blue" value="Save">
            <button type="button" id="save_exit" class="btn btn-blue">Save and Exit</button>
            <button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!--------Pop For Page and Post ---------->
<div id="restoreDeletedImagesPopUp" class="modal fade" role="dialog">
  	<div class="modal-dialog" style="max-width: 920px"> 
	    <!-- Modal content-->
		<?php 
		if(is_multisite())
		{
		?>
		<div class="modal-content">
			<form name="frmUser" method="POST" action="">
				<div class="modal-body">
					<table id="restore_img" class="table table-striped table-sm text-left" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="50">&nbsp;</th>
					            <th><?php _e("Content Name","wp_media_cleaner"); ?></th>
								<th><?php _e("Content Type","wp_media_cleaner");?></th>
								<th><?php _e("Action","wp_media_cleaner"); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($deleted_images as $deleted_urls) 
							{
							$post_title = $deleted_urls['post_title'];
							$post_status = $deleted_urls['post_status'];
							$post_type = $deleted_urls['post_type'];
							$id = $deleted_urls['ID'];
							?>
							<tr>
								<td width="50"><input type="checkbox" name="users[]" value="<?php echo $id;?>" class="chkbox" id="check"></td>
								<td><?php echo $post_title;?></td>
								<td><?php echo $post_type;?></td>
								<td>
									<button type="submit" class="btn btn-blue" name="restoremulti" value="<?php echo $id;?>">Restore</button>
								</td>
								<td>
									<button type="submit" class="btn btn-red" name="delete" value="<?php echo $id;?>">Delete</button>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
					<?php 
					if(count($deleted_images) == "")
					{ ?>
						<button type="submit" class="btn btn-red" name="deletepagepost" value="deletepagepost" id="deletepagepost"><?php _e("Delete selected content","wp_media_cleaner") ?></button>
						<button type="submit" class="btn btn-blue" name="restorepagepost" value="restorepagepost" id=
						"restorepagepost" ><?php _e("Restore selected content","wp_media_cleaner") ?></button>
					<?php }
					else
					{?>
						<button type="submit" class="btn btn-red" name="deletepagepost" value="deletepagepost" id="deletepagepost"><?php _e("Delete all backup contents","wp_media_cleaner") ?></button>
						<button type="submit" class="btn btn-blue" name="restorepagepost" value="restorepagepost" id="restorepagepost"><?php _e("Restore all backup contents","wp_media_cleaner") ?></button>
					<?php
					}
					?>
				</div>
			</form>
		</div>
		<?php }
		else
		{
		?>
		<div class="modal-content">
			<form name="singleSite" method="POST" action="">
				<div class="modal-body">
					<table id="restore_img" class="table table-striped table-sm text-left" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><?php _e("Select Content","wp_media_cleaner"); ?></th>	
								<th><?php _e("Content Name","wp_media_cleaner"); ?></th>
								<th><?php _e("Content Type","wp_media_cleaner");?></th>
								<th colspan="2" style="text-align: center;"><?php _e("Action","wp_media_cleaner"); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($deleted_images as $deleted_urls) {
								$post_title = $deleted_urls->post_title;
								$post_type = $deleted_urls->post_type; 
								$id = $deleted_urls->ID;
								?>
								<tr>
									<td><input type="checkbox" name="site[]" value="<?php echo $id;?>" class="chkbox"></td>
									<td><?php echo $post_title;?></td>
									<td><?php echo $post_type;?></td>
									<td>
										<button type="submit" class="btn btn-blue" name="restore" value="<?php echo $id;?>">Restore</button>
									</td>
									<td>
										<button type="submit" class="btn btn-red" name="delete" value="<?php echo $id;?>">Delete</button>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
						<?php 
						if(count($deleted_images) == ""){
						?>
							<button type="submit" class="btn btn-red" name="deletesingle" value="deletesingle" disabled>Delete Post and Pages</button>
							<button type="submit" class="btn btn-blue" name="restoresingle" value="restoresingle" disabled>Restore Pages and Post</button>
						<?php 
						}
						else{
						?>
							<button class="btn btn-red" name="deletesingle" value="deletesingle" data-toggle="modal" data-target="#content">Delete Post and Pages</button>
							<button class="btn btn-blue" name="restoresingle" value="restoresingle" data-toggle="modal" data-target="#content">Restore Pages and Post</button>
						<?php
						}
						?>
				</div>
			</form>
		</div>  
		<?php }
		?>
	</div>
</div>
<!--------Pop Up End For Page and Post-------->
<div class="WPMC_media_scan">
	<div style="display: none;">
		<!-- All message for the actions (like deltetion, exclusion, optimization...etc) -->
		<span class="action_message_deletion"><?php _e("Deleting Content","wp_media_cleaner"); ?></span>
	</div>
  <!-- CMS usage data from w3techs.com / captured 7/6/16 -->
  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="sss" type="button">Scan Content</button>
      </li>
      <li> <span class="text">Filter:</span>
        <select name="contentTypeOpt[]" class="btn btn-blue" multiple id="contentTypeOpt">
        </select>
      </li>
      <li>
        <input type="button" name="daterange" value="Date Picker" id="datePicker" class="btn btn-blue" />
      </li>
      <li>
        <select name="categoryTypeOpt[]" multiple id="categoryTypeOpt" class="btn btn-blue">
        </select>
      </li>
      <li>
        <select name="postStatusOpt[]" multiple id="postStatusOpt" class="btn btn-blue">
          <option value="publish">Publish</option>
          <option value="pending">Pending</option>
          <option value="draft">Draft</option>
          <option value="private">Private</option>
          <option value="future">Future</option>
          <option value="trash">Trash</option>
        </select>
      </li>
      <li>
        <label class="gray"><span id="total_files"></span><span id="no_files">No</span> content found</label>
        <!--label class="red">Original size <span id="media_size"></span><span id="no_size">- 0.0 MB</span> - Compressed size 0.0MB</label-->
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="scaned_content" class="table table-striped table-sm table_design" cellspacing="0" width="100%">
      <thead>
        <tr>
			<th><input name="show_all_check" type="checkbox"></th>
			<th>Content</th>
			<th>Content Type</th>
			<th>Media</th>
			<th>Creation Date</th>
			<th>Status</th>
			<th>&nbsp;</th>
        </tr>
      </thead>
      <tbody id="content_scanned_result">
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
        </tr>
        <tr>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">&nbsp;</td>
			<td colspan="7" align="center"><strong class="red"><?php _e("Ready to scan !","wp_media_cleaner"); ?></strong></td>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">&nbsp;</td>
        </tr>
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-blue select" id="getfilteredimages" type="button">Select Content Files</button>
      </li>
      <li>
        <button class="btn btn-red" id="deletefilteredimages" type="button" data-toggle="modal" data-target="#mediaDeleter">Delete Filtered Content</button>
      </li>
      <li>
        <button class="btn btn-blue" id="showfilteredimages" type="button">Show Filtered Content</button>
      </li>
      <li><button style="width: 150px;" class="btn btn-blue" id="showAllFilteredImages" type="button"><?php _e("Show All Content","wp_media_cleaner"); ?></button></li>
      <li>
        <button class="btn btn-red" type="button" data-toggle="modal" data-target="#restoreDeletedImagesPopUp"><?php _e("Restore Content","wp_media_cleaner"); ?></button>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript">
	jQuery(window).load(function(){
		setTimeout(function() {
		    jQuery('.action_loader').fadeOut('fast');
		}, 3000);
	});	
	jQuery(document).on("click",".editMediaModal",function() {
		jQuery("#editMedia .media_results").html('<div class="loader"></div>');
		jQuery('#editMedia').modal('show');
		var media = jQuery(this).attr("data-link");
		var source = jQuery(this).attr("data-source");
		var pagefrom = jQuery(this).attr("data-from");
		var pagetype = jQuery(this).attr("data-type");
		var pagesite = jQuery(this).attr("data-site");
		var page_url = "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/lib/content-scan/edit-media-result.php";
		jQuery.post("<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/get_attachment_meta.php", function (data) {
		    var w = window.open(page_url);
		    w.document.open();
		    w.document.write(data);
		    w.document.close();
		});
		// jQuery.ajax({
		// 	type: "POST",
		// 	url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/get_attachment_meta.php",
		// 	data: {
		// 		request_type: "get_attachment_meta",
		// 		media_src: media,
		// 		media_source: source,
		// 		page_from: pagefrom,
		// 		page_type: pagetype,
		// 		page_site: pagesite
		// 	},
		// 	cache: false,
		// 	success: function(results){
		// 		jQuery("#editMedia .media_results").html(results);
		// 	},
		// 	error: function (results) {
		// 		alert('Something Went Wrong, Please Try Again');
		// 	}
		// });
	});
	jQuery(document).on("click",".hover_me",function(e){
	    e.preventDefault();
	    jQuery(this).siblings(".tooltipstext").fadeIn(300,function(){jQuery(this).focus();});
	});

	jQuery(document).on("click",".close",function(){
	   jQuery(".tooltipstext").fadeOut(300);
	});
	jQuery(document).on("blur",".tooltipstext",function(){
	    jQuery(this).fadeOut(300);
	});

	jQuery(document).ready(function() {
		jQuery("#deletefilteredimages").prop("disabled", true); // Disable delete button on page load

		jQuery("#deletesingle").prop("disabled", true);
      	jQuery('#restoresingle').prop("disabled", true);

		jQuery("#deletepagepost").prop("disabled", true);
      	jQuery('#restorepagepost').prop("disabled", true);

		// Show only selected (checked) media	
		// jQuery("#showfilteredimages").on('click',function(){
		// 	jQuery("#filtered_image_result").html("");
		// 	var checkValues = jQuery('input[name=check_list]:checked').map(function(){
	 //            var vals = jQuery(this).parent().parent().html();
	 //            jQuery("#filtered_image_result").append('<tr role="row" class="odd">'+vals+'</tr>');
	 //        }).get();
	 //        var get_result = jQuery("#filtered_image_result").html();
	 //        jQuery("#content_scanned_result").html(get_result);
		// });
		jQuery("#showfilteredimages").click(function(){
			jQuery("#content_scanned_result input[name=check_list]:checkbox:not(:checked)").parent().parent().hide();
			jQuery("#content_scanned_result input[name=check_list1]:checked").parent().parent().show();
		});
		jQuery("#showAllFilteredImages").on('click',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
	    var $chkboxes = jQuery('.chkbox');
	    var lastChecked = null;
		jQuery(document).on("click",".chkbox",function(e) {
	        if (!lastChecked) {
	            lastChecked = this;
	            return;
	        }
	        if (e.shiftKey) {
	            var start = jQuery('.chkbox').index(this);
	            var end = jQuery('.chkbox').index(lastChecked);
	            jQuery('.chkbox').slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
	        }
	        lastChecked = this;
	    });
	});
	
	jQuery("#sss").click(function(){
		// Progress bar start		
		var i = 0;
		var myvar = 33;
		var width = 0;
		jQuery("#myBar").css("background-color","#f24e85");
		jQuery("#progress_counter").text("0%");
		jQuery("#myBar").css("width","0%");
		jQuery(".loading").show();
		jQuery("#myBar").show();
		if (i == 0) {
		    i = 1;
		    time_var = window.setInterval(function(){
			  myvar = parseInt(myvar) +  450;
			  var id = setInterval(frame, myvar);
			  jQuery("#progress_counter").empty().text(width+'%');
			  console.log(width);
			  if(width == 100){				  	
			  	clearInterval(id);
			  	clearInterval(time_var);
			  }
			}, 100);
		    var elem = document.getElementById("myBar");			    
		    // var id = setInterval(frame, myvar);
		    function frame() {			    	
				if (width >= 100) {
					// clearInterval(id);
					i = 0;
				} else {
					if (width <= 97 || width == 99) {
						width++;
						elem.style.width = width + "%";
					}
				}
		    }
		}
		// Progress bar ends
		jQuery('#scaned_content').dataTable().fnDestroy();
		jQuery('#scaned_content').DataTable({
	        'ajax': {
				type: 'POST',
				'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/contentscan.php',
				data: {
					request_type: "scan_content",
				},
				"dataSrc": function (json) {
					
					jQuery("#no_size").hide();
					jQuery("#total_files").html(json.total_files);
					jQuery("#no_files").hide();
					return json.data;
				},
				complete: function(){
					// For Progress bar loader
					width = 100;
					jQuery("#myBar").css("background-color","#4DB9AB");
					jQuery("#myBar").css("width","100%");
					setTimeout(function() {
					    jQuery('.loading').fadeOut('fast');
					}, 1000);
					// For Progress bar loader ends

					if(jQuery("#datePicker").val() != 'Date Picker'){
						var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
						var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
						dependFilter(startDate,endDate);
					}else{
						dependFilter();
					}
	            },

		    },
		    "paging": false,
		    "info": false,
		    "scrollY": "400px",
        	"scrollCollapse": true,
        	"language": {
		        searchPlaceholder: "Search content",
		    }
	    });
	});

	// jQuery('#getfilteredimages').toggle(function(){
 //        jQuery("input[name=check_list]").prop('checked', true);
 //        jQuery(this).text("Deselect Filtered Images");
 //    },function(){
 //        jQuery("input[name=check_list]").prop('checked', false);
 //        jQuery(this).text("Select Filtered Images");
 //    })

 	jQuery(document).on("click","input[name=check_list]",function() {
 		var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
      if(atLeastOneIsChecked){
      	jQuery("#deletefilteredimages").prop("disabled", false);
      }else{
      	jQuery("#deletefilteredimages").prop("disabled", true);
      }
 	});

 	// Select and  Deselect all checkbox 
	jQuery("input[name=show_all_check]").change(function(){
		if(jQuery(this).prop("checked") == true){
            jQuery("input[name=show_all_check]").prop('checked', true);
            jQuery("input[name=check_list1]").prop('checked', true);
            jQuery("input[name=check_list]").prop('checked', true);
            jQuery("#deletefilteredimages").prop("disabled", false);

            jQuery("#getfilteredimages").text("Deselect Content Files");
			jQuery("#getfilteredimages").removeClass("select").addClass("deselect");
        }else{
        	jQuery("input[name=show_all_check]").prop('checked', false);
        	jQuery("input[name=check_list1]").prop('checked', false);
        	jQuery("input[name=check_list]").prop('checked', false);

        	jQuery("#getfilteredimages").text("Select Content Files");
			jQuery("#getfilteredimages").removeClass("deselect").addClass("select");
			jQuery("#deletefilteredimages").prop("disabled", true);
        }
	});

 	// Select Deselect filtered button
	jQuery(document).on("click","#getfilteredimages",function() {
		var text = jQuery(this).hasClass("select");
		if(text){
			jQuery(this).text("Deselect Content Files");
			jQuery(this).removeClass("select").addClass("deselect");
			jQuery("input[name=check_list]").prop('checked', true);
			jQuery("#deletefilteredimages").prop("disabled", false);
		}else{
			jQuery(this).text("Select Content Files");
			jQuery(this).removeClass("deselect").addClass("select");
			jQuery("input[name=check_list]").prop('checked', false);
			jQuery("input[name=show_all_check]").prop('checked', false);
			jQuery("#deletefilteredimages").prop("disabled", true);
		}
	});

    // Script for opening edit media in new window start
    function openPopupPage(relativeUrl, media_name, media_src, page_from,page_type, page_site){
		var param = {
			'media_src': media_src,
			'page_from': page_from,
			'page_type': page_type,
			'page_site': page_site,
			'media_source': media_name
		};
		OpenWindowWithPost(relativeUrl, "width=1000, height=600, left=100, top=100, resizable=yes, scrollbars=yes", "NewFile", param);
	}
	function OpenWindowWithPost(url, windowoption, name, params){
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("action", url);
		form.setAttribute("target", name);
		for (var i in params){
			if (params.hasOwnProperty(i)){
				var input = document.createElement('input');
				input.type = 'hidden';
				input.name = i;
				input.value = params[i];
				form.appendChild(input);
			}
		}
		document.body.appendChild(form);
		//note I am using a post.htm page since I did not want to make double request to the page 
		//it might have some Page_Load call which might screw things up.
		window.open("post.htm", name, windowoption);
		form.submit();
		document.body.removeChild(form);
	}
	// Script for opening edit media in new window Ends

	function GetCheckboxSelected(){
		jQuery(".action_loader").show();
		var checkValues = jQuery('input[name=check_list]:checked').map(function()
        {
            return jQuery(this).val();
        }).get();
    	jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/add_to_media.php",
			data: {
				request_type: "add_to_media",
				media_src: checkValues
			},
			cache: false,
			success: function(res){
				jQuery(".action_loader").hide();
				var obj = jQuery.parseJSON(res);
				var i = 0;
				jQuery.each(obj, function(key, value ) {
					jQuery("input[type=checkbox][value='"+value+"']").prop("checked", false);
					i++;
				});
				jQuery( ".alert-box" ).addClass('success');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) added to library</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
			},
			error: function (res) {
				jQuery(".action_loader").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
			}
		});
	}

	function DeleteFilteredImages($backup){
		// showing loader for deletion start
		var popup_message = jQuery(".action_message_deletion").html();
		jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
		// showing loader for deletion ends

		jQuery('#mediaDeleter').modal('toggle');
		if($backup == 'yes'){
			var isbackup = 'yes';
		}else{
			var isbackup = 'no';
		}		
		var checkValues = jQuery('input[name=check_list]:checked').map(function(){
            return jQuery(this).val();
        }).get();
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/delete_content.php",
			data: {
				backup: isbackup,
				content_src: checkValues
			},
			cache: false,
			success: function(response){
				jQuery(".loading_rotating").hide();
				var obj = jQuery.parseJSON(response);
				if(obj.length>0){
					var i = 0;
					jQuery.each(obj, function(key, value ) {
						jQuery("input[type=checkbox][value='"+value+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800).remove();
						i++;
					});
					jQuery( ".alert-box" ).removeClass('unsuccess');
					jQuery( ".alert-box" ).addClass('success');
					jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' file(s) deleted</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				}else{
					jQuery(".loading_rotating").hide();
					jQuery( ".alert-box" ).removeClass('success');
					jQuery( ".alert-box" ).addClass('unsuccess');
					jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
				}
				jQuery("input[name=check_list]").prop('checked', false);
			},
			error: function (jqXHR, exception) {
				jQuery(".loading_rotating").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
			}
		});
	}

	jQuery(document).ready(function(){
		jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_cpt',
			},
			success: function(response) {
				jQuery('#contentTypeOpt').empty().append(response);
				jQuery('#contentTypeOpt').multiselect({
				    nonSelectedText: 'Content Type',
				    allSelectedText: 'Content Type',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});
			},
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });

	    jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_categories',
			},
			success: function(response) {
				jQuery('#categoryTypeOpt').empty().append(response);
				jQuery('#categoryTypeOpt').multiselect({
				    nonSelectedText: 'Category',
				    allSelectedText: 'Category',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});
			},
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });
	});

	jQuery('#linkedOpt').on('change',function(){
		var linked_status = jQuery(this).val().toString();

		if(linked_status == "no"){
			jQuery('#abcd tbody tr').each(function(){
				console.log('tr');
				console.log(jQuery('td:eq(4)', this));
			});

			console.log('if condition');
		}

		console.log(linked_status);

	});
	/* media scan filter start */
	jQuery(document).ready(function(){
		jQuery("#contentTypeOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		jQuery("#postStatusOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		jQuery("#categoryTypeOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});

		// Disable and enable Restore/Delete buttons on pop up
		jQuery(document).on("change","input[id=chbx]",function() {
		 		var disable = jQuery('input[id=chbx]:checkbox:checked').length > 0;
		      if(disable){
		      	jQuery("#deletesingle").prop("disabled", false);
		      	jQuery('#restoresingle').prop("disabled", false);
		      }else{
		      	jQuery("#deletesingle").prop("disabled", true);
		      	jQuery('#restoresingle').prop("disabled", true);
		      }
		});

		jQuery(document).on("change","input[id=check]",function() {
		 		var disable = jQuery('input[id=check]:checkbox:checked').length > 0;
		      if(disable){
		      	jQuery("#deletepagepost").prop("disabled", false);
		      	jQuery('#restorepagepost').prop("disabled", false);
		      }else{
		      	jQuery("#deletepagepost").prop("disabled", true);
		      	jQuery('#restorepagepost').prop("disabled", true);
		      }
		});
	});
	jQuery(document).on("click",".daterangepicker .applyBtn",function() {
		var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
		var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
		dependFilter(startDate,endDate);
	});
	jQuery(document).on("click",".daterangepicker .cancelBtn",function() {
		if(jQuery("#datePicker").val() != 'Date Picker'){
			var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
			var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
			dependFilter(startDate,endDate);
		}else{
			dependFilter();
		}
	});
	function dependFilter(startDate,endDate){
		var filterValue, filterValueArr, matchArray, found, found1, found2, found3, contentTypeOptArr, postStatusOptArr, categoryTypeOptArr, startDateArr, endDateArr, fDate,lDate,cDate;
		var contentTypeOpt = jQuery("#contentTypeOpt").val();
		var postStatusOpt = jQuery("#postStatusOpt").val();
		var categoryTypeOpt = jQuery("#categoryTypeOpt").val();
		var startDate = startDate;
		var endDate = endDate;
		var mainFilter = [];
		if(contentTypeOpt != null){
			contentTypeOpt = jQuery.map(contentTypeOpt, function(n,i){return n.toLowerCase();});
			mainFilter['contentTypeOpt'] = contentTypeOpt;
		}
		if(postStatusOpt != null){
			postStatusOpt = jQuery.map(postStatusOpt, function(n,i){return n.toLowerCase();});
			mainFilter['postStatusOpt'] = postStatusOpt;
		}
		if(categoryTypeOpt != null){
			categoryTypeOpt = jQuery.map(categoryTypeOpt, function(n,i){return n.toLowerCase();});
			mainFilter['categoryTypeOpt'] = categoryTypeOpt;
		}
		if(startDate != null && endDate != null){
			mainFilter['startDate'] = startDate;
			mainFilter['endDate'] = endDate;
		}
		contentTypeOptArr = mainFilter.contentTypeOpt;
		postStatusOptArr = mainFilter.postStatusOpt;
		categoryTypeOptArr = mainFilter.categoryTypeOpt;
		startDateArr = mainFilter.startDate;
		endDateArr = mainFilter.endDate;
		if(contentTypeOptArr == null){
			contentTypeOptArr = [];
		}
		if(postStatusOptArr == null){
			postStatusOptArr = [];
		}
		if(categoryTypeOptArr == null){
			categoryTypeOptArr = [];
		}
		if(startDateArr == null && endDateArr == null){
			startDateArr = [];
			endDateArr = [];
		}
		jQuery("#content_scanned_result tr td").find("#att-filter").each(function(){
			var getDiv = jQuery(this).parent().parent();
			if(contentTypeOptArr.length != 0 || postStatusOptArr.length != 0 || categoryTypeOptArr.length != 0 || startDateArr.length != 0 || endDateArr.length != 0){
				filterValue = jQuery(this).val();
				filterValueArr = filterValue.split(',');
				filterValueArr = jQuery.map(filterValueArr, function(n,i){return n.toLowerCase();});
				if(contentTypeOptArr.length != 0){
					found = contentTypeOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found = true;
				}
				if(postStatusOptArr.length != 0){
					found1 = postStatusOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found1 = true;
				}
				if(categoryTypeOptArr.length != 0){
					found2 = categoryTypeOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found2 = true;
				}
				if(startDateArr.length != 0 || endDateArr.length != 0){
					postDate = filterValueArr[1];
					fDate = Date.parse(startDateArr);
				    lDate = Date.parse(endDateArr);
				    cDate = Date.parse(postDate);
				    if((cDate <= lDate && cDate >= fDate)) {
				    	console.log('match');
				        found3 = true;
				    }else{
				    	console.log('un');
				    	found3 = false;
				    }
				}else{
					found3 = true;
				}
				if(found && found1 && found2 && found3){
					getDiv.show();
					getDiv.find('.sorting_1 input').attr('class', 'chkbox');
					getDiv.find('.sorting_1 input').attr('name', 'check_list');
				}else{
					getDiv.hide();
					getDiv.find('.sorting_1 input').attr('class', 'chkbox1');
					getDiv.find('.sorting_1 input').attr('name', 'check_list1');
				}
			}else{
				getDiv.show();
				getDiv.find('.sorting_1 input').attr('class', 'chkbox');
				getDiv.find('.sorting_1 input').attr('name', 'check_list');
			}
		});
	}
	/* media scan filter end */
</script>
<?php
}else{
	if($media_scan != "yes" && $lisence_activation == 1){
		?>
		<div class="WPMC_media_scan">
			<div class="disable_section">
				<h3><?php _e("Content scanner page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_cleaner"); ?></h3>
				<a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Go to settings","wp_media_cleaner"); ?></a>
			</div>
		</div>
		<?php
	}elseif($media_scan == "yes" && $lisence_activation == 0){
		$admin_url = get_admin_url();
		?>
		<div class="WPMC_media_scan">
			<div class="disable_section">
				<h3><?php _e("Please activate your license to access content scanner page","wp_media_cleaner"); ?></h3>
				<a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Activate Now","wp_media_cleaner"); ?></a>
			</div>
		</div>
		<?php
	}
	
}
?>